<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/p')) {
            if (0 === strpos($pathinfo, '/profile')) {
                // yotta_profile_profiledata
                if ($pathinfo === '/profile/data') {
                    return array (  '_controller' => 'YottaBundle\\Controller\\ProfileController::profileDataAction',  '_route' => 'yotta_profile_profiledata',);
                }

                // yotta_profile_profiledataupdate
                if ($pathinfo === '/profile/update') {
                    return array (  '_controller' => 'YottaBundle\\Controller\\ProfileController::profileDataUpdateAction',  '_route' => 'yotta_profile_profiledataupdate',);
                }

            }

            // pubnub
            if ($pathinfo === '/pubnubINIT') {
                return array (  '_controller' => 'YottaBundle\\Controller\\YottaController::test',  '_route' => 'pubnub',);
            }

        }

        // yotta_yotta_subscribtion
        if ($pathinfo === '/subscribtion') {
            return array (  '_controller' => 'YottaBundle\\Controller\\YottaController::subscribtion',  '_route' => 'yotta_yotta_subscribtion',);
        }

        // addBook
        if ($pathinfo === '/addBook') {
            return array (  '_controller' => 'YottaBundle\\Controller\\BookController::addBookAction',  '_route' => 'addBook',);
        }

        // notification_index
        if ($pathinfo === '/notification') {
            return array (  '_controller' => 'YottaBundle\\Controller\\NotificationController::indexAction',  '_route' => 'notification_index',);
        }

        if (0 === strpos($pathinfo, '/friendRequest')) {
            // notification_accept
            if ($pathinfo === '/friendRequestAccept') {
                return array (  '_controller' => 'YottaBundle\\Controller\\NotificationController::friendRequestAcceptAction',  '_route' => 'notification_accept',);
            }

            // notification_decline
            if ($pathinfo === '/friendRequestDecline') {
                return array (  '_controller' => 'YottaBundle\\Controller\\NotificationController::friendRequestDeclineAction',  '_route' => 'notification_decline',);
            }

        }

        // notification_status
        if ($pathinfo === '/changeNotificationStatus') {
            return array (  '_controller' => 'YottaBundle\\Controller\\NotificationController::changeNotificationStatusAction',  '_route' => 'notification_status',);
        }

        // notification_count
        if ($pathinfo === '/getNotificationCount') {
            return array (  '_controller' => 'YottaBundle\\Controller\\NotificationController::getNotificationCountAction',  '_route' => 'notification_count',);
        }

        // index
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'index');
            }

            return array (  '_controller' => 'YottaBundle\\Controller\\LoginController::indexAction',  '_route' => 'index',);
        }

        // layout_login
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'YottaBundle\\Controller\\LoginController::loginAction',  '_route' => 'layout_login',);
        }

        // login_registration
        if ($pathinfo === '/registration') {
            return array (  '_controller' => 'YottaBundle\\Controller\\LoginController::registrationAction',  '_route' => 'login_registration',);
        }

        // login_social
        if ($pathinfo === '/login_social') {
            return array (  '_controller' => 'YottaBundle\\Controller\\LoginController::login_socialAction',  '_route' => 'login_social',);
        }

        // twitter_auth_tw
        if ($pathinfo === '/OAuthTw') {
            return array (  '_controller' => 'YottaBundle\\Controller\\LoginController::OAuthTwAction',  '_route' => 'twitter_auth_tw',);
        }

        // twitter
        if ($pathinfo === '/twitter') {
            return array (  '_controller' => 'YottaBundle\\Controller\\LoginController::twitterAction',  '_route' => 'twitter',);
        }

        if (0 === strpos($pathinfo, '/s')) {
            // twitter_share
            if ($pathinfo === '/share_in_tw') {
                return array (  '_controller' => 'YottaBundle\\Controller\\LoginController::share_in_twAction',  '_route' => 'twitter_share',);
            }

            // social_friends
            if ($pathinfo === '/social_friends') {
                return array (  '_controller' => 'YottaBundle\\Controller\\LoginController::socialFriendsAction',  '_route' => 'social_friends',);
            }

        }

        // logout
        if ($pathinfo === '/logout') {
            return array (  '_controller' => 'YottaBundle\\Controller\\LoginController::logoutAction',  '_route' => 'logout',);
        }

        // activate_acount
        if ($pathinfo === '/activate_account') {
            return array (  '_controller' => 'YottaBundle\\Controller\\LoginController::activate_accountAction',  '_route' => 'activate_acount',);
        }

        if (0 === strpos($pathinfo, '/profile')) {
            // profile_edit
            if ($pathinfo === '/profile/edit') {
                return array (  '_controller' => 'YottaBundle\\Controller\\ProfileController::editProfileAction',  '_route' => 'profile_edit',);
            }

            // profile_tmpupload
            if ($pathinfo === '/profile/tmpupload') {
                return array (  '_controller' => 'YottaBundle\\Controller\\ProfileController::tmpUploadAction',  '_route' => 'profile_tmpupload',);
            }

            // profile_editpicture
            if ($pathinfo === '/profile/editpicture') {
                return array (  '_controller' => 'YottaBundle\\Controller\\ProfileController::uploadUserPictureAction',  '_route' => 'profile_editpicture',);
            }

        }

        // timeline
        if ($pathinfo === '/timeline') {
            return array (  '_controller' => 'YottaBundle\\Controller\\TimelineController::timelineAction',  '_route' => 'timeline',);
        }

        if (0 === strpos($pathinfo, '/friends')) {
            // friends
            if ($pathinfo === '/friends') {
                return array (  '_controller' => 'YottaBundle\\Controller\\FriendController::friendsAction',  '_route' => 'friends',);
            }

            // people_may_know
            if ($pathinfo === '/friends/peopleMayKnow') {
                return array (  '_controller' => 'YottaBundle\\Controller\\FriendController::peopleMayKnowAction',  '_route' => 'people_may_know',);
            }

        }

        // delete_friend
        if (0 === strpos($pathinfo, '/deletefriend') && preg_match('#^/deletefriend/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_friend')), array (  '_controller' => 'YottaBundle\\Controller\\FriendController::deleteFriendAction',));
        }

        // search_friend
        if ($pathinfo === '/searchFriend') {
            return array (  '_controller' => 'YottaBundle\\Controller\\FriendController::searchFriendAction',  '_route' => 'search_friend',);
        }

        if (0 === strpos($pathinfo, '/loadMoreFriends')) {
            // load_more_friend
            if ($pathinfo === '/loadMoreFriends') {
                return array (  '_controller' => 'YottaBundle\\Controller\\FriendController::loadMoreFriendsAction',  '_route' => 'load_more_friend',);
            }

            // load_more_friend_lent
            if ($pathinfo === '/loadMoreFriendsLent') {
                return array (  '_controller' => 'YottaBundle\\Controller\\FriendController::loadMoreFriendsLentAction',  '_route' => 'load_more_friend_lent',);
            }

        }

        // search_from_friend
        if ($pathinfo === '/searchFriendLent') {
            return array (  '_controller' => 'YottaBundle\\Controller\\FriendController::searchFriendLentAction',  '_route' => 'search_from_friend',);
        }

        // individual_book
        if (0 === strpos($pathinfo, '/book') && preg_match('#^/book/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'individual_book')), array (  '_controller' => 'YottaBundle\\Controller\\BookController::individualBookAction',));
        }

        // get_books_by_tag
        if (0 === strpos($pathinfo, '/tag') && preg_match('#^/tag/(?P<tag_name>\\w+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_books_by_tag')), array (  '_controller' => 'YottaBundle\\Controller\\BookController::getBooksByTagAction',));
        }

        // search
        if ($pathinfo === '/searchResult') {
            return array (  '_controller' => 'YottaBundle\\Controller\\BookController::searchResultAction',  '_route' => 'search',);
        }

        // add_book
        if ($pathinfo === '/addBook') {
            return array (  '_controller' => 'YottaBundle\\Controller\\BookController::addBookAction',  '_route' => 'add_book',);
        }

        // remove_book
        if ($pathinfo === '/removeBook') {
            return array (  '_controller' => 'YottaBundle\\Controller\\BookController::removeBookAction',  '_route' => 'remove_book',);
        }

        // check_book
        if ($pathinfo === '/checkBook') {
            return array (  '_controller' => 'YottaBundle\\Controller\\BookController::checkIfBookExistsAction',  '_route' => 'check_book',);
        }

        // get_book
        if ($pathinfo === '/getBook') {
            return array (  '_controller' => 'YottaBundle\\Controller\\BookController::getBookAction',  '_route' => 'get_book',);
        }

        // load_more
        if ($pathinfo === '/load-more') {
            return array (  '_controller' => 'YottaBundle\\Controller\\BookController::loadMoreAction',  '_route' => 'load_more',);
        }

        if (0 === strpos($pathinfo, '/add')) {
            // recommended_book_add
            if ($pathinfo === '/addRecommendedBook') {
                return array (  '_controller' => 'YottaBundle\\Controller\\BookController::addToReadListAction',  '_route' => 'recommended_book_add',);
            }

            // add_tag_to_book
            if ($pathinfo === '/addTag') {
                return array (  '_controller' => 'YottaBundle\\Controller\\BookController::addTagToBookAction',  '_route' => 'add_tag_to_book',);
            }

        }

        // book_rating
        if ($pathinfo === '/bookRating') {
            return array (  '_controller' => 'YottaBundle\\Controller\\BookController::rateBookAction',  '_route' => 'book_rating',);
        }

        if (0 === strpos($pathinfo, '/get')) {
            // get_markup
            if (0 === strpos($pathinfo, '/getMarkup') && preg_match('#^/getMarkup/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_markup')), array (  '_controller' => 'YottaBundle\\Controller\\BookController::getMarkupAction',));
            }

            if (0 === strpos($pathinfo, '/getL')) {
                // get_likes
                if (0 === strpos($pathinfo, '/getLikes') && preg_match('#^/getLikes/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_likes')), array (  '_controller' => 'YottaBundle\\Controller\\BookController::getLikesAction',));
                }

                // get_lent
                if (0 === strpos($pathinfo, '/getLent') && preg_match('#^/getLent/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_lent')), array (  '_controller' => 'YottaBundle\\Controller\\BookController::getLentAction',));
                }

            }

        }

        // lent
        if ($pathinfo === '/lent') {
            return array (  '_controller' => 'YottaBundle\\Controller\\BookController::lentAction',  '_route' => 'lent',);
        }

        // discover_timeline
        if ($pathinfo === '/discover') {
            return array (  '_controller' => 'YottaBundle\\Controller\\DiscoverController::timelineAction',  '_route' => 'discover_timeline',);
        }

        // discover_get_yotters_info
        if ($pathinfo === '/getYottersInfo') {
            return array (  '_controller' => 'YottaBundle\\Controller\\DiscoverController::getYottersInfoAction',  '_route' => 'discover_get_yotters_info',);
        }

        // forgot_password
        if ($pathinfo === '/forgot') {
            return array (  '_controller' => 'YottaBundle\\Controller\\ForgotController::indexAction',  '_route' => 'forgot_password',);
        }

        // password_reset
        if ($pathinfo === '/password_reset') {
            return array (  '_controller' => 'YottaBundle\\Controller\\ForgotController::passwordResetAction',  '_route' => 'password_reset',);
        }

        // reset
        if ($pathinfo === '/reset') {
            return array (  '_controller' => 'YottaBundle\\Controller\\ForgotController::resetAction',  '_route' => 'reset',);
        }

        // library
        if ($pathinfo === '/library') {
            return array (  '_controller' => 'YottaBundle\\Controller\\LibraryController::libraryAction',  '_route' => 'library',);
        }

        // filter_book
        if ($pathinfo === '/filterBook') {
            return array (  '_controller' => 'YottaBundle\\Controller\\LibraryController::filterBookAction',  '_route' => 'filter_book',);
        }

        // add_book_to_read
        if (0 === strpos($pathinfo, '/user') && preg_match('#^/user/(?P<user_id>\\d+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'add_book_to_read')), array (  '_controller' => 'YottaBundle\\Controller\\IndividualUserController::indexAction',));
        }

        // add_friend
        if (0 === strpos($pathinfo, '/add') && preg_match('#^/add/(?P<user_id>\\d+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'add_friend')), array (  '_controller' => 'YottaBundle\\Controller\\IndividualUserController::addFriendAction',));
        }

        // pubnub_keys
        if ($pathinfo === '/pubnupKeys') {
            return array (  '_controller' => 'YottaBundle\\Controller\\YottaController::pubnupKeysAction',  '_route' => 'pubnub_keys',);
        }

        // mini_chat
        if ($pathinfo === '/miniChat') {
            return array (  '_controller' => 'YottaBundle\\Controller\\YottaController::miniChatAction',  '_route' => 'mini_chat',);
        }

        // send_message
        if ($pathinfo === '/sendMessage') {
            return array (  '_controller' => 'YottaBundle\\Controller\\YottaController::sendMessageAction',  '_route' => 'send_message',);
        }

        // message
        if ($pathinfo === '/message') {
            return array (  '_controller' => 'YottaBundle\\Controller\\YottaController::getConversationAction',  '_route' => 'message',);
        }

        // conversation
        if ($pathinfo === '/conversation') {
            return array (  '_controller' => 'YottaBundle\\Controller\\YottaController::conversationAction',  '_route' => 'conversation',);
        }

        // uploadImage
        if ($pathinfo === '/uploadImage') {
            return array (  '_controller' => 'YottaBundle\\Controller\\YottaController::uploadImageAction',  '_route' => 'uploadImage',);
        }

        if (0 === strpos($pathinfo, '/mark')) {
            // markRead
            if ($pathinfo === '/markRead') {
                return array (  '_controller' => 'YottaBundle\\Controller\\YottaController::markReadAction',  '_route' => 'markRead',);
            }

            // markAllRead
            if ($pathinfo === '/markAllRead') {
                return array (  '_controller' => 'YottaBundle\\Controller\\YottaController::markAllReadAction',  '_route' => 'markAllRead',);
            }

        }

        // lastUpdatedDay
        if ($pathinfo === '/lastUpdatedDay') {
            return array (  '_controller' => 'YottaBundle\\Controller\\PingController::lastUpdatedDayAction',  '_route' => 'lastUpdatedDay',);
        }

        // add_markup
        if ($pathinfo === '/book/addMarkup') {
            return array (  '_controller' => 'YottaBundle\\Controller\\MarkupController::addMarkupAction',  '_route' => 'add_markup',);
        }

        if (0 === strpos($pathinfo, '/search')) {
            // search_video
            if ($pathinfo === '/searchVideo') {
                return array (  '_controller' => 'YottaBundle\\Controller\\MarkupController::searchVideoAction',  '_route' => 'search_video',);
            }

            // search_image
            if ($pathinfo === '/searchImage') {
                return array (  '_controller' => 'YottaBundle\\Controller\\MarkupController::searchImageAction',  '_route' => 'search_image',);
            }

            // search_wiki
            if ($pathinfo === '/searchWiki') {
                return array (  '_controller' => 'YottaBundle\\Controller\\MarkupController::searchWikiAction',  '_route' => 'search_wiki',);
            }

        }

        // delete_markup
        if ($pathinfo === '/deleteMarkup') {
            return array (  '_controller' => 'YottaBundle\\Controller\\MarkupController::deleteMarkupAction',  '_route' => 'delete_markup',);
        }

        // statistics
        if ($pathinfo === '/statistics') {
            return array (  '_controller' => 'YottaBundle\\Controller\\StatisticsController::statisticsAction',  '_route' => 'statistics',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}

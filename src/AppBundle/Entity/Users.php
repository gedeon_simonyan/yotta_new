<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 *
 * @ORM\Table(name="users", uniqueConstraints={@ORM\UniqueConstraint(name="username", columns={"username"})})
 * @ORM\Entity
 */
class Users
{
    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=50, nullable=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=254, nullable=false)
     */
    private $password = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=254, nullable=false)
     */
    private $email = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="date", nullable=false)
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=true)
     */
    private $status = 'online';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255, nullable=true)
     */
    private $hash;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter_user_id", type="string", length=255, nullable=true)
     */
    private $twitterUserId;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_user_id", type="string", length=255, nullable=true)
     */
    private $facebookUserId;

    /**
     * @var string
     *
     * @ORM\Column(name="google_id", type="string", length=255, nullable=true)
     */
    private $googleId;

    /**
     * @var string
     *
     * @ORM\Column(name="is_active", type="string", nullable=true)
     */
    private $isActive = 'no';

    /**
     * @var string
     *
     * @ORM\Column(name="access_token", type="string", length=255, nullable=true)
     */
    private $accessToken;

    /**
     * @var string
     *
     * @ORM\Column(name="browser", type="string", nullable=true)
     */
    private $browser = 'web';

    /**
     * @var string
     *
     * @ORM\Column(name="profile", type="string", nullable=true)
     */
    private $profile = 'user';

    /**
     * @var string
     *
     * @ORM\Column(name="device_token", type="string", length=2000, nullable=true)
     */
    private $deviceToken;

    /**
     * @var string
     *
     * @ORM\Column(name="allow", type="string", nullable=true)
     */
    private $allow = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="message_allow", type="string", nullable=true)
     */
    private $messageAllow = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_activity_date", type="datetime", nullable=false)
     */
    private $lastActivityDate = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}


<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CoinsStatistic
 *
 * @ORM\Table(name="coins_statistic")
 * @ORM\Entity
 */
class CoinsStatistic
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="coin", type="integer", nullable=false)
     */
    private $coin = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="coin_id", type="integer", nullable=false)
     */
    private $coinId;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}


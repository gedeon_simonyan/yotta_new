<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Coins
 *
 * @ORM\Table(name="coins", uniqueConstraints={@ORM\UniqueConstraint(name="user_type", columns={"user_id"})})
 * @ORM\Entity
 */
class Coins
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="coin", type="integer", nullable=false)
     */
    private $coin = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}


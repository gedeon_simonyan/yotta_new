<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Markups
 *
 * @ORM\Table(name="markups", uniqueConstraints={@ORM\UniqueConstraint(name="content", columns={"content", "book_id"})}, indexes={@ORM\Index(name="creator_id", columns={"creator_id"}), @ORM\Index(name="book_id", columns={"book_id"})})
 * @ORM\Entity
 */
class Markups
{
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=false)
     */
    private $type = 'music';

    /**
     * @var integer
     *
     * @ORM\Column(name="page", type="integer", nullable=true)
     */
    private $page;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=255, nullable=false)
     */
    private $content = '';

    /**
     * @var string
     *
     * @ORM\Column(name="content_type", type="string", length=255, nullable=false)
     */
    private $contentType = 'music';

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modify_date", type="datetime", nullable=true)
     */
    private $modifyDate = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="music_author", type="string", length=255, nullable=false)
     */
    private $musicAuthor = '';

    /**
     * @var float
     *
     * @ORM\Column(name="music_price", type="float", precision=10, scale=0, nullable=false)
     */
    private $musicPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="music_track_url", type="string", length=255, nullable=false)
     */
    private $musicTrackUrl = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="rating", type="integer", nullable=false)
     */
    private $rating;

    /**
     * @var string
     *
     * @ORM\Column(name="music_author_img", type="string", length=255, nullable=false)
     */
    private $musicAuthorImg = '';

    /**
     * @var string
     *
     * @ORM\Column(name="markup_desc", type="text", length=65535, nullable=false)
     */
    private $markupDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="music_genre", type="string", length=255, nullable=false)
     */
    private $musicGenre = '';

    /**
     * @var string
     *
     * @ORM\Column(name="music_type", type="string", nullable=true)
     */
    private $musicType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Users
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
     * })
     */
    private $creator;

    /**
     * @var \AppBundle\Entity\Books
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Books")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="book_id", referencedColumnName="id")
     * })
     */
    private $book;


}


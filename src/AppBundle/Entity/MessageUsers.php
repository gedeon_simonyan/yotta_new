<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MessageUsers
 *
 * @ORM\Table(name="message_users")
 * @ORM\Entity
 */
class MessageUsers
{
    /**
     * @var string
     *
     * @ORM\Column(name="last_message", type="string", length=80, nullable=true)
     */
    private $lastMessage = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="unread_messages_count", type="integer", nullable=false)
     */
    private $unreadMessagesCount = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="datetime", nullable=false)
     */
    private $modifiedDate = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="friend_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $friendId;


}


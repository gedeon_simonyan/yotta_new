<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LentBooks
 *
 * @ORM\Table(name="lent_books", uniqueConstraints={@ORM\UniqueConstraint(name="unique_field", columns={"user_id", "lent_to", "book_id"})})
 * @ORM\Entity
 */
class LentBooks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="lent_to", type="integer", nullable=false)
     */
    private $lentTo;

    /**
     * @var string
     *
     * @ORM\Column(name="book_id", type="string", length=120, nullable=false)
     */
    private $bookId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lend_date", type="date", nullable=true)
     */
    private $lendDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="back_date", type="date", nullable=true)
     */
    private $backDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}


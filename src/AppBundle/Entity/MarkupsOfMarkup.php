<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MarkupsOfMarkup
 *
 * @ORM\Table(name="markups_of_markup")
 * @ORM\Entity
 */
class MarkupsOfMarkup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="markup_id", type="integer", nullable=false)
     */
    private $markupId;

    /**
     * @var string
     *
     * @ORM\Column(name="markup_type", type="string", nullable=true)
     */
    private $markupType = 'text';

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=true)
     */
    private $createDate = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}


<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Subscribers
 *
 * @ORM\Table(name="subscribers", uniqueConstraints={@ORM\UniqueConstraint(name="user_publisher", columns={"user_id", "publisher_id"})})
 * @ORM\Entity
 */
class Subscribers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="publisher_id", type="integer", nullable=true)
     */
    private $publisherId;

    /**
     * @var string
     *
     * @ORM\Column(name="genre", type="string", length=120, nullable=true)
     */
    private $genre;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}


<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Likes
 *
 * @ORM\Table(name="likes", uniqueConstraints={@ORM\UniqueConstraint(name="unique_markups", columns={"user_id", "markup_id"})}, indexes={@ORM\Index(name="markup_id", columns={"markup_id"}), @ORM\Index(name="IDX_49CA4E7DA76ED395", columns={"user_id"})})
 * @ORM\Entity
 */
class Likes
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="like", type="boolean", nullable=false)
     */
    private $like;

    /**
     * @var integer
     *
     * @ORM\Column(name="book_id", type="integer", nullable=true)
     */
    private $bookId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="datetime", nullable=false)
     */
    private $modifiedDate = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Users
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \AppBundle\Entity\Markups
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Markups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="markup_id", referencedColumnName="id")
     * })
     */
    private $markup;


}


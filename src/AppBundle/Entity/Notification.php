<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @ORM\Table(name="notification")
 * @ORM\Entity
 */
class Notification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="book_id", type="integer", nullable=true)
     */
    private $bookId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="markup_id", type="integer", nullable=true)
     */
    private $markupId;

    /**
     * @var string
     *
     * @ORM\Column(name="notification", type="string", length=255, nullable=true)
     */
    private $notification;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="markup_type", type="string", length=255, nullable=true)
     */
    private $markupType;

    /**
     * @var string
     *
     * @ORM\Column(name="note_status", type="string", nullable=true)
     */
    private $noteStatus = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="friend_id", type="integer", nullable=true)
     */
    private $friendId;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}


<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Books
 *
 * @ORM\Table(name="books", uniqueConstraints={@ORM\UniqueConstraint(name="book_id", columns={"book_id"})})
 * @ORM\Entity
 */
class Books
{
    /**
     * @var string
     *
     * @ORM\Column(name="book_id", type="string", length=100, nullable=false)
     */
    private $bookId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=200, nullable=true)
     */
    private $title = 'book dummy';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=240, nullable=true)
     */
    private $image = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="publisher", type="string", length=100, nullable=true)
     */
    private $publisher = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publish_date", type="date", nullable=true)
     */
    private $publishDate;

    /**
     * @var string
     *
     * @ORM\Column(name="authors", type="string", length=240, nullable=true)
     */
    private $authors = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="date", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=50, nullable=true)
     */
    private $language = 'english';

    /**
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=255, nullable=true)
     */
    private $tags = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="string", length=50, nullable=true)
     */
    private $price = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="book_url", type="string", length=255, nullable=true)
     */
    private $bookUrl = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="likes", type="integer", nullable=true)
     */
    private $likes = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="markups", type="integer", nullable=true)
     */
    private $markups = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}


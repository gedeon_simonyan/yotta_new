<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CoinPoints
 *
 * @ORM\Table(name="coin_points")
 * @ORM\Entity
 */
class CoinPoints
{
    /**
     * @var string
     *
     * @ORM\Column(name="purpose_dipslay", type="string", length=255, nullable=false)
     */
    private $purposeDipslay = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="coin", type="integer", nullable=false)
     */
    private $coin;

    /**
     * @var string
     *
     * @ORM\Column(name="purpose", type="string", length=20, nullable=false)
     */
    private $purpose = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}


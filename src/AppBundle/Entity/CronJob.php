<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CronJob
 *
 * @ORM\Table(name="cron_job")
 * @ORM\Entity
 */
class CronJob
{
    /**
     * @var integer
     *
     * @ORM\Column(name="offset", type="integer", nullable=false)
     */
    private $offset;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}


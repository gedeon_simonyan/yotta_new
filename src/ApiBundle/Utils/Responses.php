<?php
namespace ApiBundle\Utils;

use Symfony\Component\HttpFoundation\JsonResponse;

class Responses
{
    public static function apiResponse($data = null, $code = 200, $status = 'success')
    {
        $output = [
            'status' => $status,
            'data' => $data,
        ];
        $response = new JsonResponse($output, $code);

        return $response;
    }
}

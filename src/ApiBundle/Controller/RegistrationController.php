<?php

namespace ApiBundle\Controller;

use Abraham\TwitterOAuth\TwitterOAuth;
use Abraham\TwitterOAuth\TwitterOAuthException;
use ApiBundle\Entity\ApiUsers;
use ApiBundle\Entity\Invitations;
use ApiBundle\Entity\UserInfo;
use ApiBundle\Services\Validator;
use ApiBundle\Utils\ErrorMessages;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ApiBundle\Utils\Responses;
use Swift_Message;
use TwitterOAuth\Exception\TwitterException;
use GuzzleHttp\Exception\RequestException;

class RegistrationController extends Controller
{

    public function registrationAction(Request $request)
    {
        $firstName = $request->get('first_name');
        $lastName = $request->get('last_name');
        $username = $request->get('username');
        $email = $request->get('email');
        $password = $request->get('password');

        /**
         * add user to api_users
         * @return / bool
         * @return / int
         */
        $hash = sha1('user'.$username.'yotta');

        $apiUserId = $this->register($firstName, $lastName, $username, $email, $password, $hash);

        if (!isset($apiUserId['errors'])) {

            $confirmation_link = $this->getParameter('server_url').'/activate_account?h='.$hash;
            $locale = $request->getLocale();
            $template = 'registration-'.$locale;
            $subject = $this->get('translator')->trans('new_registration');
            $status = $this->sendMessage($email, $template, $subject, $confirmation_link, $firstName.' '.$lastName);

            if ($status) {
                $entityManager = $this->getDoctrine()->getManager();
                $invitation = $entityManager->getRepository('ApiBundle:Invitations')->findOneBy(['email'=>$email]);
                if($invitation){
                    $entityManager->remove($invitation);
                    $entityManager->flush();
                }

                $data = ["is_active" => "no", "message" => "verify_email",'user_id'=>$apiUserId];

                return Responses::apiResponse($data);

            } else {
                $data['message'] = "send_email_problem";

                return Responses::apiResponse($data, ErrorMessages::FAILURE_CODE, ErrorMessages::FAILURE_MESSAGE);
                // resend confirmation link
            }
        } else {

            return Responses::apiResponse(
                $apiUserId['errors'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
    }

    public function resendEmailAction($userId, Request $request){
        $userManager = $this->get('fos_user.user_manager');
        $user_exist = $userManager->findUserBy(['id'=>$userId]);

        if ($user_exist) {
            $locale = $request->getLocale();
            $template = 'registration-'.$locale;
            $subject = $this->get('translator')->trans('new_registration');
            $confirmation_link = $this->getParameter('server_url').'/activate_account?h='.$user_exist->getConfirmationToken();
            $entityManager = $this->getDoctrine()->getManager();
            $userInfo = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(['userId'=>$userId]);
            $status = $this->sendMessage($user_exist->getEmail(), $template, $subject, $confirmation_link, $userInfo->getFirstname().' '.$userInfo->getLastname());

            if($status){
                $data = ["is_active" => "no", "message" => "verify_email",'user_id'=>$userId];

                return Responses::apiResponse($data);
            }
            $data['message'] = "send_email_problem";

            return Responses::apiResponse($data, ErrorMessages::FAILURE_CODE, ErrorMessages::FAILURE_MESSAGE);
        }
        $data['message'] = "user_not_exists";

        return Responses::apiResponse($data, ErrorMessages::FAILURE_CODE, ErrorMessages::FAILURE_MESSAGE);
    }

    public function sendResetEmailAction(Request $request)
    {

        $email = $request->get('email');
        $userManager = $this->get('fos_user.user_manager');
        $checkEmail = $userManager->findUserByEmail($email);

        if ($checkEmail && $checkEmail->isEnabled()) {
            $userId = $checkEmail->getId();
            $userHash = md5($userId.'_'.uniqid());
            $confirmation_link = $this->getParameter('server_url').'/reset?u='.$userHash;
            $locale = $request->getLocale();
            $template = 'forgot-'.$locale;
            $subject = $this->get('translator')->trans('reset_password');
            $entityManager = $this->getDoctrine()->getManager();
            $userInfo = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(['userId'=>$userId]);

            if ($this->sendMessage($email, $template, $subject, $confirmation_link, $userInfo->getFirstname().' '.$userInfo->getLastname())) {

                $date = new \DateTime();
                $date->modify('+15 minutes');
                $addTime = $date->format("Y-m-d H:i:s");
                $this->get('api.repository.reset')->deleteExpiredPasswords();
                $this->get('api.repository.reset')->insertResetPassword($email, $userHash, $addTime);


                return Responses::apiResponse(
                    [
                        'is_send' => true,
                        'message' => 'reset_email_send', 'email' => $email,
                    ]
                );
            }

            return Responses::apiResponse(
                ['is_send' => false, 'message' => 'error_sending_email'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

        return Responses::apiResponse(
            ['email_exists' => false, 'message' => 'not_existing_email'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );

    }

    public function checkEmailAction(Request $request)
    {
        $email = $request->get('email');
        $firstname = $request->get('first_name');
        $lastname = $request->get('last_name');
        $userManager = $this->get('fos_user.user_manager');

        $errors = [];
        $Validator = new Validator();
        if (!$Validator->validateName(trim($firstname))) {
            $errors["message"] = 'firstname_validation';
        }

        if (!$Validator->validateName(trim($lastname))) {
            $errors["message"] = 'lastname_validation';
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors["message"] = 'email_invalid';

        }

        if(!empty($errors)){
            return Responses::apiResponse(
                $errors,
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
        $checkEmail = $userManager->findUserByEmail($email);
        if ($checkEmail) {

                return Responses::apiResponse(
                    ['message' => 'email_exists'],
                    ErrorMessages::FAILURE_CODE,
                    ErrorMessages::FAILURE_MESSAGE
                );
            }
            return Responses::apiResponse(['validate'=>true]);
    }

    public function resetAction(Request $request)
    {
        $password = $request->get('password');
        $resId = $request->get('hash');

        $Validator = new Validator();

        $email = $this->get('api.repository.reset')->getReset($resId);
        if ($email) {
            $validPassword = $Validator->validatePassword($password);
            if (!$validPassword) {
                return Responses::apiResponse(
                    ['message' => 'password_validation'],
                    ErrorMessages::FAILURE_CODE,
                    ErrorMessages::FAILURE_MESSAGE
                );
            }
            $upPass = $this->updatePassword($email, $password);

            if ($upPass) {
                $this->get('api.repository.reset')->deleteResetPassword($resId);
            }

            return Responses::apiResponse(['message' => 'psw_success_updated']);
        } else {

            return Responses::apiResponse(
                ['message' => 'req_expired'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
    }

    public function checkResetHashAction($hash,Request $request)
    {
        $email = $this->get('api.repository.reset')->getReset($hash);
        if ($email) {

            return Responses::apiResponse(['valid_hash' => true]);
        } else {

            return Responses::apiResponse(
                ['message' => 'req_expired'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
    }

    public function activateAccountAction(Request $request)
    {

        $hash = $request->get('h');
        $userManager = $this->get('fos_user.user_manager');

        $userData = $userManager->findUserByConfirmationToken($hash);
        if ($userData) {
            $isUserActive = $userData->isEnabled();
            if ($isUserActive) {

                return Responses::apiResponse(
                    ['is_active' => true, 'message' => 'already_activated'],
                    ErrorMessages::FAILURE_CODE,
                    ErrorMessages::FAILURE_MESSAGE
                );
            }
            $userData->setEnabled(1);
            $userManager->updateUser($userData);


            return Responses::apiResponse(
                ['is_active' => true, 'message' => 'user_activated']
            );
        }

        return Responses::apiResponse(
            ['is_active' => false, 'message' => 'user_not_exists'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function SocialLoginAction($social, Request $request)
    {

        $socialId = $request->get('user_id');
        $email = $request->get('email');
        $firstName = $request->get('first_name');
        $lastName = $request->get('last_name');
        $image = $request->get('image');

        $userManager = $this->get('fos_user.user_manager');
        $client = $this->get('doctrine.orm.entity_manager')->getRepository('ApiBundle:Client')->findOneBy(
            [],
            ['id' => 'DESC']
        );

        $userExists = $userManager->findUserBy([$social.'UserId' => $socialId]);
        $firstLogin = 0;
        $entityManager = $this->getDoctrine()->getManager();
        if (!$userExists) {
            $firstLogin = 1;
            $date = new \DateTime();
            $registered = $this->registerSocial($socialId, $email, $social);

            if (is_array($registered) && isset($registered['errors'])) {
                return Responses::apiResponse(
                    $registered['errors'],
                    ErrorMessages::FAILURE_CODE,
                    ErrorMessages::CONFIRM_MESSAGE
                );
            }

            if(!$registered['exists']){
                $userInfo = new UserInfo();
                $userInfo->setUserId($registered['user']);
                $userInfo->setFirstname($firstName);
                $userInfo->setLastname($lastName);
                $userInfo->setPicture($image);
                $userInfo->setLastActivityDate($date);
                $entityManager->persist($userInfo);
                $entityManager->flush();
            }

        }

        $invitation = $entityManager->getRepository('ApiBundle:Invitations')->findOneBy(['email'=>$email]);
        if($invitation){
            $entityManager->remove($invitation);
            $entityManager->flush();
        }

        $user = $userManager->findUserBy([$social.'UserId' => $socialId]);
        $oauth2server = $this->get('fos_oauth_server.server');
        $accessToken = $oauth2server->createAccessToken($client, $user, '', 86400);
        $accessToken['first_login'] = $firstLogin;

        return new JsonResponse($accessToken);
    }

    public function confirmSocialLoginAction($social, Request $request)
    {
        $socialId = $request->get('user_id');
        $email = $request->get('email');
        $userManager = $this->get('fos_user.user_manager');
        $client = $this->get('doctrine.orm.entity_manager')->getRepository('ApiBundle:Client')->findOneBy(
            [],
            ['id' => 'DESC']
        );

        $userExists = $userManager->findUserBy([$social.'UserId' => $socialId]);

        if (!$email && !$socialId) {
            return Responses::apiResponse(
                'email_social_required',
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
        $firstLogin = 0;
        if (!$userExists) {
            $firstLogin = 1;
            $registered = $this->updateSocial($socialId, $email, $social);

            if (is_array($registered) && isset($registered['errors'])) {
                return Responses::apiResponse(
                    $registered['errors'],
                    ErrorMessages::FAILURE_CODE,
                    ErrorMessages::CONFIRM_MESSAGE
                );
            }
        }
        $user = $userManager->findUserBy([$social.'UserId' => $socialId]);
        $oauth2server = $this->get('fos_oauth_server.server');
        $accessToken = $oauth2server->createAccessToken($client, $user, 'user', 86400);

        $accessToken['first_login'] = $firstLogin;

        return new JsonResponse($accessToken);
    }

    public function logoutAction(Request $request)
    {
        $token = $request->get('token');
        if(!$token){
            return Responses::apiResponse(
                ['message' => 'token_required'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::CONFIRM_MESSAGE
            );
        }
        $this->get('api.repository.user')->deleteAccessToken($token);

        return Responses::apiResponse(['logged_out' => true]);
    }

    public function twitterAction(Request $request)
    {

        $connection = new TwitterOAuth(
            ErrorMessages::TWITTER_CONSUMER_KEY,
            ErrorMessages::TWITTER_CONSUMER_SECRET
        );
        try {
            $request_token = $connection->oauth(
                'oauth/request_token',
                ['oauth_callback' => $this->getParameter('server_url').'/']
            ); //get Request Token
            $request_token['oauth_url'] = $connection->url(
                'oauth/authorize',
                ['oauth_token' => $request_token['oauth_token']]
            );

            return Responses::apiResponse($request_token);

        } catch (TwitterOAuthException $e) {

            return Responses::apiResponse(
                ['message' => 'unable_get_oauth'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

    }

    public function twitterLoginAction(Request $request)
    {

        $connection = new TwitterOAuth(
            ErrorMessages::TWITTER_CONSUMER_KEY,
            ErrorMessages::TWITTER_CONSUMER_SECRET,
            $request->get('request_token'),
            $request->get('request_token_secret')
        );
        try {
            $request_token = $connection->oauth(
                'oauth/access_token',
                ['oauth_verifier' => $request->get('oauth_verifier')]
            );
            $connection->setOauthToken($request_token['oauth_token'], $request_token['oauth_token_secret']);
            $userInfo = $connection->get('account/verify_credentials');
            $entityManager = $this->getDoctrine()->getManager();
            $userExists = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(
                ['twitterUserId' => $userInfo->id]
            );
            $client = $this->get('doctrine.orm.entity_manager')->getRepository('ApiBundle:Client')->findOneBy(
                [],
                ['id' => 'DESC']
            );
            $firstLogin = 0;
            if (!$userExists) {
                $firstLogin = 1;
                $apiUsers = new ApiUsers();
                $apiUsers->setTwitterUserId($userInfo->id);
                $apiUsers->setEnabled(1);
                $apiUsers->setUsername($userInfo->screen_name);
                $apiUsers->setUsernameCanonical($userInfo->screen_name);
                $entityManager->persist($apiUsers);
                $entityManager->flush();

                $userData = new UserInfo();
                $userData->setUserId($apiUsers->getId());
                $userData->setPicture($userInfo->profile_image_url_https);
                $userData->setFirstname($userInfo->name);
                $userData->setLastActivityDate(new \DateTime());
                $entityManager->persist($userData);
                $entityManager->flush();
            }
            $user = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(
                ['twitterUserId' => $userInfo->id]
            );
            $oauth2server = $this->get('fos_oauth_server.server');
            $accessToken = $oauth2server->createAccessToken($client, $user, 'user', 86400);
            $accessToken['first_login'] = $firstLogin;

            return new JsonResponse($accessToken);

        } catch (TwitterOAuthException $e) {

            return Responses::apiResponse(
                ['message' => 'unable_get_oauth'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

    }

    public function getSocialTokenAction($social, Request $request){
        $code =$request->get('code');
        $browser = new Client();
        switch ($social){
            case 'pinterest':
                try {
                    $params = [
                        'grant_type'=>'authorization_code',
                        'client_id'=>ErrorMessages::PINTEREST_CLIENT_ID,
                        'client_secret'=>ErrorMessages::PINTEREST_CLIENT_SECRET,
                        'code'=>$code,
                    ];
                    $response = $browser->post(
                        ErrorMessages::PINTEREST_URL.'v1/oauth/token?'.http_build_query($params)
                    );

                    $token = json_decode($response->getBody()->getContents());

                    if(isset($token->access_token)){
                        return Responses::apiResponse(['access_token'=>$token->access_token]);

                    }
                    return Responses::apiResponse(['message' => 'error']);



                } catch (RequestException $e) {
                    return Responses::apiResponse(['message' => json_decode($e->getResponse()->getBody()->getContents())->error],
                        ErrorMessages::FAILURE_CODE,
                        ErrorMessages::FAILURE_MESSAGE);
                }
                break;
            case 'facebook':
                try {
                    $params = [
                        'redirect_uri'=>'https://test.yottabook.com/',
                        'client_id'=>ErrorMessages::FB_APP_ID,
                        'client_secret'=>ErrorMessages::FB_APP_SECRET,
                        'code'=>$code,
                    ];
                    $response = $browser->get(
                        ErrorMessages::FB_APP_URL.'/oauth/access_token?'.http_build_query($params)
                    );

                    $token = json_decode($response->getBody()->getContents());

                    if(isset($token->access_token)){
                        return Responses::apiResponse(['access_token'=>$token->access_token]);

                    }
                    return Responses::apiResponse(['message' => 'error'],
                        ErrorMessages::FAILURE_CODE,
                        ErrorMessages::FAILURE_MESSAGE);


                } catch (RequestException $e) {

                    return Responses::apiResponse(['message' => json_decode($e->getResponse()->getBody()->getContents())->error->message],
                        ErrorMessages::FAILURE_CODE,
                        ErrorMessages::FAILURE_MESSAGE);
                }

                break;
        }
        return Responses::apiResponse(['message' => 'error'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE);
    }

    private function registerSocial($socialId, $email, $social)
    {

        $userManager = $this->get('fos_user.user_manager');
        $usernameExist = $userManager->findUserByUsername($socialId);
        $emailExist = $userManager->findUserByEmail($email);
        $errors = array();
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors["message"] = 'email_invalid';

        }
        if ($usernameExist) {
            $errors["message"] = 'username_exists';
        }

        if (empty($errors)) {
            if($emailExist){
                $user = $userManager->findUserByEmail($email);
                $user->setEnabled(1);
            }else{
                $user = $userManager->createUser();
            }

            switch ($social) {
                case 'facebook':
                    $user->setFacebookUserId($socialId);
                    break;
                case 'twitter':
                    $user->setTwitterUserId($socialId);
                    break;
                case 'pinterest':
                    $user->setPinterestUserId($socialId);
                    break;
                case 'instagram':
                    $user->setInstagramUserId($socialId);
                    break;
            }
            if(!$emailExist){
                $user->setEmail($email);
                $user->setEmailCanonical($email);
                $user->setUsername($socialId);
                $user->setEnabled(1); // enable the user or enable it later with a confirmation token in the email
                // this method will encrypt the password with the default settings :)
            }


            $userManager->updateUser($user);
            $userId = $userManager->findUserByEmail($email);

            return ['user'=>$userId->getId(),'exists'=>$emailExist];
        } else {
            return ['errors' => $errors];
        }
    }

    private function updateSocial($socialId, $email, $social)
    {

        $userManager = $this->get('fos_user.user_manager');

        $emailExist = $userManager->findUserByEmail($email);

        if ($emailExist) {

            switch ($social) {
                case 'facebook':
                    $emailExist->setFacebookUserId($socialId);
                    break;
                case 'twitter':
                    $emailExist->setTwitterUserId($socialId);
                    break;
                case 'instagram':
                    $emailExist->setInstagramUserId($socialId);
                    break;
                case 'pinterest':
                    $emailExist->setPinterestUserId($socialId);
                    break;
            }

            $userManager->updateUser($emailExist);
            $userId = $userManager->findUserByUsername($socialId);

            return $userId;
        } else {
            return ['errors' => ['email' => 'user_not_exists']];
        }
    }

    private function updatePassword($email, $password)
    {
        $userManager = $this->get('fos_user.user_manager');

        $user_exist = $userManager->findUserByEmail($email);
        if ($user_exist) {
            $user_exist->setPlainPassword($password);
            $userManager->updateUser($user_exist);

            return true;
        }

        return false;

    }


    /**
     * This method registers an user in the database manually.
     *
     * @return boolean|array User registered / not registered
     **/
    private function register($firstname, $lastname, $username, $email, $password, $hash)
    {
        $userManager = $this->get('fos_user.user_manager');
        $manager = $this->getDoctrine()->getManager();
        // Or you can use the doctrine entity manager if you want instead the fosuser manager
        // to find
        //$em = $this->getDoctrine()->getManager();
        //$usersRepository = $em->getRepository("mybundleuserBundle:User");
        // or use directly the namespace and the name of the class
        // $usersRepository = $em->getRepository("mybundle\userBundle\Entity\User");
        //$email_exist = $usersRepository->findOneBy(array('email' => $email));

        $username_exist = $userManager->findUserByUsername($username);
        $email_exist = $userManager->findUserByEmail($email);

        $errors = [];
        $last_inserted_user_id = null;
        $username = trim($username);
        $email = trim($email);

        $Validator = new Validator();
        if ($username_exist) {
            $errors["message"] = 'username_exists';
        }
        if ($email_exist) {
            $errors["message"] = 'email_exists';
        }
        if (!$Validator->validateName(trim($firstname))) {
            $errors["message"] = 'firstname_validation';
        }

        if (!$Validator->validateName(trim($lastname))) {
            $errors["message"] = 'lastname_validation';
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors["message"] = 'email_invalid';

        }

        $validPassword = $Validator->validatePassword($password);
        if (!$validPassword) {
            $errors["message"] = 'password_validation';
        }

        $validUsername = $Validator->validateUsername($username);
        if (!$validUsername) {
            $errors["message"] = 'username_validation';
        }

        if (empty($errors)) {


            $user = $userManager->createUser();
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setEmailCanonical($email);
            $user->setConfirmationToken($hash);
//        $user->setLocked(0); // don't lock the user
            $user->setEnabled(0); // enable the user or enable it later with a confirmation token in the email
            // this method will encrypt the password with the default settings :)
            $user->setPlainPassword($password);
            $userManager->updateUser($user);
            $userId = $userManager->findUserByUsername($username)->getId();
            $users = new UserInfo();
            $users->setUserId($userId);
            $users->setLastActivityDate(new \DateTime());
            $users->setFirstname($firstname);
            $users->setLastname($lastname);

            $manager->persist($users);
            $manager->flush();

            return $userId;
        }

        return ['errors' => $errors];
    }


    public function sendInvitationAction(Request $request){
        $email = $request->get('email');
        $userId = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();

        $emailExists = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(['email'=>$email]);
        if($emailExists){
            return Responses::apiResponse(
                ['message' => 'user_is_registered'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

        $invitationExists = $entityManager->getRepository('ApiBundle:Invitations')->findOneBy(['email'=>$email]);
        if($invitationExists){
            return Responses::apiResponse(
                ['message' => 'someone_already_invite'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

        $link = $this->getParameter('server_url').'/';
        $locale = $request->getLocale();
        $template = 'invitation-'.$locale;
        $subject = $this->get('translator')->trans('invitation');
        $userData = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(['userId'=>$userId]);
        $username = $userData?$userData->getFirstname().' '.$userData->getLastname():$this->getUser()->getUsername();
        $message = $this->sendMessage($email,$template,$subject,$link,$username);

        if($message){
            $invitation = new Invitations();
            $invitation->setUserId($userId);
            $invitation->setEmail($email);
            $invitation->setCreateDate(new \DateTime());

            $entityManager->persist($invitation);
            $entityManager->flush();

            return Responses::apiResponse(['send'=>true]);
        }
        return Responses::apiResponse(
            ['message' => 'email_not_send'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function sendMessage($email, $template, $subject, $link, $username)
    {

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($this->getParameter('mailer_user'))
            ->setTo($email)
            ->setBody(
                $this->renderView(
                    'Emails/'.$template.'.html.twig',
                    array(
                        'link' => $link,
                        'name' => $username,
                    )
                ),
                'text/html'
            );
        $success = $this->get('mailer')->send($message);

        if ($success == 1) {
            return true;
        }

        return false;
    }

}
<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Messages;
use ApiBundle\Entity\MessageUsers;
use ApiBundle\Utils\ErrorMessages;
use ApiBundle\Utils\Responses;
use Pubnub\Pubnub;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ChatController extends Controller
{
    public function markAsReadAction()
    {
        $userId = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $messages = $entityManager->getRepository('ApiBundle:Messages')->findBy(['friendId' => $userId]);

        if (empty($messages)) {
            $output = ['message' => 'msg_not_found'];
            $status = ErrorMessages::FAILURE_MESSAGE;
            $code = ErrorMessages::FAILURE_CODE;
        } else {
            $stat = 'read';
            foreach ($messages as $read) {
                $read->setStatus($stat);
                $entityManager->persist($read);
            }

            $entityManager->flush();
            $output = ['marked'=>true];
            $status = ErrorMessages::SUCCESS_MESSAGE;
            $code = 200;
        }
        return Responses::apiResponse($output, $code, $status);
    }

    public function getFriendAction(Request $request)
    {
        $offset = $request->query->get('o');
        $limit = $request->query->get('l');
        $userId = $this->getUser()->getId();
        $chatInfo = $this->get('api.repository.chat')->getChatUsers($userId, $limit?$limit:10, $offset?$offset:0);
        if ($chatInfo) {
            return Responses::apiResponse($chatInfo);
        }
        return Responses::apiResponse(
            ['message' => 'no_chat_friends'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function getMessageAction($friendId, Request $request){

        $offset = intval($request->query->get('o'));
        $limit = intval($request->query->get('l'));
        $UserId = $this->getUser()->getId();
        $msg = $this->get('api.repository.chat')->getMessage($friendId, $UserId, $offset, $limit);

        if ($msg){
            return Responses::apiResponse($msg);
        }
        return Responses::apiResponse(
            ['message' => 'no_messages'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE);
    }

    public function sendMessageAction($friendId, Request $request){

        $userId = $this->getUser()->getId();
        $text = strip_tags(addslashes($request->get('message')));
        $type = $request->get('type');
        $entityManager = $this->getDoctrine()->getManager();
        $checkUserFriends = $this->get('api.repository.user')->checkFriend($userId,$friendId);

        $userExists = $entityManager->getRepository('ApiBundle:ApiUsers')->find($friendId);
        if(!$userExists){
            return Responses::apiResponse(
                ['message' => 'user_not_exists'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE);
        }

        if(!$checkUserFriends){
            return Responses::apiResponse(
                ['message' => 'users_are_not_friends'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE);
        }
        $date = new \DateTime();

        if($type == 'image'){
            try {
                $file = $request->files->get ( 'message' );
                $fileName = md5 ( uniqid () ) . '.' . $file->guessExtension ();

                $year = $date->format('Y');
                $month = $date->format('m');
                $filePath = $this->getParameter ( 'message_image_url' ).'/'.$year.'/'.$month.'/'.$userId;
                mkdir($this->getParameter ( 'message_image_url' ).'/'.$year,0777);
                mkdir($this->getParameter ( 'message_image_url' ).'/'.$year.'/'.$month,0777);
                mkdir($filePath,0777);
                $file->move( $filePath, $fileName );
                $text = 'img/chat/'.$year.'/'.$month.'/'.$userId.'/'.$fileName;

            } catch ( \Exception $e ) {

                return Responses::apiResponse(['message' => 'file_not_uploaded'],
                    ErrorMessages::FAILURE_CODE,
                    ErrorMessages::FAILURE_MESSAGE);
            }
        }

        $message = new Messages();
        $message->setFriendId($friendId);
        $message->setUserId($userId);
        $message->setMessage($text);
        $message->setStatus('unread');
        $message->setType($type);
        $message->setChatDate($date);

        $entityManager->persist($message);
        $messageUsers = $entityManager->getRepository('ApiBundle:MessageUsers')->findOneBy(['userId'=>$userId,'friendId'=>$friendId]);
        $messageFriend = $entityManager->getRepository('ApiBundle:MessageUsers')->findOneBy(['userId'=>$friendId,'friendId'=>$userId]);

        if($messageUsers && $messageFriend){
            $messageUsers->setUnreadMessagesCount(0);

            $messageFriend->setUnreadMessagesCount($messageFriend->getUnreadMessagesCount()+1);
        }else{
            $messageUsers = new MessageUsers();
            $messageUsers->setUserId($userId);
            $messageUsers->setFriendId($friendId);
            $messageUsers->setUnreadMessagesCount(0);

            $messageFriend = new MessageUsers();
            $messageFriend->setUserId($friendId);
            $messageFriend->setFriendId($userId);
            $messageFriend->setUnreadMessagesCount(1);
        }

        $messageUsers->setLastMessage($text);
        $messageUsers->setModifiedDate($date);
        $messageFriend->setLastMessage($text);
        $messageFriend->setModifiedDate($date);

        $entityManager->persist($messageUsers);
        $entityManager->persist($messageFriend);
        $entityManager->flush();

        $lastMessageId = $message->getId();
        $repository = $this->get('api.repository.chat');
        $unreadCount = $repository->getUnreadMessageUsersCount($userId);
        $unreadCountFriend = $repository->getUnreadMessageUsersCount($friendId);

        $lastMessage = $repository->getMessageById($lastMessageId);

        $messageData = array('m_count' => 1, 'sender' => $userId, 'last_mess' => $lastMessage);
        $pubnub = new Pubnub(ErrorMessages::PUBNUP_PUBLISH_KEY, ErrorMessages::PUBNUP_SUBSCRIBE_KEY);
        $messageData["unread_messages_count"] = $unreadCount;
        $pubnub->publish('oo-chat-'.$userId, $messageData);
        $messageData["unread_messages_count"] = $unreadCountFriend;
        $pubnub->publish('oo-chat-'.$friendId, $messageData);

        return Responses::apiResponse($lastMessage);

    }
}

<?php

namespace ApiBundle\Controller;

use Abraham\TwitterOAuth\TwitterOAuth;
use ApiBundle\Entity\Friends;
use ApiBundle\Entity\Notification;
use ApiBundle\Entity\Posts;
use ApiBundle\Entity\UserInfo;
use ApiBundle\Services\Validator;
use ApiBundle\Utils\ErrorMessages;
use ApiBundle\Utils\Responses;
use Buzz\Exception\RequestException;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;


class FriendsController extends Controller
{

    public function getUserSocialFriendsAction($social, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $token = $request->get('token');

        $userId = $this->getUser()->getId();
        switch ($social) {
            case 'facebook':

                try {
                    $fb = new Facebook(
                        ['app_id' => ErrorMessages::FB_APP_ID, 'app_secret' => ErrorMessages::FB_APP_SECRET]
                    );
                    $responses = $fb->get('/me/friends?fields=id,name,installed,picture', $token);

                    if (!empty($responses->getBody())) {
                        $friends = json_decode($responses->getBody())->data;
                        $output = array();

                        foreach ($friends as $key => $friend) {
                            $userFriendId = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(
                                ['facebookUserId' => $friend->id]
                            );
                            $areUsersFriends = false;
                            if ($userFriendId) {
                                $areUsersFriends = $this->get('api.repository.user')->checkFriend(
                                    $userId,
                                    $userFriendId->getId()
                                );
                            }

                            if ($userFriendId && !$areUsersFriends) {

                                $output[$key]['id'] = $userFriendId->getId();
                                $output[$key]['social_id'] = $friend->id;
                                $output[$key]['name'] = $friend->name;
                                $output[$key]['image'] = $friend->picture->data->url;
                            }

                        }
                        if ($output) {
                            return Responses::apiResponse(array_values($output));
                        }

                        return Responses::apiResponse(
                            ['message' => 'friend_not_found'],
                            ErrorMessages::FAILURE_CODE,
                            ErrorMessages::FAILURE_MESSAGE
                        );

                    }

                    return Responses::apiResponse(
                        ['message' => 'friend_not_found'],
                        ErrorMessages::FAILURE_CODE,
                        ErrorMessages::FAILURE_MESSAGE
                    );
                } catch (FacebookSDKException $e) {
                    // When Graph returns an error

                    return Responses::apiResponse(
                        [
                            'message' =>
                                'not_connected_facebook',
                        ],
                        ErrorMessages::FAILURE_CODE,
                        ErrorMessages::FAILURE_MESSAGE
                    );
                }
                break;
            case 'twitter':

                $username = $this->getUser()->getUsername();

                $twitterOauth = new TwitterOAuth(
                    ErrorMessages::TWITTER_CONSUMER_KEY,
                    ErrorMessages::TWITTER_CONSUMER_SECRET
                );
                $friends = $twitterOauth->get(
                    "friends/list",
                    ['screen_name' => $username]
                );

                if (!empty($friends->users)) {
                    $output = array();
                    foreach ($friends->users as $key => $friend) {
                        $userFriendId = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(
                            ['twitterUserId' => $friend->id]
                        );
                        $areUsersFriends = false;
                        if ($userFriendId) {
                            $areUsersFriends = $this->get('api.repository.user')->checkFriend(
                                $userId,
                                $userFriendId->getId()
                            );
                        }
                        if ($userFriendId && !$areUsersFriends) {
                            $output[$key]['id'] = $userFriendId ? $userFriendId->getId() : null;
                            $output[$key]['social_id'] = $friend->id;
                            $output[$key]['name'] = $friend->screen_name;
                            $output[$key]['image'] = $friend->profile_image_url_https;
                        }
                    }

                    if ($output) {
                        return Responses::apiResponse(array_values($output));
                    }

                    return Responses::apiResponse(
                        ['message' => 'friend_not_found'],
                        ErrorMessages::FAILURE_CODE,
                        ErrorMessages::FAILURE_MESSAGE
                    );
                }

                return Responses::apiResponse(
                    ['message' => 'friend_not_found'],
                    ErrorMessages::FAILURE_CODE,
                    ErrorMessages::FAILURE_MESSAGE
                );

                break;
            case 'pinterest':
                $browser = new Client();
                try {
                    $response = $browser->get(
                        ErrorMessages::PINTEREST_URL.'v1/me/following/users/?access_token='.$token.'&fields=id,username,first_name,last_name,image'
                    );

                    $friends = json_decode($response->getBody()->getContents());
                    $output = array();

                    if (isset($friends->data)) {
                        foreach ($friends->data as $key => $friend) {
                            $userFriendId = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(
                                ['pinterestUserId' => $friend->id]
                            );
                            $areUsersFriends = false;
                            if ($userFriendId) {
                                $areUsersFriends = $this->get('api.repository.user')->checkFriend(
                                    $userId,
                                    $userFriendId->getId()
                                );
                            }
                            if ($userFriendId && !$areUsersFriends) {
                                $output[$key]['id'] = $userFriendId->getId();
                                $output[$key]['social_id'] = $friend->id;
                                $output[$key]['name'] = $friend->first_name;
                                $output[$key]['image'] = current((array)$friend->image)->url;
                            }

                        }

                        if ($output) {
                            return Responses::apiResponse(array_values($output));
                        }
                    }


                    return Responses::apiResponse(
                        ['message' => 'friend_not_found'],
                        ErrorMessages::FAILURE_CODE,
                        ErrorMessages::FAILURE_MESSAGE
                    );

                } catch (\Exception $e) {
                    return Responses::apiResponse(
                        ['message' => json_decode($e->getResponse()->getBody()->getContents())->message]
                    );
                }

                break;
        }

        return Responses::apiResponse(
            ['message' => false],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );

    }

    public function getFriendsHavingBookAction($bookId, Request $request)
    {
        $limit = $request->get('l') ? $request->get('l') : '10';
        $offset = $request->get('o') ? $request->get('o') : '0';
        $userId = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $bookExists = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $bookId]);

        if (!$bookExists) {
            return Responses::apiResponse(
                ['message' => 'book_not_found'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
        $friendsList = $this->get('api.repository.user')->getFriendsHavingBook($userId, $bookId, $limit, $offset);
        if ($friendsList) {
            return Responses::apiResponse($friendsList);
        }

        return Responses::apiResponse(
            ['message' => 'no_users_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );

    }

    public function getFriendByZipAction($zipCode, Request $request)
    {

        $limit = $request->get('l') ? $request->get('l') : '10';
        $offset = $request->get('o') ? $request->get('o') : '0';
        $userId = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $users = $entityManager->getRepository('ApiBundle:UserInfo')->findBy(
            ['zipCode' => $zipCode],
            null,
            $limit,
            $offset
        );

        if ($users) {
            $output = array();
            foreach ($users as $key => $user) {
                $checkUserFriends = $this->get('api.repository.user')->checkFriend($userId, $user->getUserId());
                if ($checkUserFriends) {
                    $output[$key] = [
                        'id' => $user->getUserId(),
                        'first_name' => $user->getFirstname(),
                        'last_name' => $user->getLastname(),
                        'image' => $user->getPicture(),
                    ];

                }
            }
            if ($output) {
                return Responses::apiResponse($output);
            }

            return Responses::apiResponse(
                ['message' => 'users_are_not_friends'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );

        }

        return Responses::apiResponse(
            ['message' => 'no_users_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );


    }

    public function getUserFriendsAction($friendId, Request $request)
    {
        $userId = $this->getUser()->getId();
        $friendId = $friendId?$friendId:$userId;
        $limit = $request->get('l') ? $request->get('l') : '10';
        $offset = $request->get('o') ? $request->get('o') : '0';
        $friends = $this->get('api.repository.user')->getUserFriends($friendId, $userId, $limit, $offset);
        if ($friends) {
            return Responses::apiResponse($friends);
        }

        return Responses::apiResponse(
            ['message' => 'friends_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function getFriendsCountAction($userId)
    {
        $userId = $userId ? $userId : $this->getUser()->getId();

        $users['user_count'] = (int)$this->get('api.repository.user')->getFriendsCount($userId);

        if ($users) {
            return Responses::apiResponse($users);
        }

        return Responses::apiResponse(
            ['message' => 'no_users_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function unFriendAction($friendId)
    {
        $userId = $this->getUser()->getId();
        $checkFriends = $this->get('api.repository.user')->checkFriend($userId, $friendId);
        if ($checkFriends) {
            $this->get('api.repository.user')->deleteFriend($userId, $friendId);

            $coinRepository = $this->get('api.repository.coin')->addCoin($userId, 'REMOVE_FRIEND');
            $currentCoin = $coinRepository['coin_point'];
            $coinsCount = $coinRepository['result'];

            return Responses::apiResponse(
                [
                    'deleted' => true,
                    'current_coin' => $currentCoin,
                    'coins_count' => $coinsCount,
                ]
            );
        }

        return Responses::apiResponse(
            ['message' => 'users_are_not_friends'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function searchFriendAction(Request $request)
    {

        $searchText = $request->get('text');
        $limit = $request->get('l') ? $request->get('l') : '10';
        $offset = $request->get('o') ? $request->get('o') : '0';
        $userId = $this->getUser()->getId();

        $users['users'] = $this->get('api.repository.user')->searchFriends($userId, $searchText, $limit, $offset);
        $users['user_count'] = (int)$this->get('api.repository.user')->searchFriendsCount($userId, $searchText);
        if ($users) {
            return Responses::apiResponse($users);
        }

        return Responses::apiResponse(
            ['message' => 'no_users_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function searchUsersAction(Request $request)
    {

        $searchText = $request->get('text');
        $limit = $request->get('l') ? $request->get('l') : '10';
        $offset = $request->get('o') ? $request->get('o') : '0';
        $userId = $this->getUser()->getId();

        $users['users'] = $this->get('api.repository.user')->searchUser($userId, $searchText, $limit, $offset);
        $users['user_count'] = (int)$this->get('api.repository.user')->searchUsersCount($userId, $searchText);
        if ($users) {
            return Responses::apiResponse($users);
        }

        return Responses::apiResponse(
            ['message' => 'no_users_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function addFriendAction(Request $request)
    {

        $userId = $this->getUser()->getId();
        $friendId = $request->get('friend_id');
        $entityManager = $this->getDoctrine()->getManager();
        $userExists = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(['id' => $friendId]);
        if (!$userExists) {
            return Responses::apiResponse(
                ['message' => 'no_users_found'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
        $isFriend = $this->get('api.repository.user')->checkFriend($userId, $friendId);

        if (!$isFriend) {
            $friend = new Friends();
            $friend->setUserId($userId);
            $friend->setFriendId($friendId);
            $friend->setStatus('waiting');
            $friend->setSeenStatus('unseen');
            $friend->setDate(new \DateTime());

            $entityManager->persist($friend);
            $entityManager->flush();

            $notification = new Notification();
            $notification->setUserId($userId);
            $notification->setFriendId($friendId);
            $notification->setMarkupType('friend');
            $notification->setMarkupId($friend->getId());
            $notification->setDate(new \DateTime());
            $notification->setNoteStatus(0);
            $notification->setNotification('want_add_friend');

            $entityManager->persist($notification);
            $entityManager->flush();

            $coinRepository = $this->get('api.repository.coin')->addCoin($userId, 'ADD_FRIEND');
            $currentCoin = $coinRepository['coin_point'];
            $coinsCount = $coinRepository['result'];

            return Responses::apiResponse(
                [
                    'status' => $friend->getStatus(),
                    'user_id' => $userId,
                    'friend_id' => $friendId,
                    'current_coin' => $currentCoin,
                    'coins_count' => $coinsCount,
                ]
            );
        }

        return Responses::apiResponse(
            ['message' => 'users_are_friends'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function acceptAnswerFriendRequestAction($answer, $friendId, Request $request)
    {

        $userId = $this->getUser()->getId();

        if ($answer == 'accept') {
            $approved = 1;
        } elseif ($answer == 'decline') {
            $approved = 0;
        } else {

            return Responses::apiResponse(
                ['message' => 'unexpected_parameter'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
        $curDate = new \DateTime();
        $entityManager = $this->getDoctrine()->getManager();
        $friendRequestExists = $this->get('api.repository.user')->checkFriend($userId, $friendId);

        if ($friendRequestExists) {

            $friends = $entityManager->getRepository('ApiBundle:Friends')->findOneBy(
                ['id' => $friendRequestExists[0]['id']]
            );
            if ($approved) {

                $friends->setStatus('friend');
                $entityManager->persist($friends);
                $posts = new Posts();
                $posts->setFriendId($friendId);
                $posts->setUserFriendId($userId);
                $posts->setType('friend');
                $posts->setNote('are_friends');
                $posts->setDate($curDate);
                $posts->setBookId(0);
                $entityManager->persist($posts);
            } else {
                $entityManager->remove($friends);
            }

            $entityManager->flush();

            return Responses::apiResponse(['user_id' => $userId, 'friend_id' => $friendId, 'approved' => $approved]);
        }

        return Responses::apiResponse(
            ['message' => 'users_are_not_friends'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }
    public function peopleMayKnowAction(Request $request)
    {

        $userId = $this->getUser()->getId();
        $limit = $request->get('l') ? $request->get('l') : '10';
        $offset = $request->get('o') ? $request->get('o') : '0';
        $mayKnowFriends['users'] = $this->get('api.repository.user')->getFriendFriends($userId, $limit, $offset);
        $mayKnowFriends['users_count'] = (int)$this->get('api.repository.user')->getFriendFriendsCount($userId);
        if ($mayKnowFriends) {
            return Responses::apiResponse($mayKnowFriends);
        }

        return Responses::apiResponse(
            ['message' => 'friends_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function peopleLikedSameBookAction(Request $request)
    {

        $userId = $this->getUser()->getId();
        $limit = $request->get('l') ? $request->get('l') : '10';
        $offset = $request->get('o') ? $request->get('o') : '0';
        $users['users'] = $this->get('api.repository.user')->bookLikedUsers($userId, $limit, $offset);
        $users['users_count'] = $this->get('api.repository.user')->bookLikedUsersCount($userId);
        if ($users) {
            return Responses::apiResponse($users);
        }

        return Responses::apiResponse(
            ['message' => 'friends_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function suggestedUsersAction()
    {
        $userId = $this->getUser()->getId();

        $likedUsersCount = (int)$this->get('api.repository.user')->bookLikedUsersCount($userId);
        $friendFriendsCount = (int)$this->get('api.repository.user')->getFriendFriendsCount($userId);
        $users['may_know']['users'] = $this->get('api.repository.user')->getFriendFriends($userId, 6, 0);
        $users['may_know']['users_count'] = (int)$this->get('api.repository.user')->getFriendFriendsCount($userId);
        $users['book_liked_users']['users'] = $this->get('api.repository.user')->bookLikedUsers($userId, 6, 0);
        $users['book_liked_users']['users_count'] = $likedUsersCount;
        $users['genre_liked_users'] = [];
        $users['author_liked_users'] = [];
        $users['publisher_liked_users'] = [];
        $users['count_all'] = $friendFriendsCount +
            $likedUsersCount;

        if ($users) {
            return Responses::apiResponse($users);
        }

        return Responses::apiResponse(
            ['message' => 'no_users_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function getFriendRequestsAction(Request $request)
    {
        $userId = $this->getUser()->getId();
        $limit = $request->get('l') ? $request->get('l') : '10';
        $offset = $request->get('o') ? $request->get('o') : '0';
        $entityManager = $this->getDoctrine()->getManager();

        $friedRequests = $entityManager->getRepository('ApiBundle:Friends')->findBy(
            ['friendId' => $userId, 'status' => 'waiting'],
            ['date' => 'DESC'],
            $limit,
            $offset
        );
        $friedRequestsCount = $entityManager->getRepository('ApiBundle:Friends')->findBy(
            ['friendId' => $userId, 'status' => 'waiting']
        );

        $userInfo = array();
        $validator = new Validator();
        foreach ($friedRequests as $key => $friedRequest) {
            $userData = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(
                ['userId' => $friedRequest->getUserId()]
            );
            if (!$userData) {
                return Responses::apiResponse(
                    ['message' => 'users_friend_not_found'],
                    ErrorMessages::FAILURE_CODE,
                    ErrorMessages::FAILURE_MESSAGE
                );
            }
            $userFriends = $this->get('api.repository.user')->getUserFriends($friedRequest->getUserId(), $userId);
            $userFriendFriends = $this->get('api.repository.user')->getUserFriends($friedRequest->getFriendId(), $userId);
            if (count($userFriends) > count($userFriendFriends)) {
                $arr1 = $userFriends;
                $arr2 = $userFriendFriends;
            } else {
                $arr2 = $userFriends;
                $arr1 = $userFriendFriends;
            }
            $mutualFriends = array_intersect($arr2, $arr1);

            $userInfo[$key]['id'] = $userData->getUserId();
            $userInfo[$key]['firstname'] = $userData->getFirstname();
            $userInfo[$key]['lastname'] = $userData->getLastname();
            $userInfo[$key]['seen_status'] = $friedRequest->getSeenStatus();
            $userInfo[$key]['picture'] = $validator->generateCorrectProfilePicture($userData->getPicture());
            $userInfo[$key]['thumbnail'] = $validator->generateCorrectProfilePicture(
                str_replace('img/profile/', 'img/profile/thumbnail/', $userData->getPicture())
            );
            $userInfo[$key]['mutual_friends_count'] = count($mutualFriends);
            $userInfo[$key]['mutual_friends'] = array_slice($mutualFriends, 0, 10);
        }
        $output['users'] = $userInfo;
        $output['users_count'] = $friedRequestsCount ? (int)count($friedRequestsCount) : 0;

        return Responses::apiResponse($output);
    }

    public function getMutualFriendsAction($friendId, Request $request)
    {
        $userId = $this->getUser()->getId();
        $limit = $request->get('l') ? $request->get('l') : '10';
        $offset = $request->get('o') ? $request->get('o') : '0';

        $userFriends = $this->get('api.repository.user')->getUserFriends($friendId, $userId, 100);
        $userFriendFriends = $this->get('api.repository.user')->getUserFriends($userId,$friendId, 100);
        if (count($userFriends) > count($userFriendFriends)) {
            $arr1 = $userFriends;
            $arr2 = $userFriendFriends;
        } else {
            $arr2 = $userFriends;
            $arr1 = $userFriendFriends;
        }
        $mutualFriends = array_intersect($arr2, $arr1);
        $output['users'] = array_slice($mutualFriends, $offset, $limit);
        $output['users_count'] = count($mutualFriends);

        return Responses::apiResponse($output);
    }

    public function getNotSeenFriendRequestsAction()
    {
        $userId = $this->getUser()->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $notSeenFriendRequests = $entityManager->getRepository('ApiBundle:Friends')->findBy(
            ['status' => 'waiting', 'seenStatus' => 'unseen', 'friendId' => $userId]
        );

        return Responses::apiResponse(
            [
                'not_seen_friends' => $notSeenFriendRequests ? count($notSeenFriendRequests) : 0,
            ]
        );
    }

    public function changeUnseenStatusAction()
    {
        $userId = $this->getUser()->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $notSeenFriendRequests = $entityManager->getRepository('ApiBundle:Friends')->findBy(
            ['status' => 'waiting', 'seenStatus' => 'unseen', 'friendId' => $userId]
        );
        if ($notSeenFriendRequests) {
            foreach ($notSeenFriendRequests as $key => $notSeenFriendRequest) {
                $notSeenFriendRequest->setSeenStatus('seen');
                $entityManager->persist($notSeenFriendRequest);
                $entityManager->flush();
            }
        }

        return Responses::apiResponse(['seen' => true]);
    }

    public function checkUserFriendsAction($friendId)
    {
        $userId = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();

        $friendRequestExists = $this->get('api.repository.user')->checkFriend($userId, $friendId);
        $userExists = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(['id' => $friendId]);

        if (!$userExists) {
            return Responses::apiResponse(
                ['message' => 'no_users_found'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

        if ($friendRequestExists) {
            $userStatus = $friendRequestExists[0]['status'];
        } else {
            $userStatus = 'not_friend';
        }

        return Responses::apiResponse(['status' => $userStatus]);
    }
}

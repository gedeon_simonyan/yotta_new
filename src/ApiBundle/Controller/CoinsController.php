<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Coins;
use ApiBundle\Entity\CoinsStatistic;
use ApiBundle\Utils\ErrorMessages;
use ApiBundle\Utils\Responses;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CoinsController extends Controller
{
    public function indexAction($userId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $userExists = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(['id'=>$userId]);
        if(!$userExists){
            return Responses::apiResponse(
                ['message' => 'no_users_found'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

        $coins = $entityManager->getRepository('ApiBundle:Coins')->findOneBy(['userId' => $userId]);
        $output = [];
        if (empty($coins)) {
            $output = ['message' => 'coin_not_found'];
            $status = ErrorMessages::FAILURE_MESSAGE;
        } else {
            $output['user_id'] = $coins->getUserId();
            $output['coin'] = $coins->getCoin();
            $status = ErrorMessages::SUCCESS_MESSAGE;
        }

        return Responses::apiResponse($output, ErrorMessages::FAILURE_CODE, $status);
    }

    public function addAction(Request $request)
    {
        $purpose = strtoupper($request->get('purpose'));
        $userId = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $coinPoints = $entityManager->getRepository('ApiBundle:CoinPoints')->findOneBy(['purpose' => $purpose]);
        $coins = $entityManager->getRepository('ApiBundle:Coins')->findOneBy(['userId' => $userId]);

        if (!$coinPoints) {
            return Responses::apiResponse(
                ['message' => 'unable_add_coin'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

        $coinsStatic = new CoinsStatistic();
        if (empty($coins)) {
            $coins = new Coins();
            $coinCount = $coinPoints->getCoin();
            $coins->setUserId($userId);
        } else {
            $coinCount = $coins->getCoin() + $coinPoints->getCoin();
        }
        if($coinCount < 0){
            $coinCount = 0;
        }
        $coins->setCoin($coinCount);
        $entityManager->persist($coins);

        $coinsStatic->setCoin($coinPoints->getCoin());
        $coinsStatic->setCoinId($coinPoints->getId());
        $coinsStatic->setUserId($userId);
        $entityManager->persist($coinsStatic);
        $entityManager->flush();

        return Responses::apiResponse([
            'coin_point' => $coinPoints->getCoin(),
            'result' => $coins->getCoin(),
        ]);
    }
}

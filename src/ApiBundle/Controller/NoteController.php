<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\NewsletterSubscribers;
use ApiBundle\Entity\Notification;
use ApiBundle\Repository\NoteRepository;
use ApiBundle\Utils\ErrorMessages;
use ApiBundle\Utils\Responses;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NoteController extends Controller
{
    public function getAction(Request $request, NoteRepository $noteRepository)
    {

        $limit = intval($request->query->get('l'));
        $offset = intval($request->query->get('o'));
        $userId = $this->getUser()->getId();
        $noteRepository->getLast20DayNotes();
        $note = $noteRepository->getNote($userId, $offset?$offset:0, $limit?$limit:10);
        $approveNote =$noteRepository->getApproveNote($userId, $offset?$offset:0, $limit?$limit:10);

        if ($note) {
            $output['notes'] = $note;
            $output['approve_notes'] = $approveNote;

            return Responses::apiResponse($output);
        }
        return Responses::apiResponse(
            ['message' => 'no_notification'],
            200,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function updateAction($noteId, Request $request){
        $param = $request->get('status');
        $manager = $this->getDoctrine()->getManager();
        $userId = $this->getUser()->getId();

        $noteData = $manager->getRepository('ApiBundle:Notification')->findOneBy(['id'=>$noteId,'userId'=>$userId]);

        if (!$noteData) {
            return Responses::apiResponse(
                ['message' => 'note_data_not_found'],
                200,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

        if ($param == '1'){
            $noteData->setNoteStatus(1);
        }elseif ($param == '0'){
            $noteData->setNoteStatus(0);
        }else{
            return Responses::apiResponse(
                ['message'=> 'parameter_undefined'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
            $manager->merge($noteData);
            $manager->flush();
            $output = ['seen'=>true];

            return Responses::apiResponse($output);

    }

    public function subscribeAction(Request $request){
        $data = $request->get('email');

        $manager = $this->getDoctrine()->getManager();
        $date = new \DateTime();
        $sub = $manager->getRepository('ApiBundle:NewsletterSubscribers')->findOneBy(['email'=>$data]);



        if (filter_var($data, FILTER_VALIDATE_EMAIL)){
            if (empty($sub)){
                $subscribe = new NewsletterSubscribers();
                $subscribe->setEmail($data);
                $subscribe->setSubscribeDate($date);

                $manager->persist($subscribe);
                $manager->flush();


                return Responses::apiResponse(
                    ['message'=>'subscribe_success']
                );
            }else{
                return Responses::apiResponse(
                    ['message'=> 'subscribe_exist'],
                    ErrorMessages::FAILURE_CODE,
                    ErrorMessages::FAILURE_MESSAGE
                );
            }
        }else{
            return Responses::apiResponse(
                ['message'=> 'email_invalid'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

    }
}

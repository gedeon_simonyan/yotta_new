<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Authors;
use ApiBundle\Entity\BookAuthor;
use ApiBundle\Entity\BookPresent;
use ApiBundle\Entity\Books;
use ApiBundle\Entity\Likes;
use ApiBundle\Entity\Posts;
use ApiBundle\Entity\Rating;
use ApiBundle\Entity\UserBooks;

use ApiBundle\Entity\BookTags;
use ApiBundle\Entity\Tag;

use ApiBundle\Services\AmazonSearch;
use ApiBundle\Services\Validator;
use ApiBundle\Utils\ErrorMessages;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\ClassLoader\MapClassLoader;
use Symfony\Component\HttpFoundation\Request;
use ApiBundle\Utils\Responses;

class BookController extends Controller
{

    private $bookStatus = array(
        'reading',
        'wish',
        'have',
        'lend',
        'borrow',
    );
    private $orderBy = array(
        'featured',
        'like',
        'tag',
        'date',
    );
    private $orderKey = array(
        'ASC',
        'DESC',
    );

    public function searchAction(Request $request)
    {
        $searchText = htmlentities(addslashes($request->get('search_text')));
        $searchLang = $request->get('search_lang') ? $request->get('search_lang') : 'english';
        $startPage = $request->get('offset') ? $request->get('offset') : '1';
        $limit = $request->get('limit') ? $request->get('limit') : '10';
        $userId = $request->get('u') ? $request->get('u') : 0;
        $orderBy = $request->get('order_by') ? $request->get('order_by') : 'featured';
        $orderKey = $request->get('order_key') ? $request->get('order_key') : 'DESC';

        if(!in_array($orderBy,$this->orderBy)){
            return Responses::apiResponse(
                ['message' => 'incorrect_parameter'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
        if(!in_array($orderKey,$this->orderKey)){
            return Responses::apiResponse(
                ['message' => 'incorrect_parameter'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
        if ($startPage == 1) {
            $offset = 0;
        } else {
            $offset = $startPage * $limit - 1 - ($limit - 1);
        }

        $results = $this->get('api.repository.book')->searchInYotta(
            $searchText,
            $searchLang,
            false,
            $orderBy,
            $orderKey
        );

        if ($results) {

            foreach ($results as $key => $result) {
                $entityManager = $this->getDoctrine()->getManager();
                $avgRate = $this->get('api.repository.rating')->getAvgBookRate($result['id']);
                $bookAuthor = $this->get('api.repository.book')->getBookAuthor($result['id']);

                $results[$key]['tag'] = $this->get('api.repository.book')->getBookTags($result['id']);
                $results[$key]['avg_rate'] = $avgRate ? floatval($avgRate) : 0;
                $results[$key]['author'] = $bookAuthor ? $bookAuthor : '';
                $results[$key]['author'] = $bookAuthor ? $bookAuthor : '';
                $results[$key]['likes_count'] = $results[$key]['like_count'];
                $results[$key]['markups_count'] = $this->get('api.repository.markup')->getAllMarkupsCount($result['id']);
                $results[$key]['thumbnail'] = $result['thumbnail'] ? 'https://'.$request->getHttpHost(
                    ).'/'.$result['thumbnail'] : $result['image'];
                unset($results[$key]['authors']);
                unset($results[$key]['like_count']);
                unset($results[$key]['markups']);


                $myRate = 0;
                $myLike = true;
                $bookInLib = null;
                if ($userId) {
                    $myRate = $entityManager->getRepository('ApiBundle:Rating')->findOneBy(
                        ['userId' => $userId, 'bookId' => $result['id']]
                    );
                    $bookInLib = $entityManager->getRepository('ApiBundle:UserBooks')->findOneBy(
                        ['bookId' => $result['id'], 'userId' => $userId]
                    );

                    $like = $entityManager->getRepository('ApiBundle:Likes')->findOneBy(
                        ['bookId' => $result['id'], 'userId' => $userId]
                    );
                    $myLike = $like ? $like->getLike() : false;

                }
                $ratings = $entityManager->getRepository('ApiBundle:Rating')->findBy(
                    ['bookId' => $result['id']]
                );
                $ratingCount = count($ratings);
                $bookUsersCount = $entityManager->getRepository('ApiBundle:UserBooks')->findBy(
                    ['bookId' => $result['id']]
                );
                if ($bookUsersCount) {
                    $userCount = count($bookUsersCount);
                } else {
                    $userCount = 0;
                }

                $results[$key]['status'] = $bookInLib ? $bookInLib->getStatus() : null;
                $results[$key]['my_like'] = $myLike;
                $results[$key]['library_id'] = $bookInLib ? $bookInLib->getId() : null;
                $results[$key]['my_rate'] = $myRate ? intval($myRate->getStars()) : 0;
                $results[$key]['book_users_count'] = $userCount;
                $results[$key]['rate_count'] = $ratingCount;

                unset($results[$key]['likes']);
            }
        }
        $mapping = array(
            'AmazonProductAPI' => $this->getParameter('amazon_api'),
        );
        $totalCount = count($results);
        $totalPages = ceil(($totalCount + 10) / $limit);

        if ($totalPages >= $startPage) {
            $amazonStartPage = 1;
        } else {
            $amazonStartPage = $startPage - $totalPages + 1;
            $offset = $totalCount;
        }
        $loader = new MapClassLoader($mapping);
        $loader->register();
        $amazonSearch = new AmazonSearch();
        $amazonResult = $amazonSearch->searchInAmazon($searchText, $searchLang, $amazonStartPage, false);
        if ($amazonStartPage > 1) {
            $output['book_info'] = $amazonResult['book_info'];
        } else {
            $output['book_info'] = array_merge($results, $amazonResult['book_info'] ? $amazonResult['book_info'] : []);
            $books = array_slice($output['book_info'], $offset, $limit, true);
            $output['book_info'] = array_values($books);
        }

        if ($amazonResult['totalCount'] > 100) {
            $output['totalCount'] = $totalCount + 100;
        } else {
            $output['totalCount'] = $totalCount + $amazonResult['totalCount'];
        }

        $output['totalPages'] = ceil($output['totalCount'] / $limit);

        if (!empty($output['book_info'])) {
            $status = ErrorMessages::SUCCESS_MESSAGE;
            $code = 200;
        } else {
            $output = ['message' => 'book_not_found'];
            $status = ErrorMessages::FAILURE_MESSAGE;
            $code = ErrorMessages::FAILURE_CODE;
        }

        return Responses::apiResponse($output, $code, $status);
    }

    public function getBookMarkupsAction($bookId, $markup, Request $request)
    {

        $limit = $request->query->get('l');
        $offset = $request->query->get('o');

        $entityManager = $this->getDoctrine()->getManager();
        $bookExists = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $bookId]);

        if ($bookExists) {

            $validator = new Validator();
            $bookMarkups = $this->get('api.repository.book')->getBookMarkups(
                $bookId,
                $markup,
                intval($limit),
                intval($offset)
            );
            $bookMarkupsCount = $this->get('api.repository.book')->getBookMarkupsCount(
                $bookId,
                $markup
            );

            if ($bookMarkups) {

                $output = array();
                foreach ($bookMarkups as $key => $bookMarkup) {

                    $markupComments = $entityManager->getRepository('ApiBundle:BookMarkups')->findBy(
                        ['bookId' => $bookId, 'markupId' => $bookMarkup['id']],
                        ['addedDate' => 'desc'],
                        3,
                        0
                    );
                    $output['markups'][$key]['id'] = $bookMarkup['id'];
                    $output['markups'][$key]['type'] = $bookMarkup['type'];
                    $output['markups'][$key]['book_id'] = $bookId;
                    $output['markups'][$key]['title'] = $bookMarkup['title'];
                    $output['markups'][$key]['music_author'] = $bookMarkup['music_author'];
                    $output['markups'][$key]['price'] = $bookMarkup['music_price'];
                    $output['markups'][$key]['music_track_url'] = $bookMarkup['music_track_url'];
                    $output['markups'][$key]['rating'] = $bookMarkup['rating'];
                    $output['markups'][$key]['music_author_image'] = $bookMarkup['music_author_img'];
                    $output['markups'][$key]['description'] = $bookMarkup['markup_desc'];
                    $output['markups'][$key]['genre'] = $bookMarkup['music_genre'];
                    $output['markups'][$key]['preview_url'] = $bookMarkup['preview_url'];
                    if ($markupComments && !empty($markupComments)) {
                        $markupCommentsCount = count(
                            $entityManager->getRepository('ApiBundle:BookMarkups')->findBy(
                                ['bookId' => $bookId, 'markupId' => $bookMarkup['id']]
                            )
                        );

                        foreach ($markupComments as $k => $markupComment) {
                            $user = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(
                                ['userId' => $markupComment->getUserId()]
                            );
                            $output['markups'][$key]['comments'][$k] = [
                                'id' => $markupComment->getId(),
                                'user_id' => $markupComment->getUserId(),
                                'first_name' => $user->getFirstname(),
                                'last_name' => $user->getLastname(),
                                'content' => $markupComment->getContent(),
                                'image' => $validator->generateCorrectProfilePicture($user->getPicture()),
                                'thumbnail' => $validator->generateCorrectProfilePicture(
                                    str_replace('img/profile/', 'img/profile/thumbnail/', $user->getPicture())
                                ),
                                'added_date' => $markupComment->getAddedDate()->format('Y-m-d H:i:s'),
                            ];
                        }
                        $output['markups'][$key]['markup_comment_count'] = $markupCommentsCount;
                    }
                }
                if($markup == 'all'){
                    $markups = $entityManager->getRepository('ApiBundle:BookMarkups')->findBy(['bookId'=>$bookId]);
                    $allMarkupCount =  is_array($markups) ? count($markups) : 0;
                }else{
                    $markups =  $this->get('api.repository.markup')->getMarkupTypeCount($bookId, $markup);
                    $allMarkupCount = $markups ? (int)$markups['markupCount'] : 0;
                }

                $output['markup_count'] = is_array($bookMarkupsCount) ? count($bookMarkupsCount) : 0;
                $output['all_markup_count'] = $allMarkupCount;

                return Responses::apiResponse($output);
            }

            return Responses::apiResponse(
                ['message' => 'markup_not_exists'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );

        }

        return Responses::apiResponse(
            ['message' => 'book_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );

    }

    public function getBookMarkupCommentsAction($bookId, $markupId, Request $request)
    {

        $limit = $request->query->get('l');
        $offset = $request->query->get('o');
        $validator = new Validator();
        $entityManager = $this->getDoctrine()->getManager();
        $markupComments = $entityManager->getRepository('ApiBundle:BookMarkups')->findBy(
            ['bookId' => $bookId, 'markupId' => $markupId],
            ['addedDate' => 'desc'],
            $limit,
            $offset
        );
        $markupCommentsCount = count(
            $entityManager->getRepository('ApiBundle:BookMarkups')->findBy(
                ['bookId' => $bookId, 'markupId' => $markupId]
            )
        );
        if ($markupComments && !empty($markupComments)) {

            foreach ($markupComments as $key => $markupComment) {
                $user = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(
                    ['userId' => $markupComment->getUserId()]
                );
                if($user){
                    $output['comments'][] = [
                        'id' => $markupComment->getId(),
                        'user_id' => $markupComment->getUserId(),
                        'first_name' => $user?$user->getFirstname():'',
                        'last_name' => $user?$user->getLastname():'',
                        'content' => $markupComment->getContent(),
                        'image' => $user?$validator->generateCorrectProfilePicture($user->getPicture()):'',
                        'thumbnail' => $user?$validator->generateCorrectProfilePicture(
                            str_replace('img/profile/', 'img/profile/thumbnail/', $user->getPicture())
                        ):'',
                        'added_date' => $markupComment->getAddedDate()->format('Y-m-d H:i:s'),
                    ];
                }
            }
            $output['comment_count'] = $markupCommentsCount;

            return Responses::apiResponse($output);
        }

        return Responses::apiResponse(
            ['message' => 'markup_not_exists'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function getBookMarkupUsersAction($bookId, Request $request)
    {

        $limit = $request->get('l');
        $offset = $request->get('o');
        $entityManager = $this->getDoctrine()->getManager();


        $bookExists = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $bookId]);

        if (!$bookExists) {
            return Responses::apiResponse(
                ['message' => 'book_not_found'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

        $bookTaggedUsers = $this->get('api.repository.book')->getBookMarkupUsers(
            $bookId,
            $limit ? (int)$limit : 10,
            $offset ? (int)$offset : 0
        );
        $bookTaggedUsersCount = $this->get('api.repository.book')->getBookMarkupUsersCount($bookId);

        $output['users'] = $bookTaggedUsers;
        $output['user_count'] = $bookTaggedUsersCount;

        return Responses::apiResponse($output);
    }

    public function topRatedBooksAction(Request $request)
    {

        $offset = $request->get('o') ? $request->get('o') : '1';
        $limit = $request->get('l') ? $request->get('l') : '10';
        $topRatedBooks = $this->get('api.repository.rating')->topRatedBooks($limit, $offset);
        $entityManager = $this->getDoctrine()->getManager();
        if ($topRatedBooks) {

            $output = array();
            foreach ($topRatedBooks as $key => $topRatedBook) {
                $book = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $topRatedBook['bookId']]);
                $bookAuthor = $this->get('api.repository.book')->getBookAuthor($topRatedBook['bookId']);
                $output[$key]['id'] = $topRatedBook['bookId'];
                $output[$key]['book_id'] = $book->getBookId();
                $output[$key]['title'] = $book->getTitle();
                $output[$key]['author'] = $bookAuthor ? $bookAuthor : '';
                $output[$key]['image'] = $book->getImage();
                $output[$key]['thumbnail'] = $book->getThumbnail() ? 'https://'.$request->getHttpHost(
                    ).'/'.$book->getThumbnail() : $book->getImage();
                $output[$key]['book_url'] = $book->getBookUrl();
                $output[$key]['publisher'] = trim($book->getPublisher());
                $output[$key]['rate'] = $topRatedBook['rate'];;
            }

            return Responses::apiResponse($output);
        }

        return Responses::apiResponse(
            ['message' => 'book_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function lastRatedBooksAction(Request $request)
    {

        $offset = $request->get('o') ? $request->get('o') : '1';
        $limit = $request->get('l') ? $request->get('l') : '10';
        $topRatedBooks = $this->get('api.repository.rating')->lastRatedBooks($limit, $offset);
        $entityManager = $this->getDoctrine()->getManager();
        if ($topRatedBooks) {

            $output = array();
            foreach ($topRatedBooks as $key => $topRatedBook) {
                $book = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $topRatedBook['bookId']]);
                $bookAuthor = $this->get('api.repository.book')->getBookAuthor($topRatedBook['bookId']);
                $output[$key]['id'] = $topRatedBook['bookId'];
                $output[$key]['book_id'] = $book->getBookId();
                $output[$key]['title'] = $book->getTitle();
                $output[$key]['author'] = $bookAuthor ? $bookAuthor : '';
                $output[$key]['image'] = $book->getImage();
                $output[$key]['thumbnail'] = $book->getThumbnail() ? 'https://'.$request->getHttpHost(
                    ).'/'.$book->getThumbnail() : $book->getImage();
                $output[$key]['book_url'] = $book->getBookUrl();
                $output[$key]['publisher'] = trim($book->getPublisher());
                $output[$key]['rate'] = $topRatedBook['rate'];;
            }

            return Responses::apiResponse($output);
        }

        return Responses::apiResponse(
            ['message' => 'book_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function topMusicsAction(Request $request)
    {
        $offset = $request->get('o') ? $request->get('o') : '1';
        $limit = $request->get('l') ? $request->get('l') : '10';
        $musics = $this->get('api.repository.markup')->getTopMusics($limit, $offset);

        if ($musics) {

            return Responses::apiResponse($musics);
        }

        return Responses::apiResponse(
            ['message' => 'music_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );

    }

    public function getBookDataAction($bookId, Request $request)
    {
        $userId = $request->get('u') ? $request->get('u') : 0;
        $entityManager = $this->getDoctrine()->getManager();
        $bookData = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $bookId]);

        if ($bookData) {
            $output = array();
            $avgRate = $this->get('api.repository.rating')->getAvgBookRate($bookId);
            $bookInLib = null;
            $myRate = 0;
            $like = false;
            if ($userId) {
                $bookInLib = $entityManager->getRepository('ApiBundle:UserBooks')->findOneBy(
                    ['bookId' => $bookId, 'userId' => $userId]
                );
                $myRate = $entityManager->getRepository('ApiBundle:Rating')->findOneBy(
                    ['userId' => $userId, 'bookId' => $bookId]
                );
                $like = $entityManager->getRepository('ApiBundle:Likes')->findOneBy(
                    ['bookId' => $bookId, 'userId' => $userId]
                );
            }

            $myRate = $myRate ? $myRate->getStars() : 0;
            $bookUsersCount = $entityManager->getRepository('ApiBundle:UserBooks')->findBy(
                ['bookId' => $bookId]
            );
            if ($bookUsersCount) {
                $userCount = count($bookUsersCount);
            } else {
                $userCount = 0;
            }
            $ratings = $entityManager->getRepository('ApiBundle:Rating')->findBy(
                ['bookId' => $bookId]
            );
            $ratingCount = count($ratings);
            $bookTaggedUsers = $this->get('api.repository.book')->getBookMarkupUsers($bookId,5,0);
            $bookTaggedUsersCount = $this->get('api.repository.book')->getBookMarkupUsersCount($bookId);

            $myLike = $like ? $like->getLike() : false;
            $bookAuthor = $this->get('api.repository.book')->getBookAuthor($bookId);

            $bookImage = $bookData->getImage()?$bookData->getImage():'https://'.$request->getHttpHost(
                ).'/api/web/img/books/image_not_available.png';
            $bookThumbnail = $bookData->getThumbnail() ? 'https://'.$request->getHttpHost(
                ).'/'.$bookData->getThumbnail() : $bookImage;

            $lentUsersCount = $this->get('api.repository.user')->getLendUsersCount($bookId,$userId);
            $borrowUsersCount = $this->get('api.repository.user')->getBorrowUsersCount($bookId,$userId);
            $presentUsersCount = $this->get('api.repository.user')->getPresentUsers($bookId,$userId);

            $output['title'] = $bookData->getTitle();
            $output['status'] = $bookInLib ? $bookInLib->getStatus() : null;
            $output['library_id'] = $bookInLib ? $bookInLib->getId() : null;
            $output['author'] = $bookAuthor ? $bookAuthor : '';
            $output['publisher'] = $bookData->getPublisher();
            $output['publish_date'] = $bookData->getPublishDate() ? $bookData->getPublishDate()->format('Y-m-d') : '';
            $output['language'] = $bookData->getLanguage();
            $output['description'] = $bookData->getDescription();
            $output['image'] = $bookImage;
            $output['thumbnail'] = $bookThumbnail;
            $output['book_id'] = $bookData->getBookId();
            $output['book_url'] = $bookData->getBookUrl();
            $output['tags'] = $this->get('api.repository.book')->getBookTags($bookId);
            $output['avg_rate'] = $avgRate ? floatval($avgRate) : 0;
            $output['my_rate'] = $myRate;
            $output['rate_count'] = $ratingCount;
            $output['markups_count'] = $this->get('api.repository.markup')->getAllMarkupsCount($bookId);
            $output['likes_count'] = count(
                $entityManager->getRepository('ApiBundle:Likes')->findBy(['bookId' => $bookId, 'likes' => 1])
            );
            $output['my_like'] = $myLike;
            $output['book_users_count'] = $userCount;
            $output['markup_users'] = $bookTaggedUsers;
            $output['markup_user_count'] = $bookTaggedUsersCount;
            $output['lend_user_count'] = count($lentUsersCount);
            $output['borrow_user_count'] = count($borrowUsersCount);
            $output['present_user_count'] = count($presentUsersCount);

            return Responses::apiResponse($output);
        }

        return Responses::apiResponse(
            ['message' => 'book_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function addBookInLibraryAction(Request $request)
    {

        $bookId = $request->get('book_id');
        $title = trim(addslashes($request->get('title')));
        $bookUrl = $request->get('book_url');
        $description = trim(addslashes(strip_tags($request->get('description'))));
        $image = $request->get('image');
        $publisher = addslashes($request->get('publisher'));
        $price = trim($request->get('price'));
        $author = addslashes($request->get('author'));
        $publishDate = $request->get('publish_date');
        $language = trim(strtolower($request->get('language')));

        $userId = $this->getUser()->getId();
        $date = new \DateTime();

        if ($bookId && $userId) {
            $entityManager = $this->getDoctrine()->getManager();
            $bookExists = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['bookId' => $bookId]);
            $key = 'added';
            if ($bookExists) {

                $id = $bookExists->getId();
                $output = [$key => true, 'book_id' => $id];

                return Responses::apiResponse($output);
            } else {
                $authors = explode(',', $author);
                foreach ($authors as $a) {
                    $authorExists = $entityManager->getRepository('ApiBundle:Authors')->findOneBy(['publicName' => $a]);

                    if (!$authorExists) {
                        $addAuthor = new Authors();
                        $addAuthor->setPublicName($a);
                        $entityManager->persist($addAuthor);
                        $entityManager->flush();
                        $authorId = $addAuthor->getId();
                    } else {
                        $authorId = $authorExists->getId();
                    }
                }

                $thumbnail = array('thumbnail' => true, 'padding' => false);
                $upload = $this->get('api.service.image_upload')->upload($image, $thumbnail, $bookId, 'book');
                if (!isset($upload['newName'])) {
                    return Responses::apiResponse(
                        ['message' => 'unable_upload_thumbnail'],
                        ErrorMessages::FAILURE_CODE,
                        ErrorMessages::FAILURE_MESSAGE
                    );
                }
                $book = new Books();

                $book->setBookId($bookId);
                $book->setUserId($userId);
                $book->setTitle($title);
                $book->setBookUrl($bookUrl);
                $book->setDescription($description);
                $book->setImage($image);
                $book->setThumbnail('api/web/img/books/thumbnail/'.$upload['newName']);
                $book->setPublisher($publisher);
                $book->setPrice($price);
                $book->setPublishDate(new \DateTime($publishDate));
                $book->setLanguage($language);
                $book->setCreatedDate($date);
                $book->setModified($date);
                $entityManager->persist($book);
                $entityManager->flush();
                $id = $book->getId();

                $authorBook = new BookAuthor();
                $authorBook->setBookId($id);
                $authorBook->setAuthorId($authorId);
                $entityManager->persist($authorBook);
                $entityManager->flush();

                $output = [$key => true, 'book_id' => $id];

                return Responses::apiResponse($output);
            }
        }

        return Responses::apiResponse(
            ['message' => 'parameters_missing'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function generateThumbnailAction(Request $request)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $bookData = $entityManager->getRepository('ApiBundle:Books')->findAll();
        foreach ($bookData as $key => $book){
            if($book->getImage()){
                $thumbnail = array('thumbnail' => true, 'padding' => false);
                $upload = $this->get('api.service.image_upload')->upload($book->getImage(), $thumbnail, $book->getId(), 'book');
                if (isset($upload['newName'])) {

                    $book->setThumbnail('api/web/img/books/thumbnail/'.$upload['newName']);

                    $entityManager->persist($book);
                    $entityManager->flush();
                }

            }


        }

        return Responses::apiResponse(['success']);

    }

    public function removeBookAction($bookId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $userId = $this->getUser()->getId();

        $bookExistsInLibrary = $entityManager->getRepository('ApiBundle:UserBooks')->findOneBy(
            ['userId' => $userId, 'bookId' => $bookId]
        );
        if ($bookExistsInLibrary) {
            $entityManager->remove($bookExistsInLibrary);
            $entityManager->flush();
            $coinRepository = $this->get('api.repository.coin')->addCoin($userId, 'REMOVE_BOOK');
            $currentCoin = $coinRepository['coin_point'];
            $coinsCount = $coinRepository['result'];

            return Responses::apiResponse(
                [
                    'deleted' => true,
                    'book_id' => $bookExistsInLibrary->getBookId(),
                    'current_coin' => $currentCoin,
                    'coins_count' => $coinsCount,
                ]
            );
        }

        return Responses::apiResponse(
            ['message' => 'book_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function getBookUsersAction($bookId, Request $request)
    {

        $limit = $request->get('l');
        $offset = $request->get('o');
        $entityManager = $this->getDoctrine()->getManager();

        $bookExists = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $bookId]);

        if (!$bookExists) {
            return Responses::apiResponse(
                ['message' => 'book_not_found'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

        $bookUsers = $entityManager->getRepository('ApiBundle:UserBooks')->findBy(
            ['bookId' => $bookId],
            null,
            $limit,
            $offset
        );
        $bookUsersCount = $entityManager->getRepository('ApiBundle:UserBooks')->findBy(['bookId' => $bookId]);
        if ($bookUsersCount) {
            $userCount = count($bookUsersCount);
        } else {
            $userCount = 0;
        }

        if ($bookUsers) {
            $output = array();
            $validator = new Validator();
            foreach ($bookUsers as $key => $bookUser) {
                $userInfo = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(
                    ['userId' => $bookUser->getUserId()]
                );
                $apiUsers = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(
                    ['id' => $bookUser->getUserId()]
                );

                if ($userInfo && $apiUsers) {
                    $output['users'][$key]['user_id'] = $userInfo->getUserId();
                    $output['users'][$key]['username'] = $apiUsers->getUsername();
                    $output['users'][$key]['first_name'] = $userInfo->getFirstname();
                    $output['users'][$key]['last_name'] = $userInfo->getLastname();
                    $output['users'][$key]['image'] = $validator->generateCorrectProfilePicture(
                        $userInfo->getPicture()
                    );
                    $output['users'][$key]['thumbnail'] = $validator->generateCorrectProfilePicture(
                        str_replace('img/profile/', 'img/profile/thumbnail/', $userInfo->getPicture())
                    );
                }
            }
            $output['users'] = array_values($output['users']);
            $output['user_count'] = $userCount;

            return Responses::apiResponse($output);
        }

        return Responses::apiResponse(
            ['message' => 'user_not_exists'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function getBookTaggedUsersAction($bookId, Request $request)
    {

        $limit = $request->get('l');
        $offset = $request->get('o');
        $entityManager = $this->getDoctrine()->getManager();


        $bookExists = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $bookId]);

        if (!$bookExists) {
            return Responses::apiResponse(
                ['message' => 'book_not_found'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

        $bookTaggedUsers = $this->get('api.repository.book')->getBookTaggedUsers(
            $bookId,
            $limit ? $limit : 10,
            $offset ? $offset : 0
        );
        $bookTaggedUsersCount = $this->get('api.repository.book')->getBookTaggedUsersCount($bookId);

        $output['users'] = $bookTaggedUsers;
        $output['user_count'] = $bookTaggedUsersCount;

        return Responses::apiResponse($output);
    }

    public function getRecommendedBooksAction(Request $request)
    {

        $limit = $request->get('l');
        $offset = $request->get('o');
        $userId = $this->getUser()->getId();
        $recommendedBooks = $this->get('api.repository.book')->getRecommendedBooks(
            $userId,
            'https://'.$request->getHttpHost().'/',
            $limit,
            $offset
        );
        $recommendedBooksCount = $this->get('api.repository.book')->getRecommendedBooks(
            $userId,
            'https://'.$request->getHttpHost().'/'
        );
        if ($recommendedBooks) {
            return Responses::apiResponse(
                ['books' => $recommendedBooks, 'books_count' => count($recommendedBooksCount)]
            );
        }

        return Responses::apiResponse(
            ['message' => 'book_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function addTagAction(Request $request)
    {

        $bookId = $request->get('book_id');
        $tagName = trim($request->get('tag_name'));
        $userId = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $tagExists = $entityManager->getRepository('ApiBundle:Tag')->findOneBy(['name' => $tagName]);

        if ($tagExists) {
            $tagId = $tagExists->getId();
            $bookTagExists = $entityManager->getRepository('ApiBundle:BookTags')->findOneBy(
                ['tagId' => $tagId, 'bookId' => $bookId]
            );

            if ($bookTagExists) {

                return Responses::apiResponse(
                    ['message' => 'have_this_tag'],
                    ErrorMessages::FAILURE_CODE,
                    ErrorMessages::FAILURE_MESSAGE
                );
            }
        } else {
            $tag = new Tag();
            $tag->setName($tagName);
            $validator = new Validator();
            $tag->setSlug($validator->slugify($tagName));

            $entityManager->persist($tag);
            $entityManager->flush();
            $tagId = $tag->getId();
        }

        $bookTag = new BookTags();
        $bookTag->setBookId($bookId);
        $bookTag->setTagId($tagId);
        $bookTag->setUserId($userId);

        $entityManager->persist($bookTag);
        $entityManager->flush();

        $output = [
            'tag_id' => $tagId,
            'book_id' => $bookId,
        ];

        return Responses::apiResponse($output);

    }

    public function getLibraryBooksAction($status, $userId = 0)
    {
        $request = Request::createFromGlobals();
        $userId = $userId ? $userId : $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $limit = $request->get('l');
        $offset = $request->get('o');
        $userExists = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(['id' => $userId]);
        if (!$userExists) {
            return Responses::apiResponse(
                ['message' => 'user_not_exists'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

        if($status == 'lend'){
            $userBooks = $entityManager->getRepository('ApiBundle:LentBooks')->findBy(['userId' => $userId, 'isApproved' => 1], ['lendDate' => 'DESC'], $limit, $offset);

        }elseif ($status == 'borrow'){
            $userBooks = $entityManager->getRepository('ApiBundle:LentBooks')->findBy(['lentTo' => $userId, 'isApproved' => 1], ['lendDate' => 'DESC'], $limit, $offset);
        }else{
            $userBooks = $entityManager->getRepository('ApiBundle:UserBooks')->findBy(
                ['userId' => $userId, 'status' => $status],
                ['modifiedDate' => 'DESC'],
                $limit,
                $offset
            );
        }

        if ($userBooks) {
            $output = array();

            foreach ($userBooks as $key => $userBook) {
                $bookId = $userBook->getBookId();
                $bookData = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $bookId]);
                $avgRate = $this->get('api.repository.rating')->getAvgBookRate($bookId);
                $bookAuthor = $this->get('api.repository.book')->getBookAuthor($bookId);
                $output['book_info'][$key]['id'] = $bookData->getId();
                $output['book_info'][$key]['book_id'] = $bookData->getBookId();
                $output['book_info'][$key]['title'] = $bookData->getTitle();
                $output['book_info'][$key]['publisher'] = $bookData->getPublisher();
                $output['book_info'][$key]['author'] = $bookAuthor ? $bookAuthor : '';
                $output['book_info'][$key]['publish_date'] = $bookData->getPublishDate()?$bookData->getPublishDate()->format('Y-m-d'):null;
                $output['book_info'][$key]['language'] = $bookData->getLanguage();
                $output['book_info'][$key]['image'] = $bookData->getImage();
                $output['book_info'][$key]['thumbnail'] = $bookData->getThumbnail() ? 'https://'.$request->getHttpHost(
                    ).'/'.$bookData->getThumbnail() : $bookData->getImage();
                $output['book_info'][$key]['rate'] = $avgRate ? $avgRate : 0;
            }

            if($status == 'lend'){
                $output['total_count'] = count($entityManager->getRepository('ApiBundle:LentBooks')->findBy(['userId' => $userId, 'isApproved' => 1]));

            }elseif ($status == 'borrow'){
                $output['total_count'] = count($entityManager->getRepository('ApiBundle:LentBooks')->findBy(['lentTo' => $userId, 'isApproved' => 1]));

            }else{
                $output['total_count'] = count(
                    $userBooks = $entityManager->getRepository('ApiBundle:UserBooks')->findBy(
                        ['userId' => $userId, 'status' => $status]
                    )
                );
            }


            return Responses::apiResponse($output);
        }

        return Responses::apiResponse(
            ['message' => 'book_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function getLibraryBooksCountAction( $userId = 0)
    {
        $request = Request::createFromGlobals();
        $userId = $userId ? $userId : $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $limit = $request->get('l');
        $offset = $request->get('o');
        $userExists = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(['id' => $userId]);
        if (!$userExists) {
            return Responses::apiResponse(
                ['message' => 'user_not_exists'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
        $result['have'] = $entityManager->getRepository('ApiBundle:UserBooks')->findBy(['userId' => $userId, 'status' => 'have'], ['modifiedDate' => 'DESC'], $limit, $offset);
        $result['reading'] = $entityManager->getRepository('ApiBundle:UserBooks')->findBy(['userId' => $userId, 'status' => 'reading'], ['modifiedDate' => 'DESC'], $limit, $offset);
        $result['wish'] = $entityManager->getRepository('ApiBundle:UserBooks')->findBy(['userId' => $userId, 'status' => 'wish'], ['modifiedDate' => 'DESC'], $limit, $offset);
        $result['lend'] = $entityManager->getRepository('ApiBundle:LentBooks')->findBy(['userId' => $userId, 'isApproved' => 1], ['lendDate' => 'DESC'], $limit, $offset);
        $result['borrow'] = $entityManager->getRepository('ApiBundle:LentBooks')->findBy(['lentTo' => $userId, 'isApproved' => 1], ['lendDate' => 'DESC'], $limit, $offset);

        if ($result) {
            $output['have']['book_info'] = [];
            $output['wish']['book_info'] = [];
            $output['reading']['book_info'] = [];
            $output['lend']['book_info'] = [];
            $output['borrow']['book_info'] = [];

            foreach ($result as $status => $bookDatas) {
                foreach ($bookDatas as $key => $myBookData) {

                    $bookId = $myBookData->getBookId();
                    $bookData = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $bookId]);
                    if($bookData){
                        $avgRate = $this->get('api.repository.rating')->getAvgBookRate($bookId);
                        $bookAuthor = $this->get('api.repository.book')->getBookAuthor($bookId);
                        $output[$status]['book_info'][$key]['id'] = $bookData->getId();
                        $output[$status]['book_info'][$key]['book_id'] = $bookData->getBookId();
                        $output[$status]['book_info'][$key]['title'] = $bookData->getTitle();
                        $output[$status]['book_info'][$key]['publisher'] = $bookData->getPublisher();
                        $output[$status]['book_info'][$key]['author'] = $bookAuthor ? $bookAuthor : '';
                        $output[$status]['book_info'][$key]['publish_date'] = $bookData->getPublishDate()?$bookData->getPublishDate()->format('Y-m-d'):null;
                        $output[$status]['book_info'][$key]['language'] = $bookData->getLanguage();
                        $output[$status]['book_info'][$key]['image'] = $bookData->getImage();
                        $output[$status]['book_info'][$key]['thumbnail'] = $bookData->getThumbnail() ? 'https://'.$request->getHttpHost(
                            ).'/'.$bookData->getThumbnail() : $bookData->getImage();
                        $output[$status]['book_info'][$key]['rate'] = $avgRate ? $avgRate : 0;
                    }
                }
            }
            $output['have']['book_info'] = array_values($output['have']['book_info']);
            $output['reading']['book_info'] = array_values($output['reading']['book_info']);
            $output['wish']['book_info'] =array_values($output['wish']['book_info']);

            $userBooksHave = $entityManager->getRepository('ApiBundle:UserBooks')->findBy(
                ['userId' => $userId, 'status' => 'have']
            );
            $userBooksReading = $entityManager->getRepository('ApiBundle:UserBooks')->findBy(
                ['userId' => $userId, 'status' => 'reading']
            );
            $userBooksWish = $entityManager->getRepository('ApiBundle:UserBooks')->findBy(
                ['userId' => $userId, 'status' => 'wish']
            );
            $userBookLend = $entityManager->getRepository('ApiBundle:LentBooks')->findBy(['userId' => $userId, 'isApproved' => 1]);
            $userBookBorrow = $entityManager->getRepository('ApiBundle:LentBooks')->findBy(['lentTo' => $userId, 'isApproved' => 1]);

            $output['have']['total_count'] =  $userBooksHave?count($userBooksHave):0;
            $output['reading']['total_count'] =  $userBooksReading?count($userBooksReading):0;
            $output['wish']['total_count'] =  $userBooksWish?count($userBooksWish):0;
            $output['lend']['total_count'] =  $userBookLend?count($userBookLend):0;
            $output['borrow']['total_count'] =  $userBookBorrow?count($userBookBorrow):0;
            $output['total_count'] = count($userBooksHave) + count($userBooksReading) + count($userBooksWish)+count($userBookLend)+count($userBookBorrow);

            return Responses::apiResponse($output);
        }
        return Responses::apiResponse(
            ['message' => 'book_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function getFilteredBooksAction($userId = 0)
    {
        $request = Request::createFromGlobals();
        $userId = $userId ? $userId : $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $limit = $request->get('limit');
        $offset = $request->get('offset');
        $searchText = $request->get('search_text');
        $searchBy = $request->get('search_by')?$request->get('search_by'):'title';
        $status = $request->get('status');
        if(!in_array($status, $this->bookStatus)){
            return Responses::apiResponse(
                ['message' => 'incorrect_parameter'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
        $userExists = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(['id' => $userId]);
        if (!$userExists) {
            return Responses::apiResponse(
                ['message' => 'user_not_exists'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

        $bookDatas = $this->get('api.repository.book')->getBookByFilter(
            $userId,
            $status,
            $searchText,
            strtolower($searchBy),
            $limit,
            $offset
        );

        if ($bookDatas) {
            $output = array();
            foreach ($bookDatas as $key => $bookData) {
                $avgRate = $this->get('api.repository.rating')->getAvgBookRate($bookData['id']);
                $bookAuthor = $this->get('api.repository.book')->getBookAuthor($bookData['id']);

                $output['book_info'][$key]['id'] = $bookData['id'];
                $output['book_info'][$key]['book_id'] = $bookData['book_id'];
                $output['book_info'][$key]['title'] = $bookData['title'];
                $output['book_info'][$key]['publisher'] = $bookData['publisher'];
                $output['book_info'][$key]['author'] = $bookAuthor ? $bookAuthor : '';;
                $output['book_info'][$key]['publish_date'] = $bookData['publish_date'];
                $output['book_info'][$key]['language'] = $bookData['language'];
                $output['book_info'][$key]['image'] = $bookData['image'];
                $output['book_info'][$key]['thumbnail'] = $bookData['thumbnail'] ? 'https://'.$request->getHttpHost(
                    ).'/'.$bookData['thumbnail'] : $bookData['image'];
                $output['book_info'][$key]['rate'] = $avgRate ? $avgRate : 0;
            }

            $bookCount = $this->get('api.repository.book')->getBookByFilterCount(
                $userId,
                $status,
                $searchText,
                $searchBy
            );
            $output['total_count'] =  $bookCount?count($bookCount):0;
            return Responses::apiResponse($output);

        }

        return Responses::apiResponse(
            ['message' => 'book_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }


    public function getAllFilteredBooksAction($userId = 0)
    {
        $request = Request::createFromGlobals();
        $userId = $userId ? $userId : $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $limit = $request->get('limit');
        $offset = $request->get('offset');
        $searchText = $request->get('search_text');
        $searchBy = $request->get('search_by')?strtolower($request->get('search_by')):'title';

        $userExists = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(['id' => $userId]);
        if (!$userExists) {
            return Responses::apiResponse(
                ['message' => 'user_not_exists'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

        $result['have']= $this->get('api.repository.book')->getBookByFilter($userId, 'have', $searchText, $searchBy, $limit, $offset);
        $result['wish']= $this->get('api.repository.book')->getBookByFilter($userId, 'wish', $searchText, $searchBy, $limit, $offset);
        $result['reading']= $this->get('api.repository.book')->getBookByFilter($userId, 'reading', $searchText, $searchBy, $limit, $offset);
        $result['lend']= $this->get('api.repository.book')->getBookByFilter($userId, 'lend', $searchText, $searchBy, $limit, $offset);
        $result['borrow']= $this->get('api.repository.book')->getBookByFilter($userId, 'borrow', $searchText, $searchBy, $limit, $offset);

        if ($result) {
            $output['have']['book_info'] = [];
            $output['wish']['book_info'] = [];
            $output['reading']['book_info'] = [];
            $output['lend']['book_info'] = [];
            $output['borrow']['book_info'] = [];
            foreach ($result as $status => $bookDatas) {
                foreach ($bookDatas as $key => $bookData) {
                    $avgRate = $this->get('api.repository.rating')->getAvgBookRate($bookData['id']);
                    $bookAuthor = $this->get('api.repository.book')->getBookAuthor($bookData['id']);

                    $output[$status]['book_info'][$key]['id'] = $bookData['id'];
                    $output[$status]['book_info'][$key]['book_id'] = $bookData['book_id'];
                    $output[$status]['book_info'][$key]['title'] = $bookData['title'];
                    $output[$status]['book_info'][$key]['publisher'] = $bookData['publisher'];
                    $output[$status]['book_info'][$key]['author'] = $bookAuthor ? $bookAuthor : '';;
                    $output[$status]['book_info'][$key]['publish_date'] = $bookData['publish_date'];
                    $output[$status]['book_info'][$key]['language'] = $bookData['language'];
                    $output[$status]['book_info'][$key]['image'] = $bookData['image'];
                    $output[$status]['book_info'][$key]['thumbnail'] = $bookData['thumbnail'] ? 'https://'.$request->getHttpHost(
                        ).'/'.$bookData['thumbnail'] : $bookData['image'];
                    $output[$status]['book_info'][$key]['rate'] = $avgRate ? $avgRate : 0;
                }
            }

            $haveBookCount = $this->get('api.repository.book')->getBookByFilterCount($userId, 'have', $searchText, $searchBy);
            $wishBookCount = $this->get('api.repository.book')->getBookByFilterCount($userId, 'wish', $searchText, $searchBy);
            $readingBookCount = $this->get('api.repository.book')->getBookByFilterCount($userId, 'reading', $searchText, $searchBy);
            $lendBookCount = $this->get('api.repository.book')->getBookByFilterCount($userId, 'lend', $searchText, $searchBy);
            $borrowBookCount = $this->get('api.repository.book')->getBookByFilterCount($userId, 'borrow', $searchText, $searchBy);
            $output['have']['total_count'] =  $haveBookCount?count($haveBookCount):0;
            $output['wish']['total_count'] =  $wishBookCount?count($wishBookCount):0;
            $output['reading']['total_count'] =  $readingBookCount?count($readingBookCount):0;
            $output['lend']['total_count'] =  $lendBookCount?count($lendBookCount):0;
            $output['borrow']['total_count'] =  $borrowBookCount?count($borrowBookCount):0;
            $output['total_count'] = $output['have']['total_count']+$output['wish']['total_count']+$output['reading']['total_count']+$output['borrow']['total_count']+$output['lend']['total_count'];

            return Responses::apiResponse($output);

        }

        return Responses::apiResponse(
            ['message' => 'book_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function likeBookAction($bookId)
    {

        $userId = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $bookExists = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $bookId]);
        if ($bookExists) {
            $likeExists = $entityManager->getRepository('ApiBundle:Likes')->findOneBy(
                ['userId' => $userId, 'bookId' => $bookId]
            );

            if ($likeExists) {
                $likeExists->setLike(!$likeExists->getLike());

            } else {
                $likeExists = new Likes();
                $likeExists->setLike(1);
                $likeExists->setUserId($userId);
                $likeExists->setBookId($bookId);
                $likeExists->setModifiedDate(new \DateTime());
            }
            $entityManager->persist($likeExists);
            $entityManager->flush();

            $likeCount = count(
                $entityManager->getRepository('ApiBundle:Likes')->findBy(['bookId' => $bookId, 'likes' => 1])
            );

            $bookLikeCount = $bookExists->setLikeCount($likeCount);
            $entityManager->persist($bookLikeCount);
            $entityManager->flush();

            $output = [
                'liked' => $likeExists->getLike(),
                'book_id' => $bookId,
                'likes_count' => $likeCount,
            ];

            return Responses::apiResponse($output);
        }

        return Responses::apiResponse(
            ['message' => 'book_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function getBookRatesCountAction($bookId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $bookExists = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $bookId]);

        if ($bookExists) {
            $ratings = $entityManager->getRepository('ApiBundle:Rating')->findBy(['bookId' => $bookId]);
            $ratingCount = ['rate_count' => count($ratings)];

            return Responses::apiResponse($ratingCount);
        }

        return Responses::apiResponse(
            ['message' => 'book_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function getBooksByAuthorAction($authorId, Request $request)
    {

        $startPage = $request->get('offset') ? $request->get('offset') : '1';
        $limit = $request->get('limit') ? $request->get('limit') : '10';
        $searchLang = $request->get('lang') ? $request->get('lang') : 'english';
        $userId = $request->get('u') ? $request->get('u') : 0;
        $entityManager = $this->getDoctrine()->getManager();
        if ($startPage == 1) {
            $offset = 0;
        } else {
            $offset = $startPage * $limit - 1 - ($limit - 1);
        }

        $results = $this->get('api.repository.book')->searchInYotta($authorId, false, true);

        if ($results) {
            $bookData = array();
            foreach ($results as $key => $result) {

                $bookAuthor = $this->get('api.repository.book')->getBookAuthor($result['id']);
                $bookData[$key]['id'] = $result['id'];
                $bookData[$key]['title'] = $result['title'];
                $bookData[$key]['author'] = $bookAuthor ? $bookAuthor : '';
                $bookData[$key]['publisher'] = $result['publisher'];
                $bookData[$key]['publish_date'] = $result['publish_date'] ? $result['publish_date'] : '';
                $bookData[$key]['language'] = $result['language'];
                $bookData[$key]['description'] = $result['description'];
                $bookData[$key]['image'] = $result['image'];
                $bookData[$key]['thumbnail'] = $result['thumbnail'] ? 'https://'.$request->getHttpHost(
                    ).'/'.$result['thumbnail'] : $result['image'];
                $bookData[$key]['book_id'] = $result['book_id'];
                $bookData[$key]['book_url'] = $result['book_url'];
                $bookInLib = null;
                if ($userId) {
                    $bookInLib = $entityManager->getRepository('ApiBundle:UserBooks')->findOneBy(
                        ['bookId' => $result['id'], 'userId' => $userId]
                    );

                }

                $bookData[$key]['status'] = $bookInLib ? $bookInLib->getStatus() : null;
                $bookData[$key]['library_id'] = $bookInLib ? $bookInLib->getId() : null;
                unset($results[$key]['authors']);

            }
        }

        $mapping = array(
            'AmazonProductAPI' => $this->getParameter('amazon_api'),
        );
        $totalCount = count($bookData);
        $totalPages = ceil(($totalCount + 10) / $limit);

        if ($totalPages >= $startPage) {
            $amazonStartPage = 1;
        } else {
            $amazonStartPage = $startPage - $totalPages + 1;
            $offset = $totalCount;
        }
        $loader = new MapClassLoader($mapping);
        $loader->register();
        $amazonSearch = new AmazonSearch();
        $authorName = $entityManager->getRepository('ApiBundle:Authors')->findOneBy(['id' => $authorId]);
        if (!$authorName) {
            return Responses::apiResponse(
                ['message' => 'author_not_found'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
        $amazonResult = $amazonSearch->searchInAmazon(
            $authorName->getPublicName(),
            $searchLang,
            $amazonStartPage,
            true
        );
        if ($amazonStartPage > 1) {
            $output['book_info'] = $amazonResult['book_info'];
        } else {
            $output['book_info'] = array_merge($bookData, $amazonResult['book_info'] ? $amazonResult['book_info'] : []);
            $books = array_slice($output['book_info'], $offset, $limit, true);
            $output['book_info'] = array_values($books);
        }

        if ($amazonResult['totalCount'] > 100) {
            $output['totalCount'] = $totalCount + 100;
        } else {
            $output['totalCount'] = $totalCount + $amazonResult['totalCount'];
        }

        $output['totalPages'] = ceil($output['totalCount'] / $limit);

        if (!empty($output['book_info'])) {
            $status = ErrorMessages::SUCCESS_MESSAGE;
            $code = 200;
        } else {
            $output = ['message' => 'book_not_found'];
            $status = ErrorMessages::FAILURE_MESSAGE;
            $code = ErrorMessages::FAILURE_CODE;
        }

        return Responses::apiResponse($output, $code, $status);


    }

    public function rateBookAction($bookId, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $rate = $request->get('rate');
        $userId = $this->getUser()->getId();
        $bookExists = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $bookId]);
        if ($bookExists) {
            $rateExists = $entityManager->getRepository('ApiBundle:Rating')->findOneBy(
                ['bookId' => $bookId, 'userId' => $userId]
            );
            $currentCoin = 0;
            $date = new \DateTime();
            if ($rateExists) {
                $rateExists->setRateDate($date);
                $rateExists->setStars($rate);
                $coins = $entityManager->getRepository('ApiBundle:Coins')->findOneBy(['userId' => $userId]);
                $coinsCount = $coins->getCoin();
            } else {
                $rateExists = new Rating();
                $rateExists->setStars($rate);
                $rateExists->setRateDate($date);
                $rateExists->setBookId($bookId);
                $rateExists->setUserId($userId);
                $coinRepository = $this->get('api.repository.coin')->addCoin($userId, 'REVIEW_BOOK');
                $currentCoin = $coinRepository['coin_point'];
                $coinsCount = $coinRepository['result'];
            }
            $entityManager->persist($rateExists);
            $entityManager->flush();
            $rateCount = $entityManager->getRepository('ApiBundle:Rating')->findBy(
                ['bookId' => $bookId]
            );
            $bookRateCount = $bookExists->setRateCount(count($rateCount));
            $entityManager->persist($bookRateCount);
            $entityManager->flush();
            $avgRate = $this->get('api.repository.rating')->getAvgBookRate($bookId);

            return Responses::apiResponse(
                [
                    'rate_id' => $rateExists->getId(),
                    'rating' => $rateExists->getStars(),
                    'avg_rate' => $avgRate?floatval($avgRate):0,
                    'rate_count' => count($rateCount),
                    'current_coin' => $currentCoin,
                    'coins_count' => $coinsCount,
                ]
            );
        }

        return Responses::apiResponse(
            ['message' => 'book_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );

    }

    public function addBookStatusAction($bookId, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $status = $request->get('status');

        if (!in_array($status, $this->bookStatus)) {
            return Responses::apiResponse(
                ['message' => 'status_not_correct'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
        $userId = $this->getUser()->getId();
        $bookExists = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $bookId]);
        if ($bookExists) {
            $bookExistsInLibrary = $entityManager->getRepository('ApiBundle:UserBooks')->findOneBy(
                ['bookId' => $bookId, 'userId' => $userId]
            );
            $date = new \DateTime();
            $currentCoin = 0;

            if ($bookExistsInLibrary) {
                $bookExistsInLibrary->setStatus($status);
                $bookExistsInLibrary->setModifiedDate($date);
                $key = 'moved';
                $coins = $entityManager->getRepository('ApiBundle:Coins')->findOneBy(['userId' => $userId]);
                $coinsCount = $coins->getCoin();
            } else {
                $bookExistsInLibrary = new UserBooks();
                $bookExistsInLibrary->setStatus($status);
                $bookExistsInLibrary->setUserId($userId);
                $bookExistsInLibrary->setBookId($bookId);
                $bookExistsInLibrary->setModifiedDate($date);
                $key = 'added';
                $coinRepository = $this->get('api.repository.coin')->addCoin($userId, 'ADD_BOOK');
                $currentCoin = $coinRepository['coin_point'];
                $coinsCount = $coinRepository['result'];

            }
            $posts = new Posts();
            $posts->setBookId($bookId);
            $posts->setUserFriendId($userId);
            $posts->setFriendId($this->getUser()->getId());
            $posts->setDate($date);
            $posts->setType('book');
            $posts->setNote($this->get('translator')->trans($key.'_'.$status.'_list'));
            $entityManager->persist($posts);
            $entityManager->flush();
            $entityManager->persist($bookExistsInLibrary);
            $entityManager->flush();

            return Responses::apiResponse(
                [
                    'book_id' => $bookExistsInLibrary->getBookId(),
                    'status' => $bookExistsInLibrary->getStatus(),
                    'current_coin' => $currentCoin,
                    'coins_count' => $coinsCount,
                ]
            );
        }

        return Responses::apiResponse(
            ['message' => 'book_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );

    }

    public function getBookTagsAction($bookId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $bookExists = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $bookId]);

        if ($bookExists) {
            $tags['tags'] = $this->get('api.repository.book')->getBookTags($bookId);
            $tags['tag_count'] = count($this->get('api.repository.book')->getBookTags($bookId));
            if ($tags) {
                return Responses::apiResponse($tags);
            }

            return Responses::apiResponse(
                ['message' => 'tags_not_found'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

        return Responses::apiResponse(
            ['message' => 'book_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function getTagBooksAction($slug, Request $request)
    {
        $offset = $request->get('o') ? $request->get('o') : '0';
        $limit = $request->get('l') ? $request->get('l') : '10';
        $userId = $request->get('u') ? $request->get('u') : 0;
        $entityManager = $this->getDoctrine()->getManager();
        $results = $this->get('api.repository.book')->getTagBooks($slug, $limit,$offset);
        $bookCount = $this->get('api.repository.book')->getTagBooksCount($slug);
        if ($results) {
            $bookData = array();
            foreach ($results as $key => $result) {

                $bookAuthor = $this->get('api.repository.book')->getBookAuthor($result['id']);
                $bookData[$key]['id'] = $result['id'];
                $bookData[$key]['title'] = $result['title'];
                $bookData[$key]['author'] = $bookAuthor ? $bookAuthor : '';
                $bookData[$key]['publisher'] = $result['publisher'];
                $bookData[$key]['publish_date'] = $result['publishDate'] ? $result['publishDate']->format('Y-m-d') : '';
                $bookData[$key]['language'] = $result['language'];
                $bookData[$key]['image'] = $result['image'];
                $bookData[$key]['thumbnail'] = $result['thumbnail'] ? 'https://'.$request->getHttpHost(
                    ).'/'.$result['thumbnail'] : $result['image'];
                $bookData[$key]['book_id'] = $result['bookId'];
                $bookData[$key]['book_url'] = $result['bookUrl'];
                $bookInLib = null;
                if ($userId) {
                    $bookInLib = $entityManager->getRepository('ApiBundle:UserBooks')->findOneBy(
                        ['bookId' => $result['id'], 'userId' => $userId]
                    );

                }

                $bookData[$key]['status'] = $bookInLib ? $bookInLib->getStatus() : null;
                $bookData[$key]['library_id'] = $bookInLib ? $bookInLib->getId() : null;
                unset($results[$key]['authors']);
            }
            $output['books'] = $bookData;
            $output['book_count'] = $bookCount;
            return Responses::apiResponse($output);
        }
        return Responses::apiResponse(
            ['message' => 'book_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function addBookPresentAction( Request $request){
        $friendId = $request->get('friend_id');
        $bookId = $request->get('book_id') ;
        $userId = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $userExists = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(['id' => $friendId]);
        $bookExists = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $bookId]);
        if($userExists){
            if($bookExists){
                $bookPresentExists = $entityManager->getRepository('ApiBundle:BookPresent')->findOneBy(['userId'=>$userId,'bookId'=>$bookId,'friendId'=>$friendId]);
                if($bookPresentExists){
                    return Responses::apiResponse(
                        ['message' => 'already_have_present'],
                        ErrorMessages::FAILURE_CODE,
                        ErrorMessages::FAILURE_MESSAGE
                    );
                }
                $bookPresent = new BookPresent();
                $bookPresent->setUserId($userId);
                $bookPresent->setFriendId($friendId);
                $bookPresent->setBookId($bookId);

                $entityManager->persist($bookPresent);
                $entityManager->flush();

                return Responses::apiResponse(['added'=>true,'id'=>$bookPresent->getId()]);
            }
            return Responses::apiResponse(
                ['message' => 'book_not_found'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
        return Responses::apiResponse(
            ['message' => 'user_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }
}

<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\UserInfo;
use ApiBundle\Services\Validator;
use ApiBundle\Utils\ErrorMessages;
use ApiBundle\Utils\Responses;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;


class UserController extends Controller
{

    public function addDeviceTokenAction(Request $request)
    {
        try {
            $userId = $this->getUser()->getId();
            $deviceToken = $request->get('token');
            $deviceType = $request->get('type');
            $entityManager = $this->getDoctrine()->getManager();
            $userInfo = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(['userId' => $userId]);
            $userInfo->setDeviceToken($deviceToken);
            $userInfo->setDeviceType(strtolower($deviceType));

            $entityManager->flush();

            return Responses::apiResponse(['message' => 'token_updated']);

        } catch (Exception $exception) {
            return $exception->getMessage();
        }

    }

    public function getOnlineFriendsAction(Request $request)
    {

        $offset = intval($request->query->get('o'));
        $limit = intval($request->query->get('l'));
        $userId = $this->getUser()->getId();

        $userInfo = $this->get('api.repository.user')->getOnlineFriends($userId, $limit, $offset);
        $friendsCount = $this->get('api.repository.user')->getOnlineFriendsCount($userId);
        $entityManager = $this->getDoctrine()->getManager();
        $notSeenFriendRequests = $entityManager->getRepository('ApiBundle:Friends')->findBy(
            ['status' => 'waiting', 'seenStatus' => 'unseen','friendId'=>$userId]
        );
        $user = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(['userId' => $userId]);
        $user->setLastActivityDate(new \DateTime());
        $entityManager->persist($user);
        $entityManager->flush();

        return Responses::apiResponse(
            [
                'users' => $userInfo ? $userInfo : null,
                'user_count' => $friendsCount ? count($friendsCount) : 0,
                'not_seen_friends' => $notSeenFriendRequests ? count($notSeenFriendRequests) : 0,
            ]
        );

    }


    public function getPopularUsersAction($type, Request $request)
    {

        $offset = intval($request->query->get('o'));
        $limit = intval($request->query->get('l'));

        $userInfo = $this->get('api.repository.user')->getPopularUsers($type, $limit, $offset);
        if ($userInfo) {
            return Responses::apiResponse($userInfo);
        }

        return Responses::apiResponse(
            ['message' => 'no_users_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );

    }

    public function updateUserLastActiveDateAction(Request $request)
    {

        $userId = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $userInfo = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(['userId' => $userId]);
        $date = new \DateTime();

        if ($userInfo) {
            $offset = intval($request->query->get('o'));
            $limit = intval($request->query->get('l'));

            $users = $this->get('api.repository.user')->getOnlineFriends($userId, $limit, $offset);
            $friendsCount = $this->get('api.repository.user')->getOnlineFriendsCount($userId);

            try {
                $userInfo->setLastActivityDate($date);
                $entityManager->persist($userInfo);
                $entityManager->flush();

                return Responses::apiResponse(
                    ['user_id' => $userId, 'updated' => true, 'users' => $users, 'user_count' => count($friendsCount)]
                );
            } catch (\Exception $e) {
                return Responses::apiResponse(
                    ['message' => 'no_online_users'],
                    ErrorMessages::FAILURE_CODE,
                    ErrorMessages::FAILURE_MESSAGE
                );
            }
        } else {
            try {
                $userInfo = new UserInfo();
                $userInfo->setUserId($userId);
                $userInfo->setLastActivityDate($date);
                $entityManager->persist($userInfo);
                $entityManager->flush();

                return Responses::apiResponse(['user_id' => $userId, 'updated' => true]);
            } catch (\Exception $e) {

                return Responses::apiResponse(
                    ['message' => 'no_online_users'],
                    ErrorMessages::FAILURE_CODE,
                    ErrorMessages::FAILURE_MESSAGE
                );
            }

        }
    }

    public function getUserActivitiesAction($userId, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $currentUserId = $this->getUser()->getId();
        $offset = $request->get('o');
        $limit = $request->get('l');
        $individual = true;
        $validator = new Validator();
        $userExists = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(['id' => $userId]);
        if (!$userExists) {
            return Responses::apiResponse(
                ['message' => 'user_not_exists'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
        if ($userId == $currentUserId) {
            $individual = false;
        }
        $postsData = $this->get('api.repository.user')->getUserFriendPosts($userId, $offset, $limit, $individual);
        $output = [];
        if ($postsData['posts']) {
            $posts = $postsData['posts'];
            foreach ($posts as $key => $post) {

                if ($post['user_id']) {
                    $userData = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(
                        ['userId' => $post['user_id']]
                    );
                    if ($userData) {
                        $posts[$key]['user_data']['id'] = $userData->getUserId();
                        $posts[$key]['user_data']['added_date'] = $post['post_date'] ? $post['post_date'] : "";
                        $posts[$key]['user_data']['first_name'] = $userData->getFirstname();
                        $posts[$key]['user_data']['last_name'] = $userData->getLastname();
                        $posts[$key]['user_data']['image'] = $validator->generateCorrectProfilePicture(
                            $userData->getPicture()
                        );
                        $posts[$key]['user_data']['thumbnail'] = $validator->generateCorrectProfilePicture(
                            str_replace('img/profile/', 'img/profile/thumbnail/', $userData->getPicture())
                        );
                    }
                }
                if ($post['post_book_id']) {
                    $books = $entityManager->getRepository('ApiBundle:Books')->findOneBy(
                        ['id' => $post['post_book_id']]
                    );

                    if ($books) {
                        $bookUsersCount = $entityManager->getRepository('ApiBundle:UserBooks')->findBy(
                            ['bookId' => $post['post_book_id']]
                        );
                        if ($bookUsersCount) {
                            $userCount = count($bookUsersCount);
                        } else {
                            $userCount = 0;
                        }
                        $bookInLib = $entityManager->getRepository('ApiBundle:UserBooks')->findOneBy(
                            ['bookId' => $post['post_book_id'], 'userId' => $userId]
                        );
                        $ratings = $entityManager->getRepository('ApiBundle:Rating')->findBy(
                            ['bookId' => $post['post_book_id']]
                        );
                        $ratingCount = count($ratings);
                        $bookAuthor = $this->get('api.repository.book')->getBookAuthor($books->getId());
                        $like = $entityManager->getRepository('ApiBundle:Likes')->findOneBy(
                            ['bookId' => $post['post_book_id'], 'userId' => $userId]
                        );
                        $myLike = $like ? $like->getLike() : false;
                        $posts[$key]['book_data']['title'] = $books->getTitle();
                        $posts[$key]['book_data']['author'] = $bookAuthor ? $bookAuthor : '';
                        $posts[$key]['book_data']['status'] = $bookInLib ? $bookInLib->getStatus() : null;
                        $posts[$key]['book_data']['library_id'] = $bookInLib ? $bookInLib->getId() : null;
                        $posts[$key]['book_data']['publisher'] = $books->getPublisher();
                        $posts[$key]['book_data']['publish_date'] = $books->getPublishDate() ? $books->getPublishDate(
                        )->format('Y-m-d H:i:s') : "";
                        $posts[$key]['book_data']['image'] = $books->getImage();
                        $posts[$key]['book_data']['thumbnail'] = $books->getThumbnail(
                        ) ? 'https://'.$request->getHttpHost().'/'.$books->getThumbnail() : '';
                        $posts[$key]['book_data']['description'] = $books->getDescription();
                        $posts[$key]['book_data']['book_url'] = $books->getBookUrl();
                        $posts[$key]['book_data']['id'] = $books->getId();
                        $posts[$key]['book_data']['book_id'] = $books->getBookId();
                        $posts[$key]['book_data']['language'] = $books->getLanguage();
                        $posts[$key]['book_data']['my_like'] = $myLike;
                        $posts[$key]['book_data']['my_rate'] = intval($post['my_rate']);
                        $posts[$key]['book_data']['rate_count'] = $ratingCount;
                        $bookRate = $this->get('api.repository.rating')->getAvgBookRate($post['post_book_id']);
                        $posts[$key]['book_data']['book_rate'] = $bookRate ? floatval($bookRate) : 0;
                        $bookTags = $this->get('api.repository.book')->getBookTags($post['post_book_id']);
                        $posts[$key]['book_data']['book_tags_count'] = $bookTags ? count($bookTags) : 0;
                        $posts[$key]['book_data']['book_tags'] = $bookTags ? $bookTags : null;
                        $posts[$key]['book_data']['book_users_count'] = $userCount;
                        $posts[$key]['book_data']['likes_count'] = count(
                            $entityManager->getRepository('ApiBundle:Likes')->findBy(
                                ['bookId' => $post['post_book_id'], 'likes' => 1]
                            )
                        );
                        $posts[$key]['book_data']['markups_count'] = intval(
                            $this->get('api.repository.markup')->getAllMarkupsCount(
                                $post['post_book_id']
                            )
                        );
                        unset($posts[$key]['my_like']);
                        unset($posts[$key]['my_rate']);
                    }
                }
                if ($post['markup_id']) {

                    $bookMarkup = $entityManager->getRepository('ApiBundle:BookMarkups')->findOneBy(
                        ['id' => $post['markup_id']]
                    );
                    if ($bookMarkup) {
                        $markups = $entityManager->getRepository('ApiBundle:Markups')->findOneBy(
                            ['id' => $bookMarkup->getMarkupId()]
                        );

                        if ($markups) {
                            $posts[$key]['markup_data']['title'] = $markups->getTitle();
                            $posts[$key]['markup_data']['preview_url'] = $markups->getPreviewUrl();
                            $posts[$key]['markup_data']['content'] = $bookMarkup->getContent();
                            $posts[$key]['markup_data']['description'] = $markups->getMarkupDesc();
                            $posts[$key]['markup_data']['music_author_image'] = $markups->getMusicAuthorImg();
                            $posts[$key]['markup_data']['music_track_url'] = $markups->getMusicTrackUrl();
                            $posts[$key]['markup_data']['music_author'] = $markups->getMusicAuthor();
                            $posts[$key]['markup_data']['date'] = $markups->getModifyDate()->format('Y-m-d H:i:s');
                        }
                    }


                }
                if ($post['user_friend_id']) {
                    $friendData = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(
                        ['userId' => $post['user_friend_id']]
                    );

                    if ($friendData) {
                        $posts[$key]['friend_data']['id'] = $friendData->getUserId();
                        $posts[$key]['friend_data']['first_name'] = $friendData->getFirstname();
                        $posts[$key]['friend_data']['last_name'] = $friendData->getLastname();
                        $posts[$key]['friend_data']['image'] = $validator->generateCorrectProfilePicture(
                            $friendData->getPicture()
                        );
                        $posts[$key]['friend_data']['thumbnail'] = $validator->generateCorrectProfilePicture(
                            str_replace('img/profile/', 'img/profile/thumbnail/', $friendData->getPicture())
                        );
                    }
                }
            }

            $output['posts'] = $posts;
            $output['post_count'] = $postsData['post_count'];

            return Responses::apiResponse($output);
        }

        return Responses::apiResponse(
            ['message' => 'posts_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function deleteUserPostAction($postId, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $currentUserId = $this->getUser()->getId();
        $post = $entityManager->getRepository('ApiBundle:Posts')->findOneBy(
            ['id' => $postId, 'friendId' => $currentUserId]
        );
        if ($post) {
            $entityManager->remove($post);
            $entityManager->flush();
            $output = [
                'post_id' => $postId,
                'message' => 'success_deleted',
            ];

            return Responses::apiResponse($output);
        }

        return Responses::apiResponse(
            ['message' => 'post_not_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function getUserDataAction($userId = 0)
    {

        $userId = $userId ? $userId : $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $userData = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(['userId' => $userId]);
        $userAccessData = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(['id' => $userId]);

        if ($userData && $userAccessData) {
            $output = array();
            $lastActive = $userData->getLastActivityDate();
            $lastActive->modify('+'.ErrorMessages::USER_ONLINE_DURATION.' minute');
            $activeDate = $lastActive->format('Y-m-d H:i:s');
            $date = new \DateTime();
            $curDate = $date->format('Y-m-d H:i:s');
            $status = 'offline';
            if ($activeDate >= $curDate) {
                $status = 'online';
            }
            $validator = new Validator();

            $output['id'] = $userData->getUserId();
            $output['username'] = $userAccessData->getUsername();
            $output['email'] = $userAccessData->getEmail();
            $output['first_name'] = $userData->getFirstname();
            $output['last_name'] = $userData->getLastname();
            $output['image'] = $validator->generateCorrectProfilePicture($userData->getPicture());
            $output['thumbnail'] = $validator->generateCorrectProfilePicture(
                str_replace('img/profile/', 'img/profile/thumbnail/', $userData->getPicture())
            );
            $output['profile'] = $userData->getProfile();
            $output['zip_code'] = $userData->getZipCode();
            $output['facebook_id'] = $userAccessData->getFacebookUserId();
            $output['twitter_id'] = $userAccessData->getTwitterUserId();
            $output['instagram_id'] = $userAccessData->getInstagramUserId();
            $output['pinterest_id'] = $userAccessData->getPinterestUserId();
            $output['status'] = $status;

            return Responses::apiResponse($output);
        }

        return Responses::apiResponse(
            ['message' => 'no_users_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function updateUserImageAction(Request $request)
    {

        $userId = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $userData = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(['userId' => $userId]);

        if ($userData) {
            try {
                $file = $request->files->get('image');
                $thumbnail = array('thumbnail' => true, 'padding' => false);
                $upload = $this->get('api.service.image_upload')->upload($file, $thumbnail, null, 'user');
                $filePath = 'api/web/img/profile/'.$upload['newName'];
                $thumbnailPath = 'api/web/img/profile/thumbnail/'.$upload['newName'];

                $userData->setPicture($filePath);
                $entityManager->persist($userData);
                $entityManager->flush();

                return Responses::apiResponse(
                    [
                        'image' => $request->getHttpHost().'/'.$filePath,
                        'thumbnail' => $request->getHttpHost().'/'.$thumbnailPath,
                    ]
                );

            } catch (Exception $e) {

                return Responses::apiResponse(
                    ['message' => 'file_not_uploaded'],
                    ErrorMessages::FAILURE_CODE,
                    ErrorMessages::FAILURE_MESSAGE
                );
            }
        }

        return Responses::apiResponse(
            ['message' => 'no_users_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function updateUserDataAction(Request $request)
    {

        $firstName = $request->get('first_name');
        $lastName = $request->get('last_name');
        $zipCode = $request->get('zip_code');
        $email = $request->get('email');
        $userId = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();

        $userInfo = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(['userId' => $userId]);
        $apiUser = $entityManager->getRepository('ApiBundle:ApiUsers')->findOneBy(['id' => $userId]);
        $userManager = $this->get('fos_user.user_manager');
        $checkEmail = $userManager->findUserByEmail($email);

        if ($checkEmail && $checkEmail->getId() != $userId) {
            return Responses::apiResponse(
                ['message' => 'email_exists'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
        if ($apiUser) {

            if (!$userInfo) {
                $userInfo = new UserInfo();
            }
            $userInfo->setLastname($lastName);
            $userInfo->setFirstname($firstName);
            $userInfo->setZipCode($zipCode);
            $apiUser->setEmail($email);
            $apiUser->setEmailCanonical($email);

            $entityManager->persist($userInfo);
            $entityManager->persist($apiUser);
            $entityManager->flush();

            $output = [
                'id' => $userInfo->getUserId(),
                'first_name' => $userInfo->getFirstname(),
                'last_name' => $userInfo->getLastname(),
                'zip_code' => $userInfo->getZipCode(),
                'email' => $apiUser->getEmail(),
            ];

            return Responses::apiResponse($output);

        }

        return Responses::apiResponse(
            ['message' => 'no_users_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );

    }

    public function updateUserAccessDataAction(Request $request)
    {

        $username = $request->get('username');
        $password = $request->get('password');
        $userId = $this->getUser()->getId();

        $Validator = new Validator();
        $validPassword = $Validator->validatePassword($password);
        if (!$validPassword) {
            $errors["password"] = 'password_validation';
        }

        $validUsername = $Validator->validateUsername($username);
        if (!$validUsername) {
            $errors["username"] = 'username_validation';
        }

        if ($errors) {
            return Responses::apiResponse(
                $errors,
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }

        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository('ApiBundle:ApiUsers');
        $apiUser = $user->findOneBy(['id' => $userId]);
        $usernameExists = $this->get('api.repository.user')->checkUserExists($username, $userId);

        if ($usernameExists) {
            return Responses::apiResponse(
                ['message' => 'username_exists'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
        if ($apiUser) {

            $apiUser->setPlainPassword($password);
            $apiUser->setUsername($username);

            $entityManager->persist($apiUser);
            $entityManager->flush();

            $output = [
                'id' => $apiUser->getId(),
                'username' => $apiUser->getUsername(),
            ];

            return Responses::apiResponse($output);

        }

        return Responses::apiResponse(
            ['message' => 'no_users_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );

    }

    public function getUserStatisticsAction($filter)
    {
        $userId = $this->getUser()->getId();
        $statistics = $this->get('api.repository.user')->getStatistics($userId, $filter);

        return Responses::apiResponse($statistics);
    }

    public function updateUserCoordinatesAction(Request $request)
    {
        $userId = $this->getUser()->getId();
        $lat = $request->get('lat');
        $lng = $request->get('lng');
        $entityManager = $this->getDoctrine()->getManager();

        $user = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(['userId' => $userId]);

        if ($user) {
            $user->setLat($lat);
            $user->setlng($lng);
            $entityManager->persist($user);
            $entityManager->flush();

            return Responses::apiResponse(['user_id' => $userId, 'message' => 'updated']);
        }

        return Responses::apiResponse(
            ['message' => 'no_users_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function findNearYottersAction($distance, Request $request)
    {
        $userId = $this->getUser()->getId();
        $offset = $request->get('o') ? $request->get('o') : '0';
        $limit = $request->get('l') ? $request->get('l') : '10';
        $entityManager = $this->getDoctrine()->getManager();

        $user = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(['userId' => $userId]);

        if ($user) {

            $nearYotters = $this->get('api.repository.user')->getNearYotters(
                $userId,
                $user->getLat(),
                $user->getLng(),
                $distance,
                $limit,
                $offset
            );
            $nearYottersCount = $this->get('api.repository.user')->getNearYottersCount(
                $userId,
                $user->getLat(),
                $user->getLng(),
                $distance
            );

            if ($nearYotters) {
                return Responses::apiResponse(['users' => $nearYotters, 'total_count' => $nearYottersCount]);
            }

        }

        return Responses::apiResponse(
            ['message' => 'no_users_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function lastRatedUsersAction($bookId, Request $request)
    {

        $offset = $request->get('o') ? $request->get('o') : '0';
        $limit = $request->get('l') ? $request->get('l') : '10';
        $entityManager = $this->getDoctrine()->getManager();

        $ratedUsers = $entityManager->getRepository('ApiBundle:Rating')->findBy(
            ['bookId' => $bookId],
            ['rateDate' => 'DESC'],
            $limit,
            $offset
        );
        $ratedUsersCount = $entityManager->getRepository('ApiBundle:Rating')->findBy(
            ['bookId' => $bookId],
            ['rateDate' => 'DESC']
        );

        if ($ratedUsers) {
            $output = [];
            $validator = new Validator();
            foreach ($ratedUsers as $key => $ratedUser) {
                $userData = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(
                    ['userId' => $ratedUser->getUserId()]
                );

                if ($userData) {
                    $output['users'][$key]['user_id'] = $userData->getUserId();
                    $output['users'][$key]['first_name'] = trim($userData->getFirstname());
                    $output['users'][$key]['last_name'] = trim($userData->getLastname());
                    $output['users'][$key]['image'] = $validator->generateCorrectProfilePicture(
                        $userData->getPicture()
                    );
                    $output['users'][$key]['thumbnail'] = $validator->generateCorrectProfilePicture(
                        str_replace('img/profile/', 'img/profile/thumbnail/', $userData->getPicture())
                    );
                }
            }
            $output['users_count'] = count($ratedUsersCount);

            return Responses::apiResponse($output);

        }

        return Responses::apiResponse(
            ['message' => 'no_users_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function bookLikedUsersAction($bookId, Request $request)
    {

        $offset = $request->get('o') ? $request->get('o') : '0';
        $limit = $request->get('l') ? $request->get('l') : '10';
        $entityManager = $this->getDoctrine()->getManager();

        $likedUsersCount = count(
            $entityManager->getRepository('ApiBundle:Likes')->findBy(['bookId' => $bookId, 'likes' => 1])
        );
        $likedUsers = $entityManager->getRepository('ApiBundle:Likes')->findBy(
            ['bookId' => $bookId, 'likes' => 1],
            ['modifiedDate' => 'DESC'],
            $limit,
            $offset
        );

        if ($likedUsers) {
            $output = [];
            $validator = new Validator();
            foreach ($likedUsers as $key => $likedUser) {
                $userData = $entityManager->getRepository('ApiBundle:UserInfo')->findOneBy(
                    ['userId' => $likedUser->getUserId()]
                );

                if ($userData) {
                    $output['users'][$key]['user_id'] = $userData->getUserId();
                    $output['users'][$key]['first_name'] = trim($userData->getFirstname());
                    $output['users'][$key]['last_name'] = trim($userData->getLastname());
                    $output['users'][$key]['image'] = $validator->generateCorrectProfilePicture(
                        $userData->getPicture()
                    );
                    $output['users'][$key]['thumbnail'] = $validator->generateCorrectProfilePicture(
                        str_replace('img/profile/', 'img/profile/thumbnail/', $userData->getPicture())
                    );
                }
            }
            $output['users'] = array_values( $output['users']);
            $output['likes_count'] = $likedUsersCount;

            return Responses::apiResponse($output);

        }

        return Responses::apiResponse(
            ['message' => 'no_users_found'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

    public function bookPresentUsersAction($bookId, Request $request)
    {

        $offset = $request->get('o') ? $request->get('o') : '0';
        $limit = $request->get('l') ? $request->get('l') : '10';
        $userId = $this->getUser()->getId();
        $search = $request->get('text')?$request->get('text'):'';

        $presentUsers = $this->get('api.repository.user')->getPresentUsers($bookId,$userId,$search,$limit,$offset);
        $presentUsersCount = $this->get('api.repository.user')->getPresentUsers($bookId,$userId,$search);

        if ($presentUsers) {
            $output['users'] = $presentUsers;
            $output['user_count'] = count($presentUsersCount);

            return Responses::apiResponse($output);
        }

        return Responses::apiResponse(
            ['message' => 'no_present_users'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }


    /* Get all users whom i didn't lend the book*/
    public function getNotPresentUsersAction($bookId, Request $request)
    {
        $limit = $request->get('l');
        $offset = $request->get('o');
        $search = $request->get('text')?$request->get('text'):'';
        $userId = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $bookExists = $entityManager->getRepository('ApiBundle:Books')->findOneBy(['id' => $bookId]);
        if(!$bookExists){
            return Responses::apiResponse(
                ['message' => 'book_not_found'],
                ErrorMessages::FAILURE_CODE,
                ErrorMessages::FAILURE_MESSAGE
            );
        }
        $lentFriends['users'] = $this->get('api.repository.user')->getUserNotPresentFriends($userId, $bookId,$search, $limit, $offset);
        $lentFriends['user_count'] = $this->get('api.repository.user')->getUserNotPresentFriendsCount($userId, $bookId, $search);

        if ($lentFriends) {
            return Responses::apiResponse($lentFriends);
        }

        return Responses::apiResponse(
            ['message' => 'user_not_exists'],
            ErrorMessages::FAILURE_CODE,
            ErrorMessages::FAILURE_MESSAGE
        );
    }

}

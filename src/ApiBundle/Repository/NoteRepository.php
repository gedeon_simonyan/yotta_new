<?php
/**
 * Created by PhpStorm.
 * User: User1
 * Date: 12.07.2018
 * Time: 11:00
 */

namespace ApiBundle\Repository;


use ApiBundle\Entity\Notification;
use ApiBundle\Services\Validator;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use PDO;

class NoteRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * NoteRepository constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getNote($userId, $offset = 0, $limit = 10)
    {
        $query = $this->em->getConnection()->prepare("
                        SELECT n.`notification`, n.`date`, ui.`firstname`, ui.`lastname`, ui.`picture`,
                         b.`title`, n.`markup_type`, n.`markup_id`, n.`note_status`
                            FROM `notification` AS n
                              INNER JOIN `user_info` AS ui ON n.`user_id` = ui.`user_id`
                              INNER JOIN `books` AS b ON n.`book_id` = b.`id`
                            WHERE n.`friend_id`=:userId and n.`markup_type` not in ('lend','borrow') ORDER BY n.`date` DESC
                            LIMIT :offset, :limit
        ");
        $query->bindValue('userId', $userId);
        $query->bindValue('limit', intval($limit), PDO::PARAM_INT);
        $query->bindValue('offset', intval($offset), PDO::PARAM_INT);
        $query->execute();
        $res = $query->fetchAll();

        $validator = new Validator();
        foreach ($res as $key=>$result){
            $res[$key]['picture'] = $validator->generateCorrectProfilePicture($result['picture']);
        }
        $output['notes'] = $res;
        $output['total_count'] = (int)$this->getNoteCount($userId);

        return $output;
    }

    public function getApproveNote($userId, $offset = 0, $limit = 10)
    {
        $query = $this->em->getConnection()->prepare("
                        SELECT n.`notification`, n.`date`, ui.`firstname`, ui.`lastname`, ui.`picture`,
                         b.`title`, n.`markup_type`, n.`markup_id`, n.`note_status`
                            FROM `notification` AS n
                              INNER JOIN `user_info` AS ui ON n.`user_id` = ui.`user_id`
                              INNER JOIN `books` AS b ON n.`book_id` = b.`id`
                            WHERE n.`friend_id`=:userId and n.`markup_type` in ('lend','borrow') ORDER BY n.`date` DESC
                            LIMIT :offset, :limit
        ");
        $query->bindValue('userId', $userId);
        $query->bindValue('limit', intval($limit), PDO::PARAM_INT);
        $query->bindValue('offset', intval($offset), PDO::PARAM_INT);
        $query->execute();
        $res = $query->fetchAll();

        $validator = new Validator();
        foreach ($res as $key=>$result){
            $res[$key]['picture'] = $validator->generateCorrectProfilePicture($result['picture']);
        }
        $output['notes'] = $res;
        $output['total_count'] = (int)$this->getApproveNoteCount($userId);

        return $output;
    }

    public function getNoteCount($userId)
    {
        $query = $this->em->getConnection()->prepare("
                         SELECT COUNT(*)
                            FROM `notification` AS n
                              INNER JOIN `user_info` AS ui ON n.`friend_id` = ui.`user_id`
                              INNER JOIN `books` AS b ON n.`book_id` = b.`id`
                            WHERE n.`friend_id`=:userId and n.`markup_type` not in ('lend','borrow') ORDER BY n.`date` DESC
        ");

        $query->bindValue('userId', $userId);
        $query->execute();
        $res = $query->fetchColumn();

        return $res;
    }

    public function getApproveNoteCount($userId)
    {
        $query = $this->em->getConnection()->prepare("
                         SELECT COUNT(*)
                            FROM `notification` AS n
                              INNER JOIN `user_info` AS ui ON n.`friend_id` = ui.`user_id`
                              INNER JOIN `books` AS b ON n.`book_id` = b.`id`
                            WHERE n.`friend_id`=:userId and n.`markup_type` in ('lend','borrow') ORDER BY n.`date` DESC
        ");

        $query->bindValue('userId', $userId);
        $query->execute();
        $res = $query->fetchColumn();

        return $res;
    }

    public function getLast20DayNotes(){
        $date = new \DateTime(date("Y-m-d"));
        $date->modify('+20 day');
        $curDate = new \DateTime();
        $notes = $this->em->getRepository('ApiBundle:LentBooks')->findBy(['backDate'=>$date]);
        foreach ($notes as $note){
            $notification = new Notification();
            $notification->setUserId($note->getUserId());
            $notification->setFriendId($note->getLentTo());
            $notification->setBookId($note->getBookId());
            $notification->setMarkupId($note->getId());
            $notification->setMarkupType($note->getType().'_20');
            $notification->setNoteStatus(0);
            $notification->setDate($curDate);
            $notification->setNotification($note->getType().'_book_return_20_days_friend');

            $notificationFriend = new Notification();
            $notificationFriend->setUserId($note->getLentTo());
            $notificationFriend->setFriendId($note->getUserId());
            $notificationFriend->setBookId($note->getBookId());
            $notificationFriend->setMarkupId($note->getId());
            $notificationFriend->setMarkupType($note->getType().'_20');
            $notificationFriend->setNoteStatus(0);
            $notificationFriend->setDate($curDate);
            $notificationFriend->setNotification($note->getType().'_book_return_20_days');
            try{
                $this->em->persist($notification);
                $this->em->persist($notificationFriend);
                $this->em->flush();

            }catch (ORMException $e){
                return false;
            }

        }
        return true;
    }

    public function getLastDayNotes(){
        $date = new \DateTime(date("Y-m-d"));
        $date->modify('+1 day');
        $curDate = new \DateTime();
        $notes = $this->em->getRepository('ApiBundle:LentBooks')->findBy(['backDate'=>$date]);
        foreach ($notes as $note){
            $notification = new Notification();
            $notification->setUserId($note->getUserId());
            $notification->setFriendId($note->getLentTo());
            $notification->setBookId($note->getBookId());
            $notification->setMarkupId($note->getId());
            $notification->setMarkupType($note->getType().'_1');
            $notification->setNoteStatus(0);
            $notification->setDate($curDate);
            $notification->setNotification($note->getType().'_book_return_last_days_friend');

            $notificationFriend = new Notification();
            $notificationFriend->setUserId($note->getLentTo());
            $notificationFriend->setFriendId($note->getUserId());
            $notificationFriend->setBookId($note->getBookId());
            $notificationFriend->setMarkupId($note->getId());
            $notificationFriend->setMarkupType($note->getType().'_1');
            $notificationFriend->setNoteStatus(0);
            $notificationFriend->setDate($curDate);
            $notificationFriend->setNotification($note->getType().'_book_return_last_days');
            try{
                $this->em->persist($notification);
                $this->em->persist($notificationFriend);
                $this->em->flush();

            }catch (ORMException $e){
                return false;
            }

        }
        return true;
    }

}
<?php

namespace ApiBundle\Repository;

use ApiBundle\Services\Validator;
use Doctrine\DBAL\ParameterType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use PDO;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;


/**
 * SearchRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MarkupRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * MarkupRepository constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getMarkupsCount($bookId){

        $query = $this->em->createQueryBuilder()
            ->select("COUNT(m.id) as markupCount","m.type")
            ->from('ApiBundle:Markups','m')
            ->innerJoin('ApiBundle:BookMarkups','mb')
            ->groupBy('m.type')
            ->where('mb.bookId = :bookId and m.id = mb.markupId')
            ->setParameter("bookId", $bookId);

        $result = $query->getQuery()->getArrayResult();

        return $result;
    }

    public function getMarkupTypeCount($bookId, $markupType){

        try{
            $query = $this->em->createQueryBuilder()
                ->select("COUNT(m.id) as markupCount")
                ->from('ApiBundle:Markups','m')
                ->innerJoin('ApiBundle:BookMarkups','mb')
                ->groupBy('m.type')
                ->where('mb.bookId = :bookId and m.id = mb.markupId and m.type = :markupType')
                ->setParameter("bookId", $bookId)
                ->setParameter("markupType", $markupType);

            $result = $query->getQuery()->getOneOrNullResult();

            return $result;
        }catch (NonUniqueResultException $e){
            return false;
        }

    }

    public function getAllMarkupsCount($bookId){
        try{
            $query = $this->em->createQueryBuilder()
                ->select("COUNT(m.id) as markups")
                ->from('ApiBundle:BookMarkups','m')
                ->where('m.bookId = :bookId')
                ->setParameter("bookId", $bookId);

            $result = $query->getQuery()->getSingleResult();

            if($result){
                $result = $result['markups'];
            }
            return $result;
        }catch(\Exception $e){

            return false;
        }
    }

    public function getMarkupById($markupId, $limit, $offset){
        try{
            $query = $this->em->createQueryBuilder()
                ->select("m.bookId")
                ->from('ApiBundle:BookMarkups','m')
                ->where('m.markupId = :markupId')
                ->orderBy('m.addedDate','DESC')
                ->groupBy('m.bookId')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->setParameter("markupId", $markupId);

            $result = $query->getQuery()->getArrayResult();

            return $result;
        }catch(\Exception $e){

            return false;
        }
    }

    public function getMarkupCount($markupId){
        try{
            $query = $this->em->createQueryBuilder()
                ->select("m.bookId")
                ->from('ApiBundle:BookMarkups','m')
                ->where('m.markupId = :markupId')
                ->orderBy('m.addedDate','DESC')
                ->groupBy('m.bookId')
                ->setParameter("markupId", $markupId);

            $result = $query->getQuery()->getArrayResult();

            return $result;
        }catch(\Exception $e){

            return false;
        }
    }

    public function getTopMusics($limit, $offset){

        $query = $this->em->createQueryBuilder()
            ->select("m.id,
            COUNT(m.id) as musicCount,
            m.musicAuthorImg as author_img, 
            m.title, 
            m.musicAuthor as author,
            m.previewUrl as preview_url,
            m.musicTrackUrl as music_url")
            ->from('ApiBundle:Markups','m')
            ->where('m.type=:music AND m.markupId IS NOT NULL')
            ->groupBy('m.markupId')
            ->orderBy('musicCount','DESC')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ->setParameter('music','music');

        $result = $query->getQuery()->getArrayResult();

        return $result;
    }

    public function getMarkupUsers($markupId, $limit, $offset)
    {
        $em = $this->em->getConnection()->prepare(
            "SELECT ui.`user_id` as id,ui.`firstname` as first_name, ui.`lastname` as last_name,ui.`picture` as image
                        FROM `book_markups` AS bm
                        INNER JOIN `user_info` AS ui ON bm.`user_id`=ui.`user_id`
                        WHERE bm.`markup_id`=:markup_id GROUP BY bm.`user_id` LIMIT :offset,:limit"
        );
        $em->bindValue('markup_id', $markupId);
        $em->bindValue('limit', $limit,ParameterType::INTEGER);
        $em->bindValue('offset', $offset,ParameterType::INTEGER);
        $em->execute();
        $results = $em->fetchAll();
        $validator = new Validator();

        foreach ($results as $key=>$result){
            $results[$key]['image'] = $validator->generateCorrectProfilePicture($result['image']);
            $results[$key]['thumbnail'] = $validator->generateCorrectProfilePicture(
                str_replace('img/profile/', 'img/profile/thumbnail/', $result['image']));
        }
        return $results;

    }

    public function getMarkupUsersCount($markupId)
    {
        $em = $this->em->getConnection()->prepare(
            "SELECT ui.`user_id`
                        FROM `book_markups` AS bm
                        INNER JOIN `user_info` AS ui ON bm.`user_id`=ui.`user_id`
                        WHERE bm.`markup_id`=:markup_id GROUP BY ui.`user_id`"
        );
        $em->bindValue('markup_id', $markupId);
        $em->execute();
        $result = $em->fetchAll();

        return count($result);

    }
}
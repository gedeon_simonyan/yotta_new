<?php

namespace ApiBundle\Entity;

/**
 * LentBooks
 */
class LentBooks
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var integer
     */
    private $lentTo;

    /**
     * @var string
     */
    private $bookId;

    /**
     * @var string
     */
    private $type;

    /**
     * @var \DateTime
     */
    private $lendDate;

    /**
     * @var \DateTime
     */
    private $backDate;

    /**
     * @var integer
     */
    private $isApproved;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return LentBooks
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set lentTo
     *
     * @param integer $lentTo
     *
     * @return LentBooks
     */
    public function setLentTo($lentTo)
    {
        $this->lentTo = $lentTo;

        return $this;
    }

    /**
     * Get lentTo
     *
     * @return integer
     */
    public function getLentTo()
    {
        return $this->lentTo;
    }

    /**
     * Set bookId
     *
     * @param string $bookId
     *
     * @return LentBooks
     */
    public function setBookId($bookId)
    {
        $this->bookId = $bookId;

        return $this;
    }

    /**
     * Get bookId
     *
     * @return string
     */
    public function getBookId()
    {
        return $this->bookId;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return LentBooks
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set lendDate
     *
     * @param \DateTime $lendDate
     *
     * @return LentBooks
     */
    public function setLendDate($lendDate)
    {
        $this->lendDate = $lendDate;

        return $this;
    }

    /**
     * Get lendDate
     *
     * @return \DateTime
     */
    public function getLendDate()
    {
        return $this->lendDate;
    }

    /**
     * Set backDate
     *
     * @param \DateTime $backDate
     *
     * @return LentBooks
     */
    public function setBackDate($backDate)
    {
        $this->backDate = $backDate;

        return $this;
    }

    /**
     * Get backDate
     *
     * @return \DateTime
     */
    public function getBackDate()
    {
        return $this->backDate;
    }

    /**
     * Set isApproved
     *
     * @param integer $isApproved
     *
     * @return LentBooks
     */
    public function setIsApproved($isApproved)
    {
        $this->isApproved = $isApproved;

        return $this;
    }

    /**
     * Get isApproved
     *
     * @return integer
     */
    public function getIsApproved()
    {
        return $this->isApproved;
    }
}

<?php

namespace ApiBundle\Entity;

/**
 * Rating
 */
class Rating
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $bookId;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var integer
     */
    private $stars;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookId
     *
     * @param integer $bookId
     *
     * @return Rating
     */
    public function setBookId($bookId)
    {
        $this->bookId = $bookId;

        return $this;
    }

    /**
     * Get bookId
     *
     * @return integer
     */
    public function getBookId()
    {
        return $this->bookId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Rating
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set stars
     *
     * @param integer $stars
     *
     * @return Rating
     */
    public function setStars($stars)
    {
        $this->stars = $stars;

        return $this;
    }

    /**
     * Get stars
     *
     * @return integer
     */
    public function getStars()
    {
        return $this->stars;
    }
    /**
     * @var \DateTime|null
     */
    private $rateDate = 'CURRENT_TIMESTAMP';


    /**
     * Set rateDate.
     *
     * @param \DateTime|null $rateDate
     *
     * @return Rating
     */
    public function setRateDate($rateDate = null)
    {
        $this->rateDate = $rateDate;

        return $this;
    }

    /**
     * Get rateDate.
     *
     * @return \DateTime|null
     */
    public function getRateDate()
    {
        return $this->rateDate;
    }
}

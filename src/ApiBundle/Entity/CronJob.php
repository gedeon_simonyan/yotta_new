<?php

namespace ApiBundle\Entity;

/**
 * CronJob
 */
class CronJob
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $offset;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set offset
     *
     * @param integer $offset
     *
     * @return CronJob
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * Get offset
     *
     * @return integer
     */
    public function getOffset()
    {
        return $this->offset;
    }
}

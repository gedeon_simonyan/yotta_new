<?php

namespace ApiBundle\Entity;

/**
 * UserInfo
 */
class UserInfo
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $userId;

    /**
     * @var string|null
     */
    private $picture;

    /**
     * @var string|null
     */
    private $firstname = '';

    /**
     * @var string|null
     */
    private $lastname = '';

    /**
     * @var int|null
     */
    private $zipCode;

    /**
     * @var int|null
     */
    private $languageId = '1';

    /**
     * @var string|null
     */
    private $forumVisible = 'all';

    /**
     * @var string|null
     */
    private $status = 'online';

    /**
     * @var string|null
     */
    private $profile = 'user';

    /**
     * @var string|null
     */
    private $deviceToken;

    /**
     * @var string|null
     */
    private $deviceType;

    /**
     * @var int|null
     */
    private $messageAllow;

    /**
     * @var int|null
     */
    private $allow;

    /**
     * @var \DateTime|null
     */
    private $lastActivityDate = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     */
    private $lat;

    /**
     * @var string|null
     */
    private $lng;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return UserInfo
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set picture.
     *
     * @param string|null $picture
     *
     * @return UserInfo
     */
    public function setPicture($picture = null)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture.
     *
     * @return string|null
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set firstname.
     *
     * @param string|null $firstname
     *
     * @return UserInfo
     */
    public function setFirstname($firstname = null)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname.
     *
     * @return string|null
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname.
     *
     * @param string|null $lastname
     *
     * @return UserInfo
     */
    public function setLastname($lastname = null)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname.
     *
     * @return string|null
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set zipCode.
     *
     * @param int|null $zipCode
     *
     * @return UserInfo
     */
    public function setZipCode($zipCode = null)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode.
     *
     * @return int|null
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set languageId.
     *
     * @param int|null $languageId
     *
     * @return UserInfo
     */
    public function setLanguageId($languageId = null)
    {
        $this->languageId = $languageId;

        return $this;
    }

    /**
     * Get languageId.
     *
     * @return int|null
     */
    public function getLanguageId()
    {
        return $this->languageId;
    }

    /**
     * Set forumVisible.
     *
     * @param string|null $forumVisible
     *
     * @return UserInfo
     */
    public function setForumVisible($forumVisible = null)
    {
        $this->forumVisible = $forumVisible;

        return $this;
    }

    /**
     * Get forumVisible.
     *
     * @return string|null
     */
    public function getForumVisible()
    {
        return $this->forumVisible;
    }

    /**
     * Set status.
     *
     * @param string|null $status
     *
     * @return UserInfo
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set profile.
     *
     * @param string|null $profile
     *
     * @return UserInfo
     */
    public function setProfile($profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile.
     *
     * @return string|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set deviceToken.
     *
     * @param string|null $deviceToken
     *
     * @return UserInfo
     */
    public function setDeviceToken($deviceToken = null)
    {
        $this->deviceToken = $deviceToken;

        return $this;
    }

    /**
     * Get deviceToken.
     *
     * @return string|null
     */
    public function getDeviceToken()
    {
        return $this->deviceToken;
    }

    /**
     * Set deviceType.
     *
     * @param string|null $deviceType
     *
     * @return UserInfo
     */
    public function setDeviceType($deviceType = null)
    {
        $this->deviceType = $deviceType;

        return $this;
    }

    /**
     * Get deviceType.
     *
     * @return string|null
     */
    public function getDeviceType()
    {
        return $this->deviceType;
    }

    /**
     * Set messageAllow.
     *
     * @param int|null $messageAllow
     *
     * @return UserInfo
     */
    public function setMessageAllow($messageAllow = null)
    {
        $this->messageAllow = $messageAllow;

        return $this;
    }

    /**
     * Get messageAllow.
     *
     * @return int|null
     */
    public function getMessageAllow()
    {
        return $this->messageAllow;
    }

    /**
     * Set allow.
     *
     * @param int|null $allow
     *
     * @return UserInfo
     */
    public function setAllow($allow = null)
    {
        $this->allow = $allow;

        return $this;
    }

    /**
     * Get allow.
     *
     * @return int|null
     */
    public function getAllow()
    {
        return $this->allow;
    }

    /**
     * Set lastActivityDate.
     *
     * @param \DateTime|null $lastActivityDate
     *
     * @return UserInfo
     */
    public function setLastActivityDate($lastActivityDate = null)
    {
        $this->lastActivityDate = $lastActivityDate;

        return $this;
    }

    /**
     * Get lastActivityDate.
     *
     * @return \DateTime|null
     */
    public function getLastActivityDate()
    {
        return $this->lastActivityDate;
    }

    /**
     * Set lat.
     *
     * @param string|null $lat
     *
     * @return UserInfo
     */
    public function setLat($lat = null)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat.
     *
     * @return string|null
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng.
     *
     * @param string|null $lng
     *
     * @return UserInfo
     */
    public function setLng($lng = null)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng.
     *
     * @return string|null
     */
    public function getLng()
    {
        return $this->lng;
    }
}

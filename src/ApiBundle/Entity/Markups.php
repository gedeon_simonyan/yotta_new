<?php

namespace ApiBundle\Entity;

/**
 * Markups
 */
class Markups
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $type = 'music';

    /**
     * @var integer
     */
    private $creatorId;

    /**
     * @var integer
     */
    private $bookId;

    /**
     * @var integer
     */
    private $markupId;

    /**
     * @var integer
     */
    private $page;

    /**
     * @var string
     */
    private $content = '';

    /**
     * @var string
     */
    private $contentType = 'music';

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var \DateTime
     */
    private $modifyDate = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    private $musicAuthor = '';

    /**
     * @var float
     */
    private $musicPrice;

    /**
     * @var string
     */
    private $musicTrackUrl = '';

    /**
     * @var integer
     */
    private $rating;

    /**
     * @var string
     */
    private $musicAuthorImg = '';

    /**
     * @var string
     */
    private $markupDesc;

    /**
     * @var string
     */
    private $musicGenre = '';

    /**
     * @var string
     */
    private $musicType = '0';

    /**
     * @var string
     */
    private $previewUrl;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Markups
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set creatorId
     *
     * @param integer $creatorId
     *
     * @return Markups
     */
    public function setCreatorId($creatorId)
    {
        $this->creatorId = $creatorId;

        return $this;
    }

    /**
     * Get creatorId
     *
     * @return integer
     */
    public function getCreatorId()
    {
        return $this->creatorId;
    }

    /**
     * Set bookId
     *
     * @param integer $bookId
     *
     * @return Markups
     */
    public function setBookId($bookId)
    {
        $this->bookId = $bookId;

        return $this;
    }

    /**
     * Get bookId
     *
     * @return integer
     */
    public function getBookId()
    {
        return $this->bookId;
    }

    /**
     * Set markupId
     *
     * @param integer $markupId
     *
     * @return Markups
     */
    public function setMarkupId($markupId)
    {
        $this->markupId = $markupId;

        return $this;
    }

    /**
     * Get markupId
     *
     * @return integer
     */
    public function getMarkupId()
    {
        return $this->markupId;
    }

    /**
     * Set page
     *
     * @param integer $page
     *
     * @return Markups
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return integer
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Markups
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set contentType
     *
     * @param string $contentType
     *
     * @return Markups
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * Get contentType
     *
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Markups
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set modifyDate
     *
     * @param \DateTime $modifyDate
     *
     * @return Markups
     */
    public function setModifyDate($modifyDate)
    {
        $this->modifyDate = $modifyDate;

        return $this;
    }

    /**
     * Get modifyDate
     *
     * @return \DateTime
     */
    public function getModifyDate()
    {
        return $this->modifyDate;
    }

    /**
     * Set musicAuthor
     *
     * @param string $musicAuthor
     *
     * @return Markups
     */
    public function setMusicAuthor($musicAuthor)
    {
        $this->musicAuthor = $musicAuthor;

        return $this;
    }

    /**
     * Get musicAuthor
     *
     * @return string
     */
    public function getMusicAuthor()
    {
        return $this->musicAuthor;
    }

    /**
     * Set musicPrice
     *
     * @param float $musicPrice
     *
     * @return Markups
     */
    public function setMusicPrice($musicPrice)
    {
        $this->musicPrice = $musicPrice;

        return $this;
    }

    /**
     * Get musicPrice
     *
     * @return float
     */
    public function getMusicPrice()
    {
        return $this->musicPrice;
    }

    /**
     * Set musicTrackUrl
     *
     * @param string $musicTrackUrl
     *
     * @return Markups
     */
    public function setMusicTrackUrl($musicTrackUrl)
    {
        $this->musicTrackUrl = $musicTrackUrl;

        return $this;
    }

    /**
     * Get musicTrackUrl
     *
     * @return string
     */
    public function getMusicTrackUrl()
    {
        return $this->musicTrackUrl;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return Markups
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set musicAuthorImg
     *
     * @param string $musicAuthorImg
     *
     * @return Markups
     */
    public function setMusicAuthorImg($musicAuthorImg)
    {
        $this->musicAuthorImg = $musicAuthorImg;

        return $this;
    }

    /**
     * Get musicAuthorImg
     *
     * @return string
     */
    public function getMusicAuthorImg()
    {
        return $this->musicAuthorImg;
    }

    /**
     * Set markupDesc
     *
     * @param string $markupDesc
     *
     * @return Markups
     */
    public function setMarkupDesc($markupDesc)
    {
        $this->markupDesc = $markupDesc;

        return $this;
    }

    /**
     * Get markupDesc
     *
     * @return string
     */
    public function getMarkupDesc()
    {
        return $this->markupDesc;
    }

    /**
     * Set musicGenre
     *
     * @param string $musicGenre
     *
     * @return Markups
     */
    public function setMusicGenre($musicGenre)
    {
        $this->musicGenre = $musicGenre;

        return $this;
    }

    /**
     * Get musicGenre
     *
     * @return string
     */
    public function getMusicGenre()
    {
        return $this->musicGenre;
    }

    /**
     * Set musicType
     *
     * @param string $musicType
     *
     * @return Markups
     */
    public function setMusicType($musicType)
    {
        $this->musicType = $musicType;

        return $this;
    }

    /**
     * Get musicType
     *
     * @return string
     */
    public function getMusicType()
    {
        return $this->musicType;
    }

    /**
     * Set previewUrl
     *
     * @param string $previewUrl
     *
     * @return Markups
     */
    public function setPreviewUrl($previewUrl)
    {
        $this->previewUrl = $previewUrl;

        return $this;
    }

    /**
     * Get previewUrl
     *
     * @return string
     */
    public function getPreviewUrl()
    {
        return $this->previewUrl;
    }
}

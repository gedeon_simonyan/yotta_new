<?php

namespace ApiBundle\Entity;

/**
 * Books
 */
class Books
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $bookId = '';

    /**
     * @var string|null
     */
    private $title = 'book dummy';

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var string|null
     */
    private $image = '0';

    /**
     * @var string|null
     */
    private $thumbnail = '0';

    /**
     * @var string|null
     */
    private $publisher = '0';

    /**
     * @var \DateTime|null
     */
    private $publishDate;

    /**
     * @var \DateTime
     */
    private $createdDate;

    /**
     * @var \DateTime
     */
    private $modified = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     */
    private $userId = '0';

    /**
     * @var string|null
     */
    private $language = 'english';

    /**
     * @var string|null
     */
    private $price = '0';

    /**
     * @var string|null
     */
    private $bookUrl = '0';

    /**
     * @var int|null
     */
    private $likeCount = '0';
    /**
     * @var int|null
     */
    private $tagCount = '0';
    /**
     * @var int|null
     */
    private $rateCount = '0';

    /**
     * @var int|null
     */
    private $markups = '0';


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookId.
     *
     * @param string $bookId
     *
     * @return Books
     */
    public function setBookId($bookId)
    {
        $this->bookId = $bookId;

        return $this;
    }

    /**
     * Get bookId.
     *
     * @return string
     */
    public function getBookId()
    {
        return $this->bookId;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return Books
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Books
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set image.
     *
     * @param string|null $image
     *
     * @return Books
     */
    public function setImage($image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return string|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set thumbnail.
     *
     * @param string|null $thumbnail
     *
     * @return Books
     */
    public function setThumbnail($thumbnail = null)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail.
     *
     * @return string|null
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set publisher.
     *
     * @param string|null $publisher
     *
     * @return Books
     */
    public function setPublisher($publisher = null)
    {
        $this->publisher = $publisher;

        return $this;
    }

    /**
     * Get publisher.
     *
     * @return string|null
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Set publishDate.
     *
     * @param \DateTime|null $publishDate
     *
     * @return Books
     */
    public function setPublishDate($publishDate = null)
    {
        $this->publishDate = $publishDate;

        return $this;
    }

    /**
     * Get publishDate.
     *
     * @return \DateTime|null
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    /**
     * Set createdDate.
     *
     * @param \DateTime $createdDate
     *
     * @return Books
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate.
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set modified.
     *
     * @param \DateTime $modified
     *
     * @return Books
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified.
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return Books
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set language.
     *
     * @param string|null $language
     *
     * @return Books
     */
    public function setLanguage($language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language.
     *
     * @return string|null
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set price.
     *
     * @param string|null $price
     *
     * @return Books
     */
    public function setPrice($price = null)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return string|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set bookUrl.
     *
     * @param string|null $bookUrl
     *
     * @return Books
     */
    public function setBookUrl($bookUrl = null)
    {
        $this->bookUrl = $bookUrl;

        return $this;
    }

    /**
     * Get bookUrl.
     *
     * @return string|null
     */
    public function getBookUrl()
    {
        return $this->bookUrl;
    }

    /**
     * Set like count.
     *
     * @param int|null $likeCount
     *
     * @return Books
     */
    public function setLikeCount($likeCount = null)
    {
        $this->likeCount = $likeCount;

        return $this;
    }

    /**
     * Get like count.
     *
     * @return int|null
     */
    public function getLikeCount()
    {
        return $this->likeCount;
    }

    /**
     * Set tag count.
     *
     * @param int|null $tagCount
     *
     * @return Books
     */

    public function setTagCount($tagCount = null)
    {
        $this->tagCount = $tagCount;

        return $this;
    }

    /**
     * Get tag count.
     *
     * @return int|null
     */
    public function getTagCount()
    {
        return $this->tagCount;
    }

    /**
     * Set rate count.
     *
     * @param int|null $rateCount
     *
     * @return Books
     */

    public function setRateCount($rateCount = null)
    {
        $this->rateCount = $rateCount;

        return $this;
    }

    /**
     * Get rate count.
     *
     * @return int|null
     */
    public function getRateCount()
    {
        return $this->rateCount;
    }

    /**
     * Set markups.
     *
     * @param int|null $markups
     *
     * @return Books
     */
    public function setMarkups($markups = null)
    {
        $this->markups = $markups;

        return $this;
    }

    /**
     * Get markups.
     *
     * @return int|null
     */
    public function getMarkups()
    {
        return $this->markups;
    }
}

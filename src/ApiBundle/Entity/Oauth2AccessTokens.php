<?php

namespace ApiBundle\Entity;

/**
 * Oauth2AccessTokens
 */
class Oauth2AccessTokens
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $token;

    /**
     * @var integer
     */
    private $expiresAt;

    /**
     * @var string
     */
    private $scope;

    /**
     * @var \ApiBundle\Entity\Oauth2Clients
     */
    private $client;

    /**
     * @var \ApiBundle\Entity\ApiUsers
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Oauth2AccessTokens
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set expiresAt
     *
     * @param integer $expiresAt
     *
     * @return Oauth2AccessTokens
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    /**
     * Get expiresAt
     *
     * @return integer
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * Set scope
     *
     * @param string $scope
     *
     * @return Oauth2AccessTokens
     */
    public function setScope($scope)
    {
        $this->scope = $scope;

        return $this;
    }

    /**
     * Get scope
     *
     * @return string
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * Set client
     *
     * @param \ApiBundle\Entity\Oauth2Clients $client
     *
     * @return Oauth2AccessTokens
     */
    public function setClient(\ApiBundle\Entity\Oauth2Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \ApiBundle\Entity\Oauth2Clients
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set user
     *
     * @param \ApiBundle\Entity\ApiUsers $user
     *
     * @return Oauth2AccessTokens
     */
    public function setUser(\ApiBundle\Entity\ApiUsers $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ApiBundle\Entity\ApiUsers
     */
    public function getUser()
    {
        return $this->user;
    }
}

<?php

namespace ApiBundle\Entity;

/**
 * Authors
 */
class Authors
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $firstname;

    /**
     * @var string|null
     */
    private $lastname;

    /**
     * @var string|null
     */
    private $publicName;

    /**
     * @var int|null
     */
    private $userId;

    /**
     * @var string|null
     */
    private $authorSlug;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname.
     *
     * @param string|null $firstname
     *
     * @return Authors
     */
    public function setFirstname($firstname = null)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname.
     *
     * @return string|null
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname.
     *
     * @param string|null $lastname
     *
     * @return Authors
     */
    public function setLastname($lastname = null)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname.
     *
     * @return string|null
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set publicName.
     *
     * @param string|null $publicName
     *
     * @return Authors
     */
    public function setPublicName($publicName = null)
    {
        $this->publicName = $publicName;

        return $this;
    }

    /**
     * Get publicName.
     *
     * @return string|null
     */
    public function getPublicName()
    {
        return $this->publicName;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return Authors
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set authorSlug.
     *
     * @param string|null $authorSlug
     *
     * @return Authors
     */
    public function setAuthorSlug($authorSlug = null)
    {
        $this->authorSlug = $authorSlug;

        return $this;
    }

    /**
     * Get authorSlug.
     *
     * @return string|null
     */
    public function getAuthorSlug()
    {
        return $this->authorSlug;
    }
}

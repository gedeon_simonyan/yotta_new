<?php


namespace ApiBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table("api_users")
 * @ORM\Entity
 */
class ApiUsers extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(name="facebook_user_id", type="string")
     *
     */
    protected $facebookUserId;
    /**
     * @var string
     *
     * @ORM\Column(name="twitter_user_id", type="string")
     *
     */
    protected $twitterUserId;

    /**
     * @var string
     *
     * @ORM\Column(name="instagram_user_id", type="string")
     *
     */
    protected $instagramUserId;


    /**
     * @var string
     *
     * @ORM\Column(name="pinterest_user_id", type="string")
     *
     */
    protected $pinterestUserId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Get facebookUserId
     *
     * @return string
     */
    public function getFacebookUserId()
    {
        return $this->facebookUserId;
    }
    /**
     * Get getTwitterUserId
     *
     * @return string
     */
    public function getTwitterUserId()
    {
        return $this->twitterUserId;
    }
    /**
     * Get getInstagramUserId
     *
     * @return string
     */
    public function getInstagramUserId()
    {
        return $this->instagramUserId;
    }
    /**
     * Get getPinterestUserId
     *
     * @return string
     */
    public function getPinterestUserId()
    {
        return $this->pinterestUserId;
    }

    public function setFacebookUserId($facebookUserId)
    {
        $this->facebookUserId = $facebookUserId;
    }

    public function setInstagramUserId($instagramUserId)
    {
        $this->instagramUserId = $instagramUserId;
    }

    public function setPinterestUserId($pinterestUserId)
    {
        $this->pinterestUserId = $pinterestUserId;
    }

    public function setTwitterUserId($twitterUserId)
    {
        $this->twitterUserId = $twitterUserId;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}

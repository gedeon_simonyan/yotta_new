<?php

namespace ApiBundle\Entity;

/**
 * CoinsStatistic
 */
class CoinsStatistic
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var integer
     */
    private $coin = '0';

    /**
     * @var integer
     */
    private $coinId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return CoinsStatistic
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set coin
     *
     * @param integer $coin
     *
     * @return CoinsStatistic
     */
    public function setCoin($coin)
    {
        $this->coin = $coin;

        return $this;
    }

    /**
     * Get coin
     *
     * @return integer
     */
    public function getCoin()
    {
        return $this->coin;
    }

    /**
     * Set coinId
     *
     * @param integer $coinId
     *
     * @return CoinsStatistic
     */
    public function setCoinId($coinId)
    {
        $this->coinId = $coinId;

        return $this;
    }

    /**
     * Get coinId
     *
     * @return integer
     */
    public function getCoinId()
    {
        return $this->coinId;
    }
}

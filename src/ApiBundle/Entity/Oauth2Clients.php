<?php

namespace ApiBundle\Entity;

/**
 * Oauth2Clients
 */
class Oauth2Clients
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $randomId;

    /**
     * @var array
     */
    private $redirectUris;

    /**
     * @var string
     */
    private $secret;

    /**
     * @var array
     */
    private $allowedGrantTypes;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set randomId
     *
     * @param string $randomId
     *
     * @return Oauth2Clients
     */
    public function setRandomId($randomId)
    {
        $this->randomId = $randomId;

        return $this;
    }

    /**
     * Get randomId
     *
     * @return string
     */
    public function getRandomId()
    {
        return $this->randomId;
    }

    /**
     * Set redirectUris
     *
     * @param array $redirectUris
     *
     * @return Oauth2Clients
     */
    public function setRedirectUris($redirectUris)
    {
        $this->redirectUris = $redirectUris;

        return $this;
    }

    /**
     * Get redirectUris
     *
     * @return array
     */
    public function getRedirectUris()
    {
        return $this->redirectUris;
    }

    /**
     * Set secret
     *
     * @param string $secret
     *
     * @return Oauth2Clients
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * Get secret
     *
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * Set allowedGrantTypes
     *
     * @param array $allowedGrantTypes
     *
     * @return Oauth2Clients
     */
    public function setAllowedGrantTypes($allowedGrantTypes)
    {
        $this->allowedGrantTypes = $allowedGrantTypes;

        return $this;
    }

    /**
     * Get allowedGrantTypes
     *
     * @return array
     */
    public function getAllowedGrantTypes()
    {
        return $this->allowedGrantTypes;
    }
}

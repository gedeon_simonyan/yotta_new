<?php

namespace ApiBundle\Entity;

/**
 * MessageUsers
 */
class MessageUsers
{
    /**
     * @var integer
     */
    private $userId;

    /**
     * @var integer
     */
    private $friendId;

    /**
     * @var string
     */
    private $lastMessage = '';

    /**
     * @var integer
     */
    private $unreadMessagesCount = '0';

    /**
     * @var \DateTime
     */
    private $modifiedDate = 'CURRENT_TIMESTAMP';


    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return MessageUsers
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set friendId
     *
     * @param integer $friendId
     *
     * @return MessageUsers
     */
    public function setFriendId($friendId)
    {
        $this->friendId = $friendId;

        return $this;
    }

    /**
     * Get friendId
     *
     * @return integer
     */
    public function getFriendId()
    {
        return $this->friendId;
    }

    /**
     * Set lastMessage
     *
     * @param string $lastMessage
     *
     * @return MessageUsers
     */
    public function setLastMessage($lastMessage)
    {
        $this->lastMessage = $lastMessage;

        return $this;
    }

    /**
     * Get lastMessage
     *
     * @return string
     */
    public function getLastMessage()
    {
        return $this->lastMessage;
    }

    /**
     * Set unreadMessagesCount
     *
     * @param integer $unreadMessagesCount
     *
     * @return MessageUsers
     */
    public function setUnreadMessagesCount($unreadMessagesCount)
    {
        $this->unreadMessagesCount = $unreadMessagesCount;

        return $this;
    }

    /**
     * Get unreadMessagesCount
     *
     * @return integer
     */
    public function getUnreadMessagesCount()
    {
        return $this->unreadMessagesCount;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     *
     * @return MessageUsers
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }
}

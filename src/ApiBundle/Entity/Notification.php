<?php

namespace ApiBundle\Entity;

/**
 * Notification
 */
class Notification
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $bookId;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var integer
     */
    private $markupId;

    /**
     * @var string
     */
    private $notification;

    /**
     * @var \DateTime
     */
    private $date = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    private $markupType;

    /**
     * @var string
     */
    private $noteStatus = '0';

    /**
     * @var integer
     */
    private $friendId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookId
     *
     * @param integer $bookId
     *
     * @return Notification
     */
    public function setBookId($bookId)
    {
        $this->bookId = $bookId;

        return $this;
    }

    /**
     * Get bookId
     *
     * @return integer
     */
    public function getBookId()
    {
        return $this->bookId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Notification
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set markupId
     *
     * @param integer $markupId
     *
     * @return Notification
     */
    public function setMarkupId($markupId)
    {
        $this->markupId = $markupId;

        return $this;
    }

    /**
     * Get markupId
     *
     * @return integer
     */
    public function getMarkupId()
    {
        return $this->markupId;
    }

    /**
     * Set notification
     *
     * @param string $notification
     *
     * @return Notification
     */
    public function setNotification($notification)
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * Get notification
     *
     * @return string
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Notification
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set markupType
     *
     * @param string $markupType
     *
     * @return Notification
     */
    public function setMarkupType($markupType)
    {
        $this->markupType = $markupType;

        return $this;
    }

    /**
     * Get markupType
     *
     * @return string
     */
    public function getMarkupType()
    {
        return $this->markupType;
    }

    /**
     * Set noteStatus
     *
     * @param string $noteStatus
     *
     * @return Notification
     */
    public function setNoteStatus($noteStatus)
    {
        $this->noteStatus = $noteStatus;

        return $this;
    }

    /**
     * Get noteStatus
     *
     * @return string
     */
    public function getNoteStatus()
    {
        return $this->noteStatus;
    }

    /**
     * Set friendId
     *
     * @param integer $friendId
     *
     * @return Notification
     */
    public function setFriendId($friendId)
    {
        $this->friendId = $friendId;

        return $this;
    }

    /**
     * Get friendId
     *
     * @return integer
     */
    public function getFriendId()
    {
        return $this->friendId;
    }
}

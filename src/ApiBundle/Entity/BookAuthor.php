<?php

namespace ApiBundle\Entity;

/**
 * BookAuthor
 */
class BookAuthor
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $bookId;

    /**
     * @var int|null
     */
    private $authorId;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookId.
     *
     * @param int|null $bookId
     *
     * @return BookAuthor
     */
    public function setBookId($bookId = null)
    {
        $this->bookId = $bookId;

        return $this;
    }

    /**
     * Get bookId.
     *
     * @return int|null
     */
    public function getBookId()
    {
        return $this->bookId;
    }

    /**
     * Set authorId.
     *
     * @param int|null $authorId
     *
     * @return BookAuthor
     */
    public function setAuthorId($authorId = null)
    {
        $this->authorId = $authorId;

        return $this;
    }

    /**
     * Get authorId.
     *
     * @return int|null
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }
}

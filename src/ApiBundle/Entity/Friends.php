<?php

namespace ApiBundle\Entity;

/**
 * Friends
 */
class Friends
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $friendId;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var string
     */
    private $status = 'waiting';

    /**
     * @var \DateTime
     */
    private $date = 'CURRENT_TIMESTAMP';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set friendId
     *
     * @param integer $friendId
     *
     * @return Friends
     */
    public function setFriendId($friendId)
    {
        $this->friendId = $friendId;

        return $this;
    }

    /**
     * Get friendId
     *
     * @return integer
     */
    public function getFriendId()
    {
        return $this->friendId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Friends
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Friends
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Friends
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
    /**
     * @var string|null
     */
    private $seenStatus = 'unseen';


    /**
     * Set seenStatus.
     *
     * @param string|null $seenStatus
     *
     * @return Friends
     */
    public function setSeenStatus($seenStatus = null)
    {
        $this->seenStatus = $seenStatus;

        return $this;
    }

    /**
     * Get seenStatus.
     *
     * @return string|null
     */
    public function getSeenStatus()
    {
        return $this->seenStatus;
    }
}

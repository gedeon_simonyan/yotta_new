<?php

namespace ApiBundle\Entity;

/**
 * Messages
 */
class Messages
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var integer
     */
    private $friendId;

    /**
     * @var string
     */
    private $message;

    /**
     * @var \DateTime
     */
    private $chatDate = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    private $status = 'unread';

    /**
     * @var string
     */
    private $type = 'text';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Messages
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set friendId
     *
     * @param integer $friendId
     *
     * @return Messages
     */
    public function setFriendId($friendId)
    {
        $this->friendId = $friendId;

        return $this;
    }

    /**
     * Get friendId
     *
     * @return integer
     */
    public function getFriendId()
    {
        return $this->friendId;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Messages
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set chatDate
     *
     * @param \DateTime $chatDate
     *
     * @return Messages
     */
    public function setChatDate($chatDate)
    {
        $this->chatDate = $chatDate;

        return $this;
    }

    /**
     * Get chatDate
     *
     * @return \DateTime
     */
    public function getChatDate()
    {
        return $this->chatDate;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Messages
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Messages
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}

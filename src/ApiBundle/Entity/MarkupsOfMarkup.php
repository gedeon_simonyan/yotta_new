<?php

namespace ApiBundle\Entity;

/**
 * MarkupsOfMarkup
 */
class MarkupsOfMarkup
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $markupId;

    /**
     * @var string
     */
    private $markupType = 'text';

    /**
     * @var string
     */
    private $comment;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var \DateTime
     */
    private $createDate = 'CURRENT_TIMESTAMP';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set markupId
     *
     * @param integer $markupId
     *
     * @return MarkupsOfMarkup
     */
    public function setMarkupId($markupId)
    {
        $this->markupId = $markupId;

        return $this;
    }

    /**
     * Get markupId
     *
     * @return integer
     */
    public function getMarkupId()
    {
        return $this->markupId;
    }

    /**
     * Set markupType
     *
     * @param string $markupType
     *
     * @return MarkupsOfMarkup
     */
    public function setMarkupType($markupType)
    {
        $this->markupType = $markupType;

        return $this;
    }

    /**
     * Get markupType
     *
     * @return string
     */
    public function getMarkupType()
    {
        return $this->markupType;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return MarkupsOfMarkup
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return MarkupsOfMarkup
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return MarkupsOfMarkup
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }
}

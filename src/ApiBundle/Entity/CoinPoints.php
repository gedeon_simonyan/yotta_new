<?php

namespace ApiBundle\Entity;

/**
 * CoinPoints
 */
class CoinPoints
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $purposeDipslay = '';

    /**
     * @var integer
     */
    private $coin;

    /**
     * @var string
     */
    private $purpose = '';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set purposeDipslay
     *
     * @param string $purposeDipslay
     *
     * @return CoinPoints
     */
    public function setPurposeDipslay($purposeDipslay)
    {
        $this->purposeDipslay = $purposeDipslay;

        return $this;
    }

    /**
     * Get purposeDipslay
     *
     * @return string
     */
    public function getPurposeDipslay()
    {
        return $this->purposeDipslay;
    }

    /**
     * Set coin
     *
     * @param integer $coin
     *
     * @return CoinPoints
     */
    public function setCoin($coin)
    {
        $this->coin = $coin;

        return $this;
    }

    /**
     * Get coin
     *
     * @return integer
     */
    public function getCoin()
    {
        return $this->coin;
    }

    /**
     * Set purpose
     *
     * @param string $purpose
     *
     * @return CoinPoints
     */
    public function setPurpose($purpose)
    {
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * Get purpose
     *
     * @return string
     */
    public function getPurpose()
    {
        return $this->purpose;
    }
}

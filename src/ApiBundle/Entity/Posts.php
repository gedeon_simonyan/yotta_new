<?php

namespace ApiBundle\Entity;

/**
 * Posts
 */
class Posts
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $bookId;

    /**
     * @var string
     */
    private $note;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $type;

    /**
     * @var integer
     */
    private $markupId;

    /**
     * @var string
     */
    private $markupType;

    /**
     * @var integer
     */
    private $userFriendId;

    /**
     * @var \ApiBundle\Entity\ApiUsers
     */
    private $friend;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookId
     *
     * @param integer $bookId
     *
     * @return Posts
     */
    public function setBookId($bookId)
    {
        $this->bookId = $bookId;

        return $this;
    }

    /**
     * Get bookId
     *
     * @return integer
     */
    public function getBookId()
    {
        return $this->bookId;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Posts
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Posts
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Posts
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set markupId
     *
     * @param integer $markupId
     *
     * @return Posts
     */
    public function setMarkupId($markupId)
    {
        $this->markupId = $markupId;

        return $this;
    }

    /**
     * Get markupId
     *
     * @return integer
     */
    public function getMarkupId()
    {
        return $this->markupId;
    }

    /**
     * Set markupType
     *
     * @param string $markupType
     *
     * @return Posts
     */
    public function setMarkupType($markupType)
    {
        $this->markupType = $markupType;

        return $this;
    }

    /**
     * Get markupType
     *
     * @return string
     */
    public function getMarkupType()
    {
        return $this->markupType;
    }

    /**
     * Set userFriendId
     *
     * @param integer $userFriendId
     *
     * @return Posts
     */
    public function setUserFriendId($userFriendId)
    {
        $this->userFriendId = $userFriendId;

        return $this;
    }

    /**
     * Get userFriendId
     *
     * @return integer
     */
    public function getUserFriendId()
    {
        return $this->userFriendId;
    }

    /**
     * Set friend
     *
     * @param \ApiBundle\Entity\ApiUsers $friend
     *
     * @return Posts
     */
    public function setFriend(\ApiBundle\Entity\ApiUsers $friend = null)
    {
        $this->friend = $friend;

        return $this;
    }

    /**
     * Get friend
     *
     * @return \ApiBundle\Entity\ApiUsers
     */
    public function getFriend()
    {
        return $this->friend;
    }
    /**
     * @var integer
     */
    private $friendId;


    /**
     * Set friendId
     *
     * @param integer $friendId
     *
     * @return Posts
     */
    public function setFriendId($friendId)
    {
        $this->friendId = $friendId;

        return $this;
    }

    /**
     * Get friendId
     *
     * @return integer
     */
    public function getFriendId()
    {
        return $this->friendId;
    }
}

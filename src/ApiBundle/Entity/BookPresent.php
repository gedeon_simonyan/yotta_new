<?php

namespace ApiBundle\Entity;

/**
 * BookPresent
 */
class BookPresent
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $bookId;

    /**
     * @var int|null
     */
    private $userId;

    /**
     * @var int|null
     */
    private $friendId;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookId.
     *
     * @param int|null $bookId
     *
     * @return BookPresent
     */
    public function setBookId($bookId = null)
    {
        $this->bookId = $bookId;

        return $this;
    }

    /**
     * Get bookId.
     *
     * @return int|null
     */
    public function getBookId()
    {
        return $this->bookId;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return BookPresent
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set friendId.
     *
     * @param int|null $friendId
     *
     * @return BookPresent
     */
    public function setFriendId($friendId = null)
    {
        $this->friendId = $friendId;

        return $this;
    }

    /**
     * Get friendId.
     *
     * @return int|null
     */
    public function getFriendId()
    {
        return $this->friendId;
    }
}

<?php

namespace ApiBundle\Entity;

/**
 * Publishers
 */
class Publishers
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $pName;

    /**
     * @var string
     */
    private $companyName;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $region;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $zipcode;

    /**
     * @var string
     */
    private $contactPerson;

    /**
     * @var string
     */
    private $getNewsletter = '1';

    /**
     * @var string
     */
    private $pAuthorRights = '1';

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $photo;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pName
     *
     * @param string $pName
     *
     * @return Publishers
     */
    public function setPName($pName)
    {
        $this->pName = $pName;

        return $this;
    }

    /**
     * Get pName
     *
     * @return string
     */
    public function getPName()
    {
        return $this->pName;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return Publishers
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Publishers
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Publishers
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return Publishers
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Publishers
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     *
     * @return Publishers
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set contactPerson
     *
     * @param string $contactPerson
     *
     * @return Publishers
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Set getNewsletter
     *
     * @param string $getNewsletter
     *
     * @return Publishers
     */
    public function setGetNewsletter($getNewsletter)
    {
        $this->getNewsletter = $getNewsletter;

        return $this;
    }

    /**
     * Get getNewsletter
     *
     * @return string
     */
    public function getGetNewsletter()
    {
        return $this->getNewsletter;
    }

    /**
     * Set pAuthorRights
     *
     * @param string $pAuthorRights
     *
     * @return Publishers
     */
    public function setPAuthorRights($pAuthorRights)
    {
        $this->pAuthorRights = $pAuthorRights;

        return $this;
    }

    /**
     * Get pAuthorRights
     *
     * @return string
     */
    public function getPAuthorRights()
    {
        return $this->pAuthorRights;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Publishers
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Publishers
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return Publishers
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }
}

<?php

namespace ApiBundle\Entity;

/**
 * Likes
 */
class Likes
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $likes;

    /**
     * @var integer
     */
    private $markupId;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var integer
     */
    private $bookId;

    /**
     * @var \DateTime
     */
    private $modifiedDate = 'CURRENT_TIMESTAMP';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set like
     *
     * @param boolean $like
     *
     * @return Likes
     */
    public function setLike($likes)
    {
        $this->likes = $likes;

        return $this;
    }

    /**
     * Get likes
     *
     * @return boolean
     */
    public function getLike()
    {
        return $this->likes;
    }

    /**
     * Set markupId
     *
     * @param integer $markupId
     *
     * @return Likes
     */
    public function setMarkupId($markupId)
    {
        $this->markupId = $markupId;

        return $this;
    }

    /**
     * Get markupId
     *
     * @return integer
     */
    public function getMarkupId()
    {
        return $this->markupId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Likes
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set bookId
     *
     * @param integer $bookId
     *
     * @return Likes
     */
    public function setBookId($bookId)
    {
        $this->bookId = $bookId;

        return $this;
    }

    /**
     * Get bookId
     *
     * @return integer
     */
    public function getBookId()
    {
        return $this->bookId;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     *
     * @return Likes
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set likes
     *
     * @param boolean $likes
     *
     * @return Likes
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;

        return $this;
    }

    /**
     * Get likes
     *
     * @return boolean
     */
    public function getLikes()
    {
        return $this->likes;
    }
    /**
     * @var bool
     */
    private $like;


}

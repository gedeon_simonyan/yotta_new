<?php

namespace ApiBundle\Entity;

/**
 * Languages
 */
class Languages
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $language = '';

    /**
     * @var string
     */
    private $image = '';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return Languages
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Languages
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
}

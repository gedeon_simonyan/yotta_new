<?php

namespace ApiBundle\Entity;

/**
 * BookTags
 */
class BookTags
{
    /**
     * @var integer
     */
    private $tagId;

    /**
     * @var integer
     */
    private $bookId;

    /**
     * @var integer
     */
    private $userId;


    /**
     * Set tagId
     *
     * @param integer $tagId
     *
     * @return BookTags
     */
    public function setTagId($tagId)
    {
        $this->tagId = $tagId;

        return $this;
    }

    /**
     * Get tagId
     *
     * @return integer
     */
    public function getTagId()
    {
        return $this->tagId;
    }

    /**
     * Set bookId
     *
     * @param integer $bookId
     *
     * @return BookTags
     */
    public function setBookId($bookId)
    {
        $this->bookId = $bookId;

        return $this;
    }

    /**
     * Get bookId
     *
     * @return integer
     */
    public function getBookId()
    {
        return $this->bookId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return BookTags
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }
}

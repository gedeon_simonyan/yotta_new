<?php
/**
 * Created by PhpStorm.
 * User: Gedeon
 * Date: 26-Jun-18
 * Time: 17:48
 */

namespace ApiBundle\Services;

use AmazonProductAPI;
use Symfony\Component\ClassLoader\MapClassLoader;

class AmazonSearch
{

    public function searchInAmazon($searchText, $lang, $startPage,$isAuthor)
    {

        $amazonAPI = new AmazonProductAPI();
        if($isAuthor){
            $result = $amazonAPI->getItemByAuthor(rawurlencode($searchText), 1);
        }
        elseif (is_numeric($searchText)) {
            $result = $amazonAPI->getItemByAsin(rawurlencode($searchText), 1);
        } else {
            $word = str_replace(" ", "+", $searchText);
            $result = $amazonAPI->searchProducts(
                $word,
                AmazonProductAPI::BOOK,
                "Keywords",
                $startPage,
                $lang
            );
        }

        if ($result) {
            $arr = $this->objectToArrayGenerator($result);

            $res = array();
            foreach ($arr["Items"]["Item"] as $k => $v) {
                if (is_object($v)) {
                    $res[] = $this->objectToArrayGenerator($v);
                } else {
                    $res[] = $arr["Items"]["Item"];
                    break;
                }
            }

            $booksInfo = $this->booksArrayGenerator($res);

        } else {

            return false;
        }
        foreach ($booksInfo as $key => $value) {
            $booksInfo[$key]['language'] = $this->languageFilter($value['language']);
        }
        $totalPages = $arr["Items"]["TotalPages"];
        $total = $arr["Items"]["TotalResults"];

        return array("book_info" => $booksInfo, "totalCount" => $total, "totalPages" => $totalPages);
    }

    private function objectToArrayGenerator($obj, $out = array())
    {
        foreach ((array)$obj as $index => $node) {
            $out[$index] = (is_object($node)) ? $this->objectToArrayGenerator($node) : $node;
        }

        return $out;
    }

    private function booksArrayGenerator($array)
    {
        $x = 0;
        $res = array();

        foreach ($array as $key => $val) {
            if (isset($val["ItemAttributes"]["ISBN"])) {
                $bookId = $val["ItemAttributes"]["ISBN"];
            } elseif (isset($val["ItemAttributes"]["EISBN"])) {
                $bookId = $val["ItemAttributes"]["EISBN"];
            } else {
                $bookId = $val["ItemAttributes"]["EAN"];
            }
            $res[$x]['id'] = '';
            $res[$x]['book_id'] = $bookId;
            $res[$x]['book_url'] = $val ["DetailPageURL"];
            $res[$x]['image'] = $val ["LargeImage"]["URL"];
            $res[$x]['title'] = $val["ItemAttributes"]["Title"];
            $res[$x]['description'] = strip_tags($val["EditorialReviews"]["EditorialReview"]["Content"]);
            $res[$x]['publisher'] = $val["ItemAttributes"]["Publisher"];
            $res[$x]['publish_date'] = $val["ItemAttributes"]["PublicationDate"];
            $res[$x]['authors'] = is_array($val['ItemAttributes']["Author"]) ? implode(
                ',',
                $val['ItemAttributes']["Author"]
            ) : $val['ItemAttributes']["Author"];
            $language = $val["ItemAttributes"]["Languages"]["Language"];

            if (count($language) > 2) {

                $language = $this->objectToArrayGenerator($language);
                $language = $language[0]['Name'];


            } else {
                $language = $language['Name'];
            }

            $res[$x]['language'] = $language?$language:"";
            $res[$x]['created_date'] = '';
            $res[$x]['modified'] = '';
            $res[$x]['user_id'] = '';
            $res[$x]['likes'] = '';
            $res[$x]['markups'] = '';
            $res[$x]['tag'] = '';
            $res[$x]['avg_rate'] = '';
            $res[$x]['my_rate'] = '';
            $res[$x]['price'] = $val["ItemAttributes"]["ListPrice"]["FormattedPrice"];

            $x++;
        }

        return $res;
    }

    public function languageFilter($value)
    {
        switch ($value) {
            case "en":
                $value = "english";
                break;
            case "fr":
                $value = "french";
                break;
            case "sp":
                $value = "spanish";
                break;
            case "no":
                $value = "norwegian";
                break;
            case "it":
                $value = "italian";
                break;
            case "ge":
                $value = "german";
                break;
            case "du":
                $value = "dutch";
                break;
            case "ja":
                $value = "japanese";
                break;
            case "ar":
                $value = "armenian";
                break;
            case "hy":
                $value = "armenian";
                break;


        }

        return $value;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Gedeon
 * Date: 13-Nov-18
 * Time: 10:51
 */

namespace ApiBundle\Services;


class ImageUploadService
{
    private $imageUploadDir;
    private $thumbnailUploadDir;
    private $thumbnailDefaults;
    private $validExtensions;

    /**
     * @param $root
     * @param $imageUpload
     */
    public function __construct($imageUpload)
    {
        $this->imageUploadDir = $imageUpload['image_upload_dir'];
        $this->thumbnailUploadDir = $imageUpload['thumbnail_upload_dir'];
        $this->bookThumbnailUploadDir = $imageUpload['book_thumbnail_upload_dir'];
        $this->thumbnailDefaults = $imageUpload['thumbnail_defaults'];
        $this->validExtensions = $imageUpload['valid_extensions'];
    }

    /**
     * Image gets uploaded.
     *
     * @param $image
     * @param array $thumbnail
     * @return array|string
     */
    public function upload($image, array $thumbnail = null, $fileName = null, $type='user')
    {

        if($type == 'book'){

            $imageFile = $image;
            $info = getimagesize($image);
            $extension = trim(image_type_to_extension($info[2]), '.');
            switch ($info[2]) {
                case IMAGETYPE_GIF:
                    $image = imagecreatefromgif($image);
                    break;
                case IMAGETYPE_JPEG:
                    $image = imagecreatefromjpeg($image);
                    break;
                case IMAGETYPE_PNG:
                    $image = imagecreatefrompng($image);
                    break;
            }

            if (! in_array($extension, $this->validExtensions)) {
                return 'Invalid image format.';
            }
            $this->thumbnailDefaults['height'] = 300;
            $this->thumbnailDefaults['width'] = 300;
            $this->thumbnailUploadDir =
            $newName = $fileName . '.' . $extension;
            $imageThumbnailFile = $this->bookThumbnailUploadDir . $newName;

            if (! is_null($thumbnail) && count($thumbnail) == 2) {
                if ($thumbnail['thumbnail'] === true) {
                    if ($thumbnail['padding'] === false) {
                        $this->createThumbnailWithoutPadding(
                            $imageFile,
                            $imageThumbnailFile
                        );
                    }
                }
            }
        }else{

            $originalName = strip_tags($image->getClientOriginalName());
            $extension = $image->guessExtension();
            if (! in_array($extension, $this->validExtensions)) {
                return 'Invalid image format.';
            }
            $newName = sha1(crc32(time()) . microtime() . $originalName) . '.' . $extension;
            $image->move($this->imageUploadDir, $newName);

            if (! file_exists($this->imageUploadDir . $newName)) {
                return 'Image could not be uploaded.';
            }
            $imageFile = $this->imageUploadDir . $newName;
            $imageThumbnailFile = $this->thumbnailUploadDir . $newName;

            if (! is_null($thumbnail) && count($thumbnail) == 2) {
                if ($thumbnail['thumbnail'] === true) {

                    $this->createUserThumbnailWithoutPadding(
                        $imageFile,
                        $imageThumbnailFile
                    );

                }
            }
        }



        return array('newName' => $newName);
    }


    /**
     * Resizes image without adding padding to short edge.
     * Transparency of image is preserved.
     *
     * @param $sourceImage
     * @param $targetImage
     * @return bool
     */
    private function createUserThumbnailWithoutPadding($sourceImage, $targetImage)
    {
        list($sourceWidth, $sourceHeight, $sourceType) = getimagesize($sourceImage);

        switch ($sourceType) {
            case IMAGETYPE_GIF:
                $sourceGdImage = imagecreatefromgif($sourceImage);
                break;
            case IMAGETYPE_JPEG:
                $sourceGdImage = imagecreatefromjpeg($sourceImage);
                break;
            case IMAGETYPE_PNG:
                $sourceGdImage = imagecreatefrompng($sourceImage);
                break;
        }

        if ($sourceGdImage === false) {
            return false;
        }

        $sourceAspectRatio = ($sourceWidth / $sourceHeight);
        $thumbnailAspectRatio = ($this->thumbnailDefaults['width'] / $this->thumbnailDefaults['height']);

        if ($sourceWidth <= $this->thumbnailDefaults['width'] && $sourceHeight <= $this->thumbnailDefaults['height']) {
            $thumbnailWidth = $sourceWidth;
            $thumbnailHeight = $sourceHeight;
        } elseif ($thumbnailAspectRatio > $sourceAspectRatio) {
            $thumbnailWidth = (int) ($this->thumbnailDefaults['height'] * $sourceAspectRatio);
            $thumbnailHeight= $this->thumbnailDefaults['height'];
        } else {
            $thumbnailHeight = $this->thumbnailDefaults['width'];
            $thumbnailWidth = (int) ($this->thumbnailDefaults['width'] * $sourceAspectRatio);
        }

        $thumbnailGdImage = imagecreatetruecolor($thumbnailWidth, $thumbnailHeight);

        //Keep the transparency
        imagecolortransparent($thumbnailGdImage, imagecolorallocatealpha($thumbnailGdImage, 0, 0, 0, 127));
        imagealphablending($thumbnailGdImage, false);
        imagesavealpha($thumbnailGdImage, true);

        imagecopyresampled(
            $thumbnailGdImage,
            $sourceGdImage,
            0,
            0,
            0,
            0,
            $thumbnailWidth,
            $thumbnailHeight,
            $sourceWidth,
            $sourceHeight
        );

        //clearstatcache();

        switch ($sourceType) {
            case IMAGETYPE_GIF:
                imagegif($thumbnailGdImage, $targetImage);
                break;
            case IMAGETYPE_JPEG:
                imagejpeg($thumbnailGdImage, $targetImage, 90);
                break;
            case IMAGETYPE_PNG:
                imagepng($thumbnailGdImage, $targetImage, 9);
                break;
        }

        imagedestroy($sourceGdImage);
        imagedestroy($thumbnailGdImage);

        return true;
    }

    /**
     * Resizes image without adding padding to short edge.
     * Transparency of image is preserved.
     *
     * @param $sourceImage
     * @param $targetImage
     * @return bool
     */
    private function createThumbnailWithoutPadding($sourceImage, $targetImage)
    {
        list($sourceWidth, $sourceHeight, $sourceType) = getimagesize($sourceImage);

        switch ($sourceType) {
            case IMAGETYPE_GIF:
                $sourceGdImage = imagecreatefromgif($sourceImage);
                break;
            case IMAGETYPE_JPEG:
                $sourceGdImage = imagecreatefromjpeg($sourceImage);
                break;
            case IMAGETYPE_PNG:
                $sourceGdImage = imagecreatefrompng($sourceImage);
                break;
        }

        if ($sourceGdImage === false) {
            return false;
        }

        $sourceAspectRatio = ($sourceWidth / $sourceHeight);
        $thumbnailAspectRatio = ($this->thumbnailDefaults['width'] / $this->thumbnailDefaults['height']);

        if ($sourceWidth <= $this->thumbnailDefaults['width'] && $sourceHeight <= $this->thumbnailDefaults['height']) {
            $thumbnailWidth = $sourceWidth;
            $thumbnailHeight = $sourceHeight;
        } elseif ($thumbnailAspectRatio > $sourceAspectRatio) {
            $thumbnailWidth = (int) ($this->thumbnailDefaults['height'] * $sourceAspectRatio);
            $thumbnailHeight = $this->thumbnailDefaults['height'];
        } else {
            $thumbnailHeight = $this->thumbnailDefaults['width'];
            $thumbnailWidth = (int) ($this->thumbnailDefaults['width'] / $sourceAspectRatio);
        }

        $thumbnailGdImage = imagecreatetruecolor($thumbnailWidth, $thumbnailHeight);

        //Keep the transparency
        imagecolortransparent($thumbnailGdImage, imagecolorallocatealpha($thumbnailGdImage, 0, 0, 0, 127));
        imagealphablending($thumbnailGdImage, false);
        imagesavealpha($thumbnailGdImage, true);

        imagecopyresampled(
            $thumbnailGdImage,
            $sourceGdImage,
            0,
            0,
            0,
            0,
            $thumbnailWidth,
            $thumbnailHeight,
            $sourceWidth,
            $sourceHeight
        );

        //clearstatcache();

        switch ($sourceType) {
            case IMAGETYPE_GIF:
                imagegif($thumbnailGdImage, $targetImage);
                break;
            case IMAGETYPE_JPEG:
                imagejpeg($thumbnailGdImage, $targetImage, 90);
                break;
            case IMAGETYPE_PNG:
                imagepng($thumbnailGdImage, $targetImage, 9);
                break;
        }

        imagedestroy($sourceGdImage);
        imagedestroy($thumbnailGdImage);

        return true;
    }
}
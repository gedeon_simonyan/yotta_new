<?php
/**
 * Created by PhpStorm.
 * User: Hovo
 * Date: 5/16/2018
 * Time: 2:37 PM
 */

namespace ApiBundle\Services;

use Symfony\Component\HttpFoundation\Request;

class Validator
{

    public function validatePassword($password)
    {
        $password = htmlspecialchars($password);
        if (preg_match("/^.*(?=.{6,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$/", $password) === 0) {

            return false;
        }

        return true;
    }

    public function validateUsername($username)
    {

        $username = htmlspecialchars($username);
        if (preg_match("/^[0-9a-zA-Z_\.]{6,}$/", $username) === 0) {
            return false;
        }

        return true;
    }

    public function validateName($name)
    {
        $name = htmlspecialchars($name);
        if (preg_match("/^([a-zA-Z][\-\s]?){2,50}$/", $name) === 0) {

            return false;
        }

        return true;
    }
    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        // trim
        $text = trim($text, '-');
        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);
        // lowercase
        $text = strtolower($text);
        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    public function generateCorrectProfilePicture($user_img)
    {
        $request = Request::createFromGlobals();
        $hostName = 'https://'.$request->getHttpHost();

        preg_match("#http.*#i", $user_img, $matches);
        if ($matches) {
            $src = $user_img;
        } else {
            if ($user_img == null) {
                $src = $hostName."/img/defaultUserImg.png";
            } else {
                $src = $hostName."/".$user_img;
            }
        }

        return $src;

    }
}
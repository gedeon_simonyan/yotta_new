<?php
/**
 * Created by PhpStorm.
 * User: Gedeon
 * Date: 26-Feb-19
 * Time: 13:49
 */

namespace ApiBundle\Command;


use ApiBundle\Repository\NoteRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NotificationCommand extends  Command
{

    private $noteRepository;

    public function __construct(NoteRepository $noteRepository)
    {
        $this->noteRepository = $noteRepository;

        parent::__construct();
    }
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('yotta:send_notifications')
            // the short description shown while running "php bin/console list"
            ->setDescription('Update past leagues data')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows update past leagues data...");
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        $last20Notes = $this->noteRepository->getLast20DayNotes();
        if($last20Notes){
            $output->writeln('Last 20 notes added successfully!');
        }
        $lastNotes = $this->noteRepository->getLastDayNotes();
        if($lastNotes){
            $output->writeln('Last notes added successfully!');
        }
    }
}
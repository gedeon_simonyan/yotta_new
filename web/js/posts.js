(function(p,c,main,a){
    "use strict";
    a = c.module('appYotta');
    a.controller('PostsController', ['$scope','$rootScope','$http','$sce','$filter', function ($scope,$rootScope,$http,$sce,$filter) {
        $scope.loadingText = "Loading ..."
        $scope.likeHover = function (bookId,fullCount,el) {
            if (fullCount != "undefined"){
                el.css("visibility","visible");
                if (el.html() == ""){
                    el.html( $scope.loadingText);
                }
                if (el.html() ==  $scope.loadingText){
                    $http({
                        method: 'POST',
                        url: "/getLikes/"+bookId,
                        data: {
                            offset: 0
                        }
                    }).then(function successCallback(response) {
                            var newMessages = response.data.users;
                            var inlineText = "";
                            for(var i = 0; i < newMessages.length; ++i){
                                var str = newMessages[i].firstname + " " + newMessages[i].lastname;
                                str = str.substring(0,24);
                                inlineText += str + "<br/>";
                            }

                            var difference = fullCount - response.data.users.length;
                            if (difference != 0 ){
                                inlineText += "and "+difference+" more...";
                            }
                            el.html(inlineText);
                        },
                        function errorCallback(response) {
                            showErrorAlert("Something went wrong. Please try again later!");
                        });
                }
            }
        };

        $scope.likeLeave = function (el) {
            el.css("visibility","hidden");
        };

        $scope.clickedLikes = function (bookId,countOfLikes,el) {
            if (countOfLikes != "undefined"){
                el.css("visibility","hidden");
                $rootScope.$emit('show-user-list', {"icon":"/img/icons/heart.png","type":"likes","endpoint":"/getLikes/"+bookId,"fullcount":countOfLikes});
            }
        };
    }]);

})(window,window.angular,document.getElementById("mainContainer"));
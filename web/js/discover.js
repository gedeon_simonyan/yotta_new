
$(document).ready(function () {
    /*********SEARCH AREA JS********/
    $('#search-param>span').click(function () {
        $('.search-area-block').slideToggle('slow');
    });

    // $('input[type=text],input[type=password],input[type=email]').focus(function () {
    //     $(this).parent().css({
    //         'border-color': '#66afe9',
    //         'outline': 0,
    //         '-webkit-box-shadow': 'inset 0 1px 1px rgba(0,0,0,.075),0 0 30px rgba(0,0,0,.6)',
    //         'box-shadow': 'inset 0 1px 1px rgba(0,0,0,.075),0 0 30px rgba(0,0,0,.6)'
    //     });
    // });
    //
    // $('input[type=text]').blur(function () {
    //     $(this).parent().css({
    //         'border-color': '#b7b7b7',
    //         '-webkit-box-shadow': 'none'
    //     });
    // });

    $(".newsletter").children('input[type=text]').focus(function () {
        $(this).parent().css({
            'border-color': '#66afe9',
            'outline': 0,
            '-webkit-box-shadow': 'inset 0 1px 1px rgba(0,0,0,.075),0 0 30px rgba(0,0,0,.6)',
            'box-shadow': 'inset 0 1px 1px rgba(0,0,0,.075),0 0 30px rgba(0,0,0,.6)'
        });
    });
    $(".newsletter").children('input[type=text]').blur(function () {
        $(this).parent().css({
            'border-color': '#b7b7b7',
            '-webkit-box-shadow': 'none'
        });
    });



    /*******Storing search aria radio value in localStorage********/

    $("input[name=radio1]").on("change", function () {
        localStorage.setItem('searchArea', $(this).val());
    });
    $("input[name=radio2]").on("change", function () {
        localStorage.setItem('searchFilter', $(this).val());
    });
    $("input[name=radio3]").on('change', function () {
        localStorage.setItem('language', $(this).val());
    });

    var searchArea = localStorage.getItem('searchArea');
    var searchFilter = localStorage.getItem('searchFilter');
    var language = localStorage.getItem('language');

    if (searchArea) {
        $("input[value=" + searchArea + "]").prop('checked', true);
    }
    if (searchFilter) {
        $("input[value=" + searchFilter + "]").prop('checked', true);
    }
    if (language) {
        $("input[value=" + language + "]").prop('checked', true);
    }

    /********Slick slider**********/
    var windowLocation = window.location.pathname;
    if (windowLocation == "/discover") {
        /******init. slick carousel********/
        $('.lazy').slick({
            lazyLoad: 'ondemand',
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            infinite: false,
            prevArrow: false,
        });

        $('.responsive').slick({
            autoplay: true,
            autoplaySpeed: 2500,
            dots: false,
            infinite: false,
            speed: 400,
            slidesToShow: 6,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1800,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1,
                        infinite: false,
                        dots: false
                    }
                },
                {
                    breakpoint: 1500,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        infinite: false,
                        dots: false
                    }
                },
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: false,
                        dots: false
                    }
                },
                {
                    breakpoint: 820,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }

                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    }


    /*******Search book by Author directly clicking on author name********/

    $(document).delegate('.book-item-auther', 'click', function () {
        var val = $(this).text();
        var where = $('input[name=radio1]:checked').val();
        var lang = $('input[name=radio3]:checked').val();
        var searchBy = 'by_author';
        if(val.length > 1){
            search(val, where, searchBy,lang);
        }
    });

    /*******Search book by Author directly clicking on author name from Top Rated block********/
    $(document).delegate('.bookAuthor','click', function () {
        var bookTitle = $(this).text();
        var searchBy = 'by_author';
        var where = $('input[name=radio1]:checked').val();
        var lang = $('input[name=radio3]:checked').val();
        if (bookTitle.length >= 1) {
            search(bookTitle, where, searchBy, lang)
        }
    });

    /*******pagination for top search result, tag result********/
    var searchWindowLocation = window.location.pathname;
    var tagSearchResultLocation = window.location.pathname.substr(0, 5);
    if ((searchWindowLocation == "/searchResult") || (tagSearchResultLocation == "/tag/")) {
        /*******get cookie********/
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        var totalPages = getCookie('totalPages');
        var startPage = parseInt(getCookie('startPage'));
        if (totalPages > 1) {
            /******get pagination lenght from  cookie*******/
            var $pagination = $('#pagination-demo');
            var defaultOpts = {
                totalPages: totalPages,
                startPage: startPage
            };
            $pagination.twbsPagination(defaultOpts);
        }

        $(document).delegate('.page-link', 'click', function () {
            var startPage = $(this).text();
            if(startPage === "Next" || startPage === "Previous"){
                startPage = $('.active').children('.page-link').text();
            }
            if(startPage === "First"){
                startPage = 1;
            }
            if(startPage === "Last"){
                startPage = totalPages;
            }
            var uriPath = window.location.href;
            if (uriPath.indexOf("startPage") >= 0){
                uriPath = uriPath.substring(0, uriPath.length - 12);
            }
            window.location = uriPath + '&startPage=' + startPage
        });
    }



    // if (windowLocation == "/tag/short-stories") {
    //     /*******get cookie********/
    //     function getCookie(cname) {
    //         var name = cname + "=";
    //         var decodedCookie = decodeURIComponent(document.cookie);
    //         var ca = decodedCookie.split(';');
    //         for (var i = 0; i < ca.length; i++) {
    //             var c = ca[i];
    //             while (c.charAt(0) == ' ') {
    //                 c = c.substring(1);
    //             }
    //             if (c.indexOf(name) == 0) {
    //                 return c.substring(name.length, c.length);
    //             }
    //         }
    //         return "";
    //     }
    //
    //     var totalPages = getCookie('totalPages');
    //     var startPage = parseInt(getCookie('startPage'));
    //     if (totalPages > 1) {
    //         /******get pagination lenght from  cookie*******/
    //         var $pagination = $('#pagination-demo');
    //         var defaultOpts = {
    //             totalPages: totalPages,
    //             startPage: startPage
    //         };
    //         $pagination.twbsPagination(defaultOpts);
    //     }
    //
    //     $(document).delegate('.page-link', 'click', function () {
    //         var startPage = $(this).text();
    //         if(startPage === "Next" || startPage === "Previous"){
    //             startPage = $('.active').children('.page-link').text();
    //         }
    //         if(startPage === "First"){
    //             startPage = 1;
    //         }
    //         if(startPage === "Last"){
    //             startPage = totalPages;
    //         }
    //         var uriPath = window.location.href;
    //         if (uriPath.indexOf("startPage") >= 0){
    //             uriPath = uriPath.substring(0, uriPath.length - 12);
    //         }
    //         window.location = uriPath + '?startPage=' + startPage
    //     });
    // }


    //     $pagination.twbsPagination(defaultOpts);
    // $.ajax({
    //     url:'/searchResult',
    //     type: "POST",
    //     success: function (success) {
    //         console.log(success);
    //         var totalPages = 2;
    //         // var currentPage = $pagination.twbsPagination('getCurrentPage');
    //         $pagination.twbsPagination('destroy');
    //         $pagination.twbsPagination($.extend({}, defaultOpts, {
    //             // startPage: currentPage,
    //             totalPages: totalPages
    //         }));
    //     }
    // });


//Pagination
//
//     $(document).delegate('.item','click', function(){
//         var that = $(this);
//         var thatId = that.attr('id');
//         var active = $('.activeItem');
//         var activeId = active.attr('id');
//         var lastPage = $('.lastPage');
//         var lastPageId = lastPage.attr('id');
//         var lastPageIdFirst = $('.lastPageIdFirst').attr('id');
//         var firstItem = $('.firstItem').attr('id');
//         var total = $('.total_pages');
//         var totalId = total.attr('id');
//         var page = 1;
//         var pageFrom = 1;
//         var pageTill = lastPageId;
//         var first = $('.first');
//         var firstId = first.attr('id');
//         var nextItem = null;
//         var prevItem = null;
//         var next = null;
//         var prev = null;
//         var prevItem = $('.prev');
//         var prevItemNext = prevItem.next().attr('id');
//
//         function addActive(id){
//             $('#'+id).addClass('activeItem');
//         }
//
//         $('.item').removeClass('activeItem');
//         that.not('.prev').not('.next').addClass('activeItem');
//
//         $('.item').removeClass('activeItem');
//         that.not('.prev').not('.next').addClass('activeItem');
//
//         if(that.hasClass('first')){
//             page = 1;
//             if(active.hasClass('lastPage')){
//                 pageTill = lastPageIdFirst;
//             }
//         }else if(that.hasClass('last')){
//             page = totalId;
//             if(page > 10){
//                 pageFrom = totalId - 9;
//                 pageTill = totalId;
//             }
//         }else if(that.hasClass('prev')){
//             page = activeId - 1;
//             console.log(page);
//             if(page == 0){
//                 page = 1;
//             }else if(page >= 10){
//                 pageFrom = activeId - 10;
//                 pageTill = pageFrom + 9;
//             }
//             console.log(page + " " + pageFrom + " " + pageTill);
//         }else if(that.hasClass('next')){
//             page = parseInt(activeId) + 1;
//             if(page > 10 && active.next().hasClass('next')){
//                 if(page >= totalId){
//                     page = totalId;
//                     pageFrom = totalId - 9;
//                     pageTill = totalId;
//                 }else{
//                     pageFrom = totalId - 10;
//                     pageTill = pageFrom + 9;
//                 }
//             }else if(page < 10 && active.next().hasClass('next')){
//                 page = totalId;
//             }
//         }else{
//             page = thatId;
//             pageFrom = parseInt(prevItemNext);
//             if(totalId > 10){
//                 pageTill = pageFrom + 9;
//             }
//         }
//
//         $.ajax({
//             url:window.location.pathname,
//             method:'POST',
//             data:{page:page, pageFrom:pageFrom, pageTill:pageTill, lastPageIdFirst:lastPageIdFirst},
//             success:function(data){
//                 data = $.parseJSON(data);
//
//                 var books = data.books;
//                 var pageFrom = data.pageFrom;
//                 var pageTill = data.pageTill;
//                 var page = data.page;
//
//                 var result = '';
//
//
//                 for(var i = 0; i < books.length; i++){
//
//                     var pubDate = books[i].publishDate.date;
//                     var pat = /^[-0]/;
//                     var res = pubDate.match(pat);
//                     if(res){
//                         books[i].publishDate.date = '00-00-0000';
//                     }
//                     var authors = books[i].authors;
//                     if(authors == null){
//                         books[i].authors = 'Unknown Author';
//                     }
//
//                     result +=
//                         '<div class="result-item white">'
//                         +'<div class="result-item-image">'
//                         +'<img src="'+books[i].image+'" alt="book" title="book" />'
//                         +'</div>'
//                         +'<div class="result-item-content left">'
//                         +'<h2 class="book-item-title">'+books[i].title+'</h2>'
//                         +'<p class="book-item-auther">'+books[i].authors+'</p>'
//                         +'<p class="book-item-location">'+books[i].publisher+'</p>'
//                         +'<p class="book-item-publish">'+books[i].publishDate.date+'</p>'
//                         +'<p class="book-item-lenguage">'+books[i].language+'</p>'
//                         +'</div>'
//                         +'<div class="result-item-vertical-line left"></div>'
//                         +'<div class="clear"></div>'
//                         +'<div class="add-lent-book-block right">'
//                         +'<div class="add-book">Add book'
//                         +'<div class="tooltip" id="'+books[i].bookId+'">'
//                         +'<div class="triangle"></div>'
//                         +'<div class="book-read tooltip-items">Read</div>'
//                         +'<div class="book-reading tooltip-items">Reading</div>'
//                         +'<div class="book-wish tooltip-items">Wish</div>'
//                         +'<div class="book-want tooltip-items">Want</div>'
//                         +'</div>'
//                         +'</div>'
//                         +'<div class="lent-book">Lent book</div>'
//                         +'</div>'
//                         +'<div class="clear"></div>'
//                         +'</div>';
//                 }
//                 var pagination = '<div class="pagination"><a href="#" class="item first" id="0">FIRST</a><a href="#" class="item prev">PREV</a>';
//                 var lastId = '';
//                 for(var i = pageFrom; i <= pageTill; i++){
//                     if(i == pageTill){
//                         if(i == page){
//                             pagination += '<a href="#" class="item lastPage activeItem" id="'+i+'">'+i+'</a>';
//                         }else{
//                             pagination += '<a href="#" class="item lastPage" id="'+i+'">'+i+'</a>';
//                         }
//                         lastId = i;
//                     }else if(i == page){
//                         pagination += '<a href="#" class="item activeItem" id="'+i+'">'+i+'</a>';
//                     }else{
//                         pagination += '<a href="#" class="item" id="'+i+'">'+i+'</a>';
//                     }
//                 }
//
//                 var total = parseInt(lastId) + 1;
//                 pagination +=
//                     '<a href="#" class="item next">NEXT</a>'
//                     +'<a href="#" class="item last" id="'+total+'">LAST</a>'
//                     +'<span class="lastPageIdFirst" id="'+ data.lastPageIdFirst +'"></span>'
//                     +'<span class="total_pages" id="'+ data.totalPages +'"></span>'
//                     +'</div>';
//                 result += pagination;
//
//                 $('.book-search-result').html(result);
//             }
//         });
//
//     });


    /******Scroll bar*********/

    //
    // /******fixing screen size changes******/
    // var $window = $(window);
    // var $pane = $('.topRatedBooks .ratedBooksRow');
    // var $paneMiddle = $('.lastRatedBooks .ratedBooksRow');
    // var $bottomMiddle = $('.topMusic .musicListRow');
    // // var $question = $('.questions-block .questionSlide');
    //
    // function checkWidth() {
    //     var windowsize = $window.width();
    //     if (windowsize > 751) {
    //         $pane.animate({scrollLeft: '-='+ 4000  }, 100);
    //         $paneMiddle.animate({scrollLeft: '-='+ 4000  }, 100);
    //         $bottomMiddle.animate({scrollLeft: '-='+ 4000  }, 100);
    //         // $question.animate({scrollLeft: '-='+ 4000  }, 100);
    //
    //     }
    // }
    //
    // /*** Books fixing scroll size****/
    // var screen = $(window);
    // var bookItem = $(".topRatedBooks .ratedBooksRow .ratedBooks");
    // var scrollSize = [];
    // function fixingScrollSize() {
    //     var windowsize = screen.width();
    //     if(windowsize <= 751){
    //         return scrollSize =  bookItem.width() + 24;
    //         // var x = {scrollSize:bookItem.width() + 24}
    //     }
    //     else if(windowsize > 751 && windowsize <= 975){
    //         return scrollSize =  bookItem.width() + 28;
    //     }
    //     else if(windowsize > 975 && windowsize <= 1183){
    //         return scrollSize =  bookItem.width() + 28;
    //     }
    //     else if(windowsize > 1183 && windowsize <= 1349){
    //         return scrollSize =  bookItem.width() + 28;
    //     } else {
    //         return scrollSize =  bookItem.width() + 18;
    //     }
    //
    // }
    //
    // /*** Music fixing scroll size****/
    // var musicItem = $(".topMusic .musicListRow .musicItems");
    // var scrollSizeMusic = 0;
    // function fixingScrollSizeMusic() {
    //     var windowsize = screen.width();
    //     if(windowsize <= 751){
    //         return scrollSizeMusic =  musicItem.width() + 24;
    //         // var x = {scrollSize:bookItem.width() + 24}
    //     }
    //     else if(windowsize > 751 && windowsize <= 975){
    //         return scrollSizeMusic =  musicItem.width() + 32;
    //     }
    //     else if(windowsize > 975 && windowsize <= 1183){
    //         return scrollSizeMusic =  musicItem.width() + 34;
    //     }
    //     else if(windowsize > 1183 && windowsize <= 1349){
    //         return scrollSizeMusic =  musicItem.width() + 34;
    //     } else {
    //         return scrollSizeMusic =  musicItem.width() + 18;
    //     }
    //
    //
    // }
    //
    // /*** Question fixing scroll size****/
    // var questionItem = $(".questions-block .questionSlide .questionSlideItems");
    // var questionScrollSize = 0;
    // function fixingScrollSizeQuestion() {
    //     var windowsize = screen.width();
    //     if(windowsize <= 751){
    //         return questionScrollSize =  questionItem.width() + 23;
    //         // var x = {scrollSize:bookItem.width() + 24}
    //     }
    //     else if(windowsize > 751 && windowsize <= 975){
    //         return questionScrollSize =  questionItem.width() + 24;
    //     }
    //     else if(windowsize > 975 && windowsize <= 1183){
    //         return questionScrollSize =  questionItem.width() + 24;
    //     }
    //     else if(windowsize > 1183 && windowsize <= 1349){
    //         return questionScrollSize =  questionItem.width() + 24;
    //     } else {
    //         return questionScrollSize =  questionItem.width() + 24;
    //     }
    //
    // }
    //
    // /*******getting book count*********/
    //
    // var bookCount = $(".topRatedBooks .ratedBooksRow .ratedBooks").length;
    // var count = 0;
    // function fixingCount() {
    //     var windowsize = $window.width();
    //     if(windowsize <= 751){
    //         return count = bookCount - 2;
    //     }
    //     else if(windowsize > 751 && windowsize <= 975){
    //         return count = bookCount - 3;
    //     }
    //     else if(windowsize > 975 && windowsize <= 1183){
    //         return count = bookCount - 4;
    //     }
    //     else if(windowsize > 1183 && windowsize <= 1349){
    //         return count =  bookCount - 4;
    //     } else {
    //         return count =  bookCount - 4;
    //     }
    // }
    //
    // /*******getting music count*********/
    //
    // var musCount = 0;
    // var musicCount = $(".topMusic .musicListRow .musicItems").length;
    // function fixingMusicCount() {
    //     var windowsize = $window.width();
    //     if(windowsize <= 751){
    //         return musCount = musicCount - 3;
    //     }
    //     else if(windowsize > 751 && windowsize <= 975){
    //         return musCount = musicCount - 4;
    //     }
    //     else if(windowsize > 975 && windowsize <= 1183){
    //         return musCount = musicCount - 5;
    //     }
    //     else if(windowsize > 1183 && windowsize <= 1349){
    //         return musCount =  musicCount - 5;
    //     } else {
    //         return musCount =  musicCount - 5;
    //     }
    // }
    //
    // /******fixing x count******/
    // var x = 0;
    // function fixingButtonClickCount() {
    //     var windowsize = $window.width();
    //     if(windowsize <= 751){
    //         return x = { x: 0, y: 0, z: 0 };
    //     }
    //     else if(windowsize > 751 && windowsize <= 975){
    //         return x = { x: 0, y: 0, z: 0 };
    //     }
    //     else if(windowsize > 975 && windowsize <= 1183){
    //         return x = { x: 0, y: 0, z: 0 };
    //     }
    //     else if(windowsize > 1183 && windowsize <= 1349){
    //         return x = { x: 0, y: 0, z: 0 };
    //     } else {
    //         return x = { x: 0, y: 0, z: 0 };
    //     }
    // }
    //
    //
    //
    // // Execute on load
    // checkWidth();
    // fixingScrollSize();
    // fixingCount();
    // fixingButtonClickCount();
    // fixingScrollSizeMusic();
    // fixingMusicCount();
    // fixingScrollSizeQuestion();
    // // Bind event listener
    // $(window).resize(fixingScrollSize);
    // $(window).resize(checkWidth);
    // $(window).resize(fixingCount);
    // $(window).resize(fixingButtonClickCount);
    // $(window).resize(fixingScrollSizeMusic);
    // $(window).resize(fixingMusicCount);
    // $(window).resize(fixingScrollSizeQuestion);
    //
    //
    //
    // // var x = 0;
    // /*********TOP RATED BOOKS BUTTON***********/
    // $(".nextButtonTop").click(function () {
    //     if (x.x <= count - 1) {
    //         x.x++;
    //         $('.topRatedBooks .ratedBooksRow').animate({scrollLeft: '+='+ scrollSize  }, 100);
    //         $(".nextButtonTop").css("background", "rgba(0, 0, 0, 0.5)");
    //         $(".previewButtonTop").css({
    //             "background": "rgba(0, 0, 0, 0.5)",
    //             "cursor": "pointer",
    //             "hover": " border: 1px solid rgba(0, 0, 0, 0.1)"
    //         })
    //     }
    //     // if (x === count) {
    //     //     $(".nextButtonTop").css({
    //     //         "background": "rgba(0, 0, 0, 0.1)",
    //     //         "cursor": "context-menu",
    //     //         "border": "none"
    //     //     })
    //     // }
    // });
    //
    // $(".previewButtonTop").click(function () {
    //     if (x.x > 0) {
    //         x.x--;
    //         $('.topRatedBooks .ratedBooksRow').animate({scrollLeft: '-='+ scrollSize}, 100);
    //         $(".nextButtonTop").css({
    //             "background": "rgba(0, 0, 0, 0.5)",
    //             "cursor": "pointer",
    //             "hover": "border: 1px solid rgba(0, 0, 0, 0.1)"
    //         });
    //     }
    //     // if (x == 0) {
    //     //     $(".previewButtonTop").css({
    //     //         "background": "rgba(0, 0, 0, 0.1)",
    //     //         "cursor": "context-menu",
    //     //         "border": "none"
    //     //     })
    //     // }
    // });
    //
    // /**********LAST RATED BOOKS BUTTONS*************/
    //
    //
    // $(".nextButtonLast").click(function () {
    //     if (x.y <= count - 1) {
    //         x.y++;
    //         $('.lastRatedBooks .ratedBooksRow').animate({scrollLeft: '+='+ scrollSize}, 100);
    //         $(".nextButtonLast").css("background", "rgba(0, 0, 0, 0.5)");
    //         $(".previewButtonLast").css({
    //             "background": "rgba(0, 0, 0, 0.5)",
    //             "cursor": "pointer",
    //             "hover": " border: 1px solid rgba(0, 0, 0, 0.1)"
    //         })
    //     }
    //     // if (y == count) {
    //     //     $(".nextButtonLast").css({
    //     //         "background": "rgba(0, 0, 0, 0.1)",
    //     //         "cursor": "context-menu",
    //     //         "border": "none"
    //     //     })
    //     // }
    // });
    //
    // $(".previewButtonLast").click(function () {
    //     if (x.y > 0) {
    //         x.y--;
    //         $('.lastRatedBooks .ratedBooksRow').animate({scrollLeft: '-='+ scrollSize}, 100);
    //         $(".nextButtonLast").css({
    //             "background": "rgba(0, 0, 0, 0.5)",
    //             "cursor": "pointer",
    //             "hover": "border: 1px solid rgba(0, 0, 0, 0.1)"
    //         });
    //     }
    //     // if (y == 0) {
    //     //     $(".previewButtonLast").css({
    //     //         "background": "rgba(0, 0, 0, 0.1)",
    //     //         "cursor": "context-menu",
    //     //         "border": "none"
    //     //     })
    //     // }
    // });
    //
    // /**********Music *************/
    //
    // $(".nextButtonMusic").click(function () {
    //
    //     if (x.z <= musCount - 1) {
    //         x.z++;
    //         $('.topMusic .musicListRow').animate({scrollLeft: '+=' + scrollSizeMusic}, 100);
    //         $(".nextButtonMusic").css("background", "rgba(0, 0, 0, 0.5)");
    //         $(".previewButtonMusic").css({
    //             "background": "rgba(0, 0, 0, 0.5)",
    //             "cursor": "pointer",
    //             "hover": " border: 1px solid rgba(0, 0, 0, 0.1)"
    //         })
    //     }
    //     // if (music === count) {
    //     //     $(".nextButtonMusic").css({
    //     //         "background": "rgba(0, 0, 0, 0.1)",
    //     //         "cursor": "context-menu",
    //     //         "border": "none"
    //     //     })
    //     // }
    // });
    //
    // $(".previewButtonMusic").click(function () {
    //     if (x.z > 0) {
    //         x.z--;
    //         $('.topMusic .musicListRow').animate({scrollLeft: '-=' + scrollSizeMusic}, 100);
    //         $(".nextButtonMusic").css({
    //             "background": "rgba(0, 0, 0, 0.5)",
    //             "cursor": "pointer",
    //             "hover": "border: 1px solid rgba(0, 0, 0, 0.1)"
    //         });
    //     }
    //     // if (music == 0) {
    //     //     $(".previewButtonMusic").css({
    //     //         "background": "rgba(0, 0, 0, 0.1)",
    //     //         "cursor": "context-menu",
    //     //         "border": "none"
    //     //     })
    //     // }
    // });

    /********Discover Subscriptions*********/

    $('#discover_subscribe').click(function () {
        var email = $(this).parent().find('#textInput').val();
        var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (email.length > 0) {
            if (pattern.test(email)) {
                // if (email) {
                $.ajax({
                    type: 'POST',
                    url: "/subscribtion",
                    data: email,
                    success: function (response) {
                        if (response) {
                            $('.newsletter .notify-tooltip-error >.ng-binding').text(response);
                            $('.newsletter .notify-tooltip-error').fadeIn(1000);
                            setTimeout(function () {
                                $('.newsletter .notify-tooltip-error').fadeOut(1000);
                            }, 4000);
                        } else {
                            $('.newsletter>input').fadeOut(800);
                            setTimeout(function () {
                                $(".newsletter").append("<p style='display: none'>Thank you for subsctibtion</p>");
                                $(".newsletter p").fadeIn(1000);
                            }, 600);
                        }
                    }
                });
            } else {
                $('.newsletter .notify-tooltip-error >.ng-binding').text('Invalid Email!');
                $('.newsletter .notify-tooltip-error').fadeIn(1000);
                setTimeout(function () {
                    $('.newsletter>.notify-tooltip-error').fadeOut(1000);
                }, 4000);
            }
        }
    });


    var question = 0;
    $(".nextButtonQuestions").click(function () {
        var count = 2;
        if (question < count) {

            $(".questionSlide>.questionSlideItems:first-child").animate({width: 0}, function () {
                $(this).remove();
                question++;
                if (question === 2) {
                    var forQuestion = ".fourQuestion";
                    $(forQuestion).append("<div class='sendQuestionButton' >Send</div>");
                    $(forQuestion).remove('nextButtonQuestions');
                    $("div.nextButtonQuestions").remove();
                }
            });

            // $('.questionSlide').animate({scrollLeft: '+=' + questionScrollSize}, 100);
            $(".nextButtonQuestions").css("background", "rgba(0, 0, 0, 0.5)");
        }
        if (question === count) {
            $(".nextButtonQuestions").css({
                "background": "rgba(0, 0, 0, 0.1)",
                "cursor": "context-menu",
                "border": "none"
            })
        }
    });

    /* discover page open the popup */
    $('.tag-tools .rateCount').click(function () {
        $(this).prepend("<div class='ratedBooksAnimate'>For more action please Log in </div>");
        $('.ratedBooksAnimate').fadeOut(3500, function () {
            $('.ratedBooksAnimate').remove();
        })
    });

    $('.timeline-section-discover .rateCount').click(function () {
        $(this).prepend("<div class='ratedBooksAnimate discover'>For more action please Log in </div>");
        $('.ratedBooksAnimate').fadeOut(3500, function () {
            $('.ratedBooksAnimate').remove();
        })
    });

    $('.newReleaseButtons').click(function () {
        $(this).prepend("<div class='newReleaseAnimate newReleasePopup'>For more action please Log in </div>");
        $('.newReleaseAnimate').fadeOut(3500, function () {
            $('.newReleaseAnimate').remove();
        })
    });

    $('.tooltip').click(function () {
        $(this).prepend("<div class='tooltipAnimate addBook'>For more action please Log in </div>");
        $('.tooltipAnimate').fadeOut(3500, function () {
            $('.tooltipAnimate').remove();
        })
    });

    $('.lent-book').click(function () {
        $(this).prepend("<div class='tooltiLendAnimate lendBook'>For more action please Log in </div>");
        $('.tooltiLendAnimate').fadeOut(3500, function () {
            $('.tooltiLendAnimate').remove();
        })
    });

    /*******open popup individual book page********/
    $('.add-writing').click(function () {
        $(this).prepend("<div class='tooltiLendAnimate addWritingPopup'>For more action please Log in </div>");
        $('.tooltiLendAnimate').fadeOut(3500, function () {
            $('.tooltiLendAnimate').remove();
        })
    });
    $('.add-tag-btn').click(function () {
        $(this).prepend("<div class='tooltiLendAnimate addTagPopup'>For more action please Log in </div>");
        $('.tooltiLendAnimate').fadeOut(3500, function () {
            $('.tooltiLendAnimate').remove();
        })
    });

    $('.add-markup-btn button').click(function () {
        $(this).prepend("<div class='tooltiLendAnimate addMarkButtonPopup'>For more action please Log in </div>");
        $('.tooltiLendAnimate').fadeOut(3500, function () {
            $('.tooltiLendAnimate').remove();
        })
    });

    $('.likes img').click(function () {
        $(this).parent().parent().prepend("<div class='tooltiLendAnimate addLikesPopup'>For more action please Log in </div>");
        $('.tooltiLendAnimate').fadeOut(3500, function () {
            $('.tooltiLendAnimate').remove();
        })
    });

    /********Individual book page*********/
    $('.more').on('click', function(){
        $('.dots').animate({opacity: 'toggle', width:'toggle'}, 1000).css({height: '15px'});
        $('.rest-description').animate({opacity: 'toggle'}, 1000);

        $('.more').animate({opacity:'toggle'}, 1000);

        setTimeout(function(){
            if($('.more').text() == 'more'){
                $('.more').text('less').animate({opacity:'toggle'}, 1000);
            }else{
                $('.more').text('more').animate({opacity:'toggle'}, 1000);
            }
        }, 1000);
    });

    /******STORING VALUES IN COOKIE FOR USING IN BREADCRUMP****/
    $(".post-item-title").click(function () {
        var bookName = $(this).children('a').text();
        document.cookie = 'bookName='+ bookName +';expires="";path=/';
    });

    $(".ratedBookImage").click(function () {
        var bookName = $(this).next('p').children('a').text();
        document.cookie = 'bookName='+ bookName +';expires="";path=/'
    });
    $(".ratedBookImage").next('p').click(function () {
        var bookName = $(this).children('a').text();
        document.cookie = 'bookName='+ bookName +';expires="";path=/'
    });
    $(".book-tag").click(function () {
        var tagName = $(this).attr('href');
        document.cookie = 'tagName='+ tagName +';expires="";path=/'
    });
});


// /*******SEARCH AREA: get params from local storage and searc book, reset default params*******/
// var searchBook = function () {
//     var searchText = $('#search-param').children('div').children('input').val();
//
//     var searchIn = "";
//     if(localStorage.getItem('searchArea') != null){
//         searchIn = localStorage.getItem('searchArea');
//     } else {
//         searchIn = 'yottabook';
//     }
//     var searchBy = "";
//     if(localStorage.getItem('searchFilter') != null){
//         searchBy = localStorage.getItem('searchFilter');
//     } else {
//         searchBy = 'by_all';
//     }
//     var searchLang = "";
//     if(localStorage.getItem('language') != null){
//         searchLang = localStorage.getItem('language')
//     } else {
//         searchLang = ''
//     }
//     if(searchText.length > 1){
//
//         window.location = "/searchResult?searchText=" + searchText + '&searchIn=' + searchIn +
//                           '&searchBy=' + searchBy + '&searchLang=' + searchLang;
//     }
// };
//
// var resetDefaultParams = function () {
//     var searchText = "";
//     var searcIn = $("input[value=yottabook]").prop('checked', true).val();
//     var searcBy = $("input[value=by_all]").prop('checked', true).val();
//     var searchLang = $("input[value='']").prop('checked', true).val();
//     localStorage.setItem('searchArea', searcIn);
//     localStorage.setItem('searchFilter', searcBy);
//     localStorage.setItem('language', searchLang);
// };
//
// var search = function (byWhat, where, searchBy, lang) {
//     window.location = "/searchResult?searchText=" + byWhat + '&searchIn='
//         + where + '&searchBy=' + searchBy + '&searchLang=' + lang;
// };

/*******SEARCH AREA: get params from local storage and searc book, reset default params*******/
var searchBook = function () {
    var searchText = $('#search-param').children('div').children('input').val();

    var searchIn = "";
    if(localStorage.getItem('searchArea') != null){
        searchIn = localStorage.getItem('searchArea');
    } else {
        searchIn = 'yottabook';
    }
    var searchBy = "";
    if(localStorage.getItem('searchFilter') != null){
        searchBy = localStorage.getItem('searchFilter');
    } else {
        searchBy = 'by_all';
    }
    var searchLang = "";
    if(localStorage.getItem('language') != null){
        searchLang = localStorage.getItem('language')
    } else {
        searchLang = ''
    }
    if(searchText.length > 1){

        window.location = "/searchResult?searchText=" + searchText + '&searchIn=' + searchIn +
            '&searchBy=' + searchBy + '&searchLang=' + searchLang;
    }
};

var resetDefaultParams = function () {
    var searchText = "";
    var searcIn = $("input[value=yottabook]").prop('checked', true).val();
    var searcBy = $("input[value=by_all]").prop('checked', true).val();
    var searchLang = $("input[value='']").prop('checked', true).val();
    localStorage.setItem('searchArea', searcIn);
    localStorage.setItem('searchFilter', searcBy);
    localStorage.setItem('language', searchLang);
};

var search = function (byWhat, where, searchBy, lang) {
    window.location = "/searchResult?searchText=" + byWhat + '&searchIn='
        + where + '&searchBy=' + searchBy + '&searchLang=' + lang;
};

var checkForHidingBookItemDisplay = false;
$(document).ready(function () {
    libraryReadyFunction();
    addDelegates();
    $(document).mouseover(function (e) {
        //getting the menu class from html
        var container = $("#book_item_display");
        ifContainer = !container.is(e.target) && container.has(e.target).length === 0;

        next = $(".slick-next");
        ifNext = !next.is(e.target) && next.has(e.target).length === 0;

        prev = $(".slick-prev");
        ifPrev = !prev.is(e.target) && prev.has(e.target).length === 0;
        //check if the mouse is not inside the menu
        if (checkForHidingBookItemDisplay == true && ifContainer && next && prev) {
            hideDisplay()
        }
    });

    $( window).resize(function() {
        $('.responsive').slick('destroy');
       getSliderSettings()
    });

    $( ".mainContainer" ).scroll(function() {
        hideDisplay();
    });
});


function addDelegates() {
    $(document).delegate('.filterBooks', 'click', function () {
        var path = window.location.pathname;
        var value = $.trim($(".filterSearchArea").val());
        var filterBy = $('.filterBy').val();
        if(!value) {
            return false;
        } else {
            showBlockAlert("Filtering books");
            var user_id = $("input[name=user_data]").val();
            $.ajax({
                type: 'post',
                url: "/filterBook",
                data: {
                    searchValue: value,
                    filterBy: filterBy,
                    user_id:user_id
                },
                success: function (data) {
                    hideAlert();
                    $('.responsive').removeClass("slick-initialized slick-slider");
                    if (path === "/library") {
                        $('.mainLibrary').empty().append(data);
                        $('.responsive').slick(getSliderSettings());
                    } else {
                        $('.onLoadSection').empty().append(data);
                        $('.responsive').slick(getSliderSettings());
                    }
                },
                error: function ( xhr, status, error) {
                    hideAlert();
                }
            });
        }
    });

    $(document).delegate('.reset', 'click', function () {
        var path = window.location.pathname;
        showBlockAlert("Filtering books");
        var user_id = $("input[name=user_data]").val();
        $.ajax({
            type: 'post',
            url: "/filterBook",
            data: {
                searchValue: "s",
                filterBy: "reset",
                user_id:user_id
            },
            success: function (data) {
                hideAlert();
                $('.responsive').removeClass("slick-initialized slick-slider");
                if (path === "/library") {
                    $('.mainLibrary').empty().append(data);
                    $('.responsive').slick(getSliderSettings());
                } else {
                    $('.onLoadSection').empty().append(data);
                    $('.responsive').slick(getSliderSettings());
                }
            },
            error: function ( xhr, status, error) {
                hideAlert();
            }
        });
    });
}

function hideDisplay() {
    $("#book_item_display").css("display","none");
    checkForHidingBookItemDisplay = false;
}

function libraryReadyFunction() {
    getSliderSettings();
    $(".responsive").css("visibility","visible");
}

function calcSlidesToShow() {
    width = $('.responsive').width();
    return Math.floor(width/125);
}

function getSliderSettings() {

    $('.responsive').slick({
        autoplay: false,
        autoplaySpeed: false,
        dots: false,
        infinite: false,
        slidesToShow: calcSlidesToShow(),
        speed: 400,
        variableWidth: true,
        draggable: false
    });
}

var timeoutOfShowingBook;

function hoverBookItem(element,item) {
    checkForHidingBookItemDisplay = false;
    bookItemDisplay = $("#book_item_display");
    $("#book_item_display_image").attr("src",item.image);
    $("#book_item_display_title").html(item.title);
    $("#book_item_display_author").html(item.authors);
    $("#book_item_display_publisher").html(item.publisher);
    $("#book_item_display_publish_date").html(item.publish_date);
    $("#book_item_display_publish_language").html(item.language);
    $("#book_item_display_publish_star").css("width",item.rate+"%");
    $("#book_item_display_click").attr("href","/book/"+item.books_book_id);

    elOffset = getAbsoluteBoundingRect(element);
    bookItemDisplay.css("top",elOffset.top);
    bookItemDisplay.css("left",elOffset.left - 20 - parseInt($("#mainContainer").css("margin-left")));
    bookItemDisplay.css("display","block");
    clearTimeout(timeoutOfShowingBook);
    timeoutOfShowingBook = setTimeout(function () {
        checkForHidingBookItemDisplay = true
    },100);

}


function getAbsoluteBoundingRect (el) {
    var doc  = document,
        win  = window,
        body = doc.body,

        // pageXOffset and pageYOffset work everywhere except IE <9.
        offsetX = win.pageXOffset !== undefined ? win.pageXOffset :
            (doc.documentElement || body.parentNode || body).scrollLeft,
        offsetY = win.pageYOffset !== undefined ? win.pageYOffset :
            (doc.documentElement || body.parentNode || body).scrollTop,

        rect = el.getBoundingClientRect();

    if (el !== body) {
        var parent = el.parentNode;

        // The element's rect will be affected by the scroll positions of
        // *all* of its scrollable parents, not just the window, so we have
        // to walk up the tree and collect every scroll offset. Good times.
        while (parent !== body) {
            offsetX += parent.scrollLeft;
            offsetY += parent.scrollTop;
            parent   = parent.parentNode;
        }
    }

    return {
        bottom: rect.bottom + offsetY,
        height: rect.height,
        left  : rect.left + offsetX,
        right : rect.right + offsetX,
        top   : rect.top + offsetY,
        width : rect.width
    };
}
//
// function offset(el) {
//     var rect = el.getBoundingClientRect(),
//         scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
//         scrollTop = window.pageYOffset || document.documentElement.scrollTop;
//     return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
// }


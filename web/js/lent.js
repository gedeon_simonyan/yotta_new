(function(p,c,a){
    "use strict";
    a = c.module('appYotta');
    a.directive('scrollBottom', function () {
        return {
            scope: {
                scrollBottom: "=",
            },
            link: function (scope, element) {
                scope.$watchCollection('scrollBottom', function (newValue) {
                    if (element.attr("firstTime") == "false"){
                        if (newValue) {
                            $(element).scrollTop($(element)[0].scrollHeight);
                        }
                    }
                    $(".post-user").attr("firstTime","false");
                });
            }
        }
    });

    a.controller('openLentController', ['$scope','$rootScope', function ($scope,$rootScope) {
        $scope.openLent = function () {
            $rootScope.$emit('show-lent', {});
        };
    }]);

    a.controller('lentController', ['$scope','$http','$sce','$filter','$rootScope','$q', function ($scope,$http,$sce,$filter,$rootScope,$q) {
        $scope.lentFriends = [];

        $rootScope.$on('show-lent', function(event, args) {
            $scope.openLent();
        });

        $scope.openLent = function () {
            $(".post-user").attr("firstTime","true");
            $scope.offset = 0;
            $( "#lent-form" ).datepicker({
            });
            $( "#return-date" ).datepicker({
                minDate: new Date()
            });

            var now = new Date();
            $('#lent-form').val($.datepicker.formatDate("mm/dd/yy",now)) ;
            $('#lent-form').show();
            $('#return-date').val($.datepicker.formatDate("mm/dd/yy",now)) ;
            $("#lent-book").css("visibility","visible");
            $("#lent-book .modal-body").animate({top:"50%"},400);
            $scope.loadFriends(null);
        };

        $scope.hideLent = function () {
            $scope.lentFriends = [];
            $scope.lentFriendsCache = [];
            setTimeout(function () {
                $("#lent-book").css("visibility","hidden");
            },400);
            $("#lent-book .modal-body").animate({top:"-100%"},400);
        };

        $scope.loadFriends = function ($searchText) {
            $http({
                method: 'POST',
                url: "/loadMoreFriendsLent",
                data: {
                    "offset":$scope.offset,
                    "bookId":$scope.bookId
                }
            }).then(function successCallback(response) {
                    $scope.lentFriends = $scope.lentFriends.concat(response.data);
                    $scope.offset = $scope.lentFriends.length;

                    $scope.lentFriendsCache = $scope.lentFriends;
                },
                function errorCallback(response) {
                });
        };

        $scope.searchFriendTextChanged = function () {
            if ($scope.canceler){
                $scope.canceler.resolve();
            }
            $(".post-user").attr("firstTime","true");
            if ($scope.searchValue == "" || $scope.searchValue.length < 3){
                $("#load-more-friends").css("visibility","visible");
                $scope.lentFriends = $scope.lentFriendsCache;
                return;
            }
            $("#load-more-friends").css("visibility","hidden");
            $scope.canceler = $q.defer();
            $http({
                method: 'POST',
                url: "/searchFriendLent",
                data: {
                    "searchText":$scope.searchValue
                }
            }).then(function successCallback(response) {
                    $(".post-user").attr("firstTime","true");
                    $scope.lentFriends = response.data;
                },
                function errorCallback(response) {
                });
        };

        $scope.lent = function(item,user_id){
            var lentFrom = new Date($('#lent-form').val());
            var returnDate = new Date($('#return-date').val());
            if ($('#lent-form').val() == "" || $('#return-date').val() == ""){
                showErrorAlert("Dates are required!!");
            } else if (lentFrom > returnDate){
                showErrorAlert("Return date must be grater.");
            } else {

                $http({
                    method: 'POST',
                    url: "/lent",
                    data: {
                        "user_id":user_id,
                        "book_id":$scope.bookId,
                        "lent_date":lentFrom,
                        "return_date":returnDate
                    }
                }).then(function successCallback(response) {
                        $(".post-user").attr("firstTime","true");
                        if ($("#load-more-friends").css("visibility") != "visible"){
                            $scope.removeElementByUserId(user_id);
                        } else {
                            $scope.removeElementByUserId(user_id);
                        }
                    },
                    function errorCallback(response) {
                    });
            }
        };

        $scope.removeElementByUserId = function(user_id){
            for (var i = 0; i < $scope.lentFriends.length; ++i){
                if ($scope.lentFriends[i].id == user_id){
                    $scope.lentFriends.splice(i,1);
                    break;
                }
            }

            for (var i; i < $scope.lentFriendsCache.length; ++i){
                if ($scope.lentFriendsCache[i].id == user_id){
                    $scope.lentFriendsCache.splice(i,1);
                    break;
                }
            }
            $scope.offset = $scope.lentFriendsCache.length;
        };

        $scope.openLentFromCalendar = function () {
            $( "#lent-form" ).datepicker("show");
        };

        $scope.openReturnDateCalendar = function () {
            $( "#return-date" ).datepicker("show");
        };

    }]);

})(window,window.angular);
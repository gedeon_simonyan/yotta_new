var loadingTemplate = '<article><div class="loading">' +
    '<div style="width: 100%; height: 230px; text-align: center; vertical-align: middle">' +
    'Loading. Please Wait...' +
    '</div>' +
    '</div></article>';
$(document).ready(function () {
    /* load timeline by default */
    if ($('.friends-menu li a').hasClass('active')) {
        // $('.loading').children('article').html(loadingTemplate);
        $('.onLoadSection').html(loadingTemplate);
        var param = $('.friends-menu li .active').attr('id');
        loadTimeline(param);
    }
    /* load timeline by clicking on it */
    $('#user_timeline').click(function () {
        if (!$(this).hasClass('active')) {
            $('.onLoadSection').empty();
            $('.onLoadSection').html(loadingTemplate);
            var param = $(this).attr('id');
            loadTimeline(param);
        }
    });
    /* load  library by clicking on it */
    $('#user_library').click(function () {
        if (!$(this).hasClass('active')) {
            $('.onLoadSection').empty();
            $('.onLoadSection').html(loadingTemplate);
            var param = $(this).attr('id');
            loadLibrary(param);
        }
    });
    /* load  friend by clicking on it */
    $('#user_friend').click(function () {
        if (!$(this).hasClass('active')) {
            $('.onLoadSection').empty();
            $('.onLoadSection').html(loadingTemplate);
            var param = $(this).attr('id');
            loadFriends(param);
        }
    });
    /* add friend/ unfriend / wating*/
    $('.friends-menu>li:nth-of-type(5)').click(function () {
        if (!$(this).hasClass('active')){
            var param = $('.friends-menu>li:nth-of-type(5)').children('a').attr('id');
            addFriendUnfriend(param);
        }
    });

    $("#write_message").click(function () {
        //fixedChat(user.user_id, user.friend_id, user.message_id, user.username, chatUsers, false)
        var userId = $(this).attr("user_id");
        var friendId = $(this).attr("friend_id");
        var username = $(this).attr("user_name");
        angular.element(document.getElementById('imageUploadForm')).scope().fixedChat(userId,friendId,0,username,[],false);
    });
});

function unActive() {
    $('.friends-menu li a').removeClass('active').css('cursor', 'pointer');
}

function loadTimeline(param) {
    var user_id = $("input[name=user_data]").val();
    if (user_id) {
        $.ajax({
            method: "POST",
            url: "/user/" + user_id,
            data: {
                template: param
            }, success: function (res) {
                $('.onLoadSection').empty();
                $('.onLoadSection').html(res);
                unActive();
                $("#user_timeline").addClass('active').css('cursor', 'default');
            }
        });
    }
}

function loadLibrary(param) {
    // $('.responsive').slick('destroy');
    $('.responsive').removeClass("slick-initialized slick-slider");
    var user_id = $("input[name=user_data]").val();
    if (user_id) {
        $.ajax({
            method: "POST",
            url: "/user/" + user_id,
            data: {
                template: param
            }, success: function (res) {
                unActive();
                $('.onLoadSection').empty();
                $('.onLoadSection').html(res);
                $("#user_library").addClass('active').css('cursor', 'default');
            }
        });
    }
}

function loadFriends(param) {
    var user_id = $("input[name=user_data]").val();
    if (user_id) {
        $.ajax({
            method: "POST",
            url: "/user/" + user_id,
            data: {
                template: param
            }, success: function (res) {
                $('.onLoadSection').empty();
                $('.onLoadSection').html(res);
                unActive();
                $("#user_friend").addClass('active').css('cursor', 'default');
            }
        });
    }
}

function addFriendUnfriend(param) {
    var user_id = $("input[name=user_data]").val();
    if (user_id && param !== "waitingFriend" ) {
        $.ajax({
            method: "POST",
            url: "/add/" + user_id,
            data: {
                template: param
            }, success: function (res) {
                var result = JSON.parse(res);
                if((result.status == true) && result.message == '+Add Friend') {
                    var html = "<a id='addFriend' style='cursor: pointer'>"+ result.message +"</a>"
                    var param = $('.friends-menu>li:nth-of-type(5)').empty();
                    $('.friends-menu>li:nth-of-type(5)').append(html);
                    if(result.coin){
                        showCoins(result.coin, "You Unfriend Friend");
                    }
                }
                if((result.status == true) && result.message == 'Unfriend') {
                    var html = "<a id='waitingFriend' >Waiting</a>"
                    var param = $('.friends-menu>li:nth-of-type(5)').empty();
                    $('.friends-menu>li:nth-of-type(5)').append(html);
                    if(result.coin){
                        showCoins(result.coin, "You Add Friend");
                    }
                }
                if(result === false){
                    return false;
                }
            }
        });
    }
}

function getSliderSettings() {
    $('.responsive').slick({
        autoplay: false,
        autoplaySpeed: false,
        dots: false,
        infinite: false,
        speed: 400,
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    prevArrow: 'none',
                    nextArrow: 'none'
                }
            },
            {
                breakpoint: 993,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5
                }
            },
            {
                breakpoint: 1201,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5
                }
            },
            {
                breakpoint: 1367,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 6
                }
            }, {
                breakpoint: 1500,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 6
                }
            }, {
                breakpoint: 1800,
                settings: {
                    slidesToShow: 7,
                    slidesToScroll: 7
                }
            }
        ]
    });
}
(function(p,c){
    "use strict";
    p=c.module("$chatFunctions",[]);
    p.factory("$fixedChat",function () {
        var gettingMoreMessageIsInProgress = false;

        function emotes() {
            return [
                {"x": ":) ", "y": "smile"},
                {"x": ":( ", "y": "sad"},
                {"x": ";) ", "y": "wink"},
                {"x": ":D ", "y": "grin"},
                {"x": ":O ", "y": "surprise"},
                {"x": ":$ ", "y": "hasitate"},
                {"x": "(A) ", "y": "angel"},
                {"x": ":'( ", "y": "crying"},
                {"x": ":o) ", "y": "smile-big"},
                {"x": "8) ", "y": "glasses"},
                {"x": ":* ", "y": "kiss"},
                {"x": ":P ", "y": "tongue"},
                {"x": "(inlove) ", "y": "love"},
                {"x": "^_^ ", "y": "cool"},
                {"x": "(facepalm) ", "y": "facepalm"},
                {"x": ";p ", "y": "winktongue"},
                {"x": "(puke) ", "y": "puke"},
                {"x": "o_O ", "y": "what"},
                {"x": "(eat) ", "y": "eat"},
                {"x": "(hello) ", "y": "hello"},
                {"x": "o_o ", "y": "shash"},
                {"x": "|-) ", "y": "sleep"},
                {"x": ":&lt; ", "y": "angry"},
                {"x": "(Y) ", "y": "like"},
                {"x": "(6) ", "y": "devil"},
                {"x": "(cake) ", "y": "cake"},
                {"x": "(M) ", "y": "monkey"}
            ];
        }

        function changeChatUser(userId, friendId, mess_id, userName, flag, $scope,$http, returnResponse){
            document.cookie = "f_id=" + friendId + "; path=/";
            document.cookie = 'u_id=' + userId + "; path=/";
            document.cookie = 'u_name=' + userName + "; path=/";
            $(function () {
                if (flag == false) {
                    $('.messages-wrap ul li').remove();
                }
                $("#fixChatUser-" + friendId).find(".fixChatUserNewMess").remove();
                $('.fixedChatusersList ul').find('.activeConversation').removeClass('activeConversation');
                $('.fixedChatusersList ul').find("#fixChatUser-" + friendId).addClass('activeConversation');
            });
            getChatMessages($scope, $http, flag, returnResponse,true);
        }

        function changeChatMessage($scope,chatMessage, eraze) {
            gettingMoreMessageIsInProgress = false;
            var messages = chatMessage;
            for (var i = 0; i < messages.length; i++) {
                var mess = decodeEmoticons(messages[i].message);
                messages[i].message = mess;
            }

            angular.forEach(messages, function (value, key) {
                if ("string" == typeof value.chatDate.date ){
                    value.date_server = value.chatDate.date;

                    value.chatDate.date = new Date(value.chatDate.date.replace(/-/g, "/").replace(".000000",""));

                }
            });

            messages.sort(function (a, b) {
                if(a.chatDate.date < b.chatDate.date) return -1;
                if(a.chatDate.date > b.chatDate.date) return 1;
                return 0;
            });

            if (eraze) {
                $scope.conversation = messages;
                $(function () {
                    var haight = document.getElementById("message_list").scrollHeight;
                    $('#message_list').scrollTop(haight);
                });
            } else {
                var newMessages = [];
                newMessages = newMessages.concat(messages);
                newMessages = newMessages.concat($scope.conversation);
                $scope.conversation = newMessages;
                var haight = 10;
                $('#message_list').scrollTop(haight);
            }
        }

        function getChatMessages($scope,$http,flag,returnResponse,eraze) {
            if(eraze == true){
                $scope.conversation = [];
            }
            if(gettingMoreMessageIsInProgress == true){
                return;
            }
            var startDate = null;
            if ($scope.conversation && $scope.conversation.length != 0){
                startDate = $scope.conversation[0].date_server;
            }

            $http({
                method: 'POST',
                url: '/message',
                data: {
                    usrname: currentChatUserName(),
                    user_id: currentChatUserId(),
                    friend_id: currentChatFriendId(),
                    flag: flag,
                    start_date: startDate
                }
            }).then(function (response) {
                $(".load_more_loading").css("display","none");
                changeChatMessage($scope,response.data.messages,eraze);
                if (eraze){
                    $scope.current_user = response.data.current_user_id;
                    $scope.friend_id = response.data.friend_id;
                    $scope.friend_username = response.data.f_username;
                    $scope.showFixChat = true;
                }
                $scope.changeMessageCount(response.data.unread_messages_count);
                returnResponse(response);
                gettingMoreMessageIsInProgress = false;
            });
            gettingMoreMessageIsInProgress = true;
        }

        function checkForSelected($scope) {
            var gCookieVal = document.cookie.split("; ");
            var cookies = [3];
            for (var i = 0; i < gCookieVal.length; i++) {
                var item = gCookieVal[i].split("=")
                if (item[0] == 'f_id' || item[0] == 'u_id' || item[0] == 'u_name') {
                    if (item[0] == 'f_id') {
                        cookies[1] = item[1];
                    } else if (item[0] == 'u_id') {
                        cookies[0] = item[1];
                    } else if (item[0] == 'u_name') {
                        cookies[2] = item[1];
                    }
                    cookies[i] = item[1];
                }
            }
            if (cookies.length >= 3) {
                // $scope.user_id= cookies[0];
                $scope.fixedChat(cookies[0], cookies[1], 0, cookies[2], $scope.chatUsers, false);
            }
        }

        function sendMessage(type, img, $scope, $http) {
            var friend_id   = currentChatFriendId();
            if (friend_id == -1){
                return;
            }
            var message = $("#mini-chat-message").val();
            message = message.trim();
            $("#mini-chat-message").val('');

            if (type == 'text') {
                if (message.length > 0 && message != ' ') {
                    $http({
                        method: 'POST',
                        url: '/sendMessage',
                        data: {
                            message: message,
                            friend_id: friend_id,
                            type: type
                        }
                    }).then(function (response) {
                            hideAlert();
                            var haight = document.getElementById("message_list").scrollHeight;
                            $('#message_list').scrollTop(haight);

                            angular.forEach($scope.chatUsers, function (value) {
                                if (value["friend_id"] == currentChatFriendId()){
                                    value["unread_messages_count"] = 0;
                                }
                            });
                            $scope.changeMessageCount(response.data.unread_messages_count);
                        },
                        function(response){
                            hideAlert();
                        });
                }
            } else {
                if (img != '') {
                    $http({
                        method: 'POST',
                        url: '/sendMessage',
                        data: {
                            message: img.path,
                            friend_id: friend_id,
                            type: type
                        }
                    }).then(function (response) {
                            var haight = document.getElementById("message_list").scrollHeight;
                            $('#message_list').scrollTop(haight);
                            hideAlert();
                        },
                        function(response){
                            hideAlert();
                        });
                }
            }
        }

        function changeMessageCount($newValue,$scope,$timeout) {
            $timeout(function () {
                $scope.messageCount = $newValue;
                if ($scope.messageCount == 0){
                    $scope.hideMessageCount = true;
                } else {
                    $scope.hideMessageCount = false;
                }
                $scope.$apply();
            },0);
        }

        function getChatUsers(data,$scope,$http,$timeout) {
            $http({
                method: 'POST',
                url: '/miniChat',
                data: ''
            }).then(function (response) {
                $(function () {
                    var messages = response.data.messages;
                    console.log(messages);
                    if (messages) {
                        for (var i = 0; i < messages.length; i++) {
                            var mess = decodeEmoticons(messages[i].last_message);
                            messages[i].message = mess;
                        }
                    }
                    $scope.chatUsers = messages;
                });

                $scope.changeMessageCount(response.data.unread_message_count);
                if ($scope.messageCount > 0) {
                    $scope.show = true;
                    $scope.disableTagButton = {'visibility': 'visible'};
                }

                if (data == true){
                    $timeout(function () {
                        checkForSelected($scope);
                    },0);

                }
            });
        }

        function decodeEmoticons(html) {
            var emotes = {
                "smile": Array(":-)", ":)", "=]", "=)"),
                "sad": Array(":-(", "=(", ":[", ":("),
                "wink": Array(";-)", ";)", ";]", "*)"),
                "grin": Array(":D", "=D", "XD", "BD", "8D", "xD", ":-D"),
                "surprise": Array(":O", ":0", "=O", ":-O", "=-O"),
                "hasitate": Array(":$"),
                "angel": Array("(A)"),
                "crying": Array(":'(", ":'-(", ":`)"),
                "plain": Array(":|"),
                "smile-big": Array(":o)"),
                "glasses": Array("8)", "8-)"),
                "kiss": Array("(K)", ":-*", ":*", ":K"),
                "tongue": Array(":P", ":p"),
                "monkey": Array("(M)", "(m)"),
                "love": Array("<3", "(inlove)"),
                "cool": Array("^_^"),
                "angry": Array(":<", ":&lt;"),
                "facepalm": Array("(facepalm)"),
                "winktongue": Array(";P", ";p"),
                "puke": Array("(puke)"),
                "what": Array("o.O", "o_O"),
                "eat": Array("(eat)"),
                "hello": Array("(hello)"),
                "shash": Array("o.o", "o_o"),
                "sleep": Array("|-)", "|.)", "(sleep)", "(sleepy)"),
                "cake": Array("(^)", "(cake)"),
                "devil": Array("3:)", "(6)"),
                "like": Array("(Y)", "(y)"),
            };
            for (var emoticon in emotes) {
                for (var i = 0; i < emotes[emoticon].length; i++) {
                    var html = html.replace(emotes[emoticon][i], '<img class="message-emoji" src="../img/emoticons/face-' + emoticon + '.png"/>');
                    if (html.indexOf(emotes[emoticon][i]) >= 0) {
                        html = decodeEmoticons(html);
                    }
                }
            }
            return html;
        }

        function uploadImageSubmit($this,e,$scope,$http){
            showBlockAlert("Uploading image.");
            e.preventDefault();
            var formData = new FormData($this);
            // jQuery('.chat-message-form').append('<div id="loading_image" style="position:absolute;bottom: 25px;left: 43%;">Image is uploading...</div>');
            $.ajax({
                type: 'POST',
                url: "/uploadImage",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.error) {
                        hideAlert();
                        showErrorAlert(data.error);
                    } else {
                        sendMessage("image",data,$scope, $http);
                    }
                },
                error: function (data) {
                    hideAlert();
                    showErrorAlert("Something went wrong. Please try again later.");
                }
            });
        }

        function openImage($image) {
            showModalImage($image.replace("preview.jpg",".jpg"));
        }

        return {
            changeChatMessage: changeChatMessage,
            emotes: emotes,
            changeChatUser: changeChatUser,
            checkForSelected: checkForSelected,
            sendMessage: sendMessage,
            changeMessageCount: changeMessageCount,
            getChatUsers: getChatUsers,
            uploadImageSubmit: uploadImageSubmit,
            getChatMessages:getChatMessages,
            openImage: openImage
        }
    });
})(window,window.angular);

// ------ SEND MESSAGE ----- //
$('#message_list').on('scroll', (function () {
    var scrollTop = $(this).scrollTop();
    if (scrollTop + $(this).innerHeight() >= this.scrollHeight) {
        // $('#message').text('end reached');
    } else if (scrollTop <= 0) {
        // $('#message').text('Top reached');
        $(".load_more_loading").css("display","block");

        angular.element(document.getElementById('imageUploadForm')).scope().getChatMessages();
    } else {
        // $('#message').text('');
    }
}));


function currentChatFriendId(){
    var gCookieVal = document.cookie.split("; ");
    for (var i = 0; i < gCookieVal.length; i++) {
        var item = gCookieVal[i].split("=")
        if (item[0] == 'f_id') {
            return item[1];
        }
    }
    return -1;
}

function currentChatUserId(){
    var gCookieVal = document.cookie.split("; ");
    for (var i = 0; i < gCookieVal.length; i++) {
        var item = gCookieVal[i].split("=")
        if (item[0] == 'u_id') {
            return item[1];
        }
    }
    return -1;
}

function currentChatUserName(){
    var gCookieVal = document.cookie.split("; ");
    for (var i = 0; i < gCookieVal.length; i++) {
        var item = gCookieVal[i].split("=")
        if (item[0] == 'u_name') {
            return item[1];
        }
    }
    return -1;
}

function forceToOpen(){
    var gCookieVal = document.cookie.split("; ");
    for (var i = 0; i < gCookieVal.length; i++) {
        var item = gCookieVal[i].split("=")
        if (item[0] == 'force_to_open') {
            return item[1];
        }
    }
    return false;
}

/*********Upload image *********/
$('.add-photo').click(function () {
    $("#ImageBrowse").click();

});

$("#ImageBrowse").on("change", function () {
    $("#imageUploadForm").submit();
});

$('#imageUploadForm').on('submit', (function (e) {
    angular.element(document.getElementById('imageUploadForm')).scope().sendImage(this,e);
}));

$('textarea#mini-chat-message').keyup(function (e) {
    if (e.keyCode == 13) {
        $('#send-message').click();
    }
});


$("#mini-chat-message").focus(function () {
    markRead();
});

function markRead() {
    var friendID = currentChatFriendId();
    var userId = currentChatUserId();
    if (friendID && userId){
        $.ajax({
            url: "/markRead",
            type:"POST",
            data: {user_id : userId, friend_id : friendID},
            success: function (data) {
                if (data && data.unread_messages_count) {
                    angular.element(document.getElementById('imageUploadForm')).scope().changeMessageCount(data.unread_messages_count);
                }
            },
            error:function(err){
            },
            cache:false,
            dataType:'json'
        });
    }
}



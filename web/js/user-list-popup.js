(function(p,c,a){
    "use strict";
    a = c.module('appYotta');
    a.controller('ShowUserListPopup', ['$scope','$rootScope','$http','$sce','$filter', function ($scope,$rootScope,$http,$sce,$filter) {
        $scope.hidePopup = function () {
            $("#users-list-popup .modal-body").animate({top:"-50%"},300);
            setTimeout(function () {
                $("#users-list-popup").css("visibility","hidden");
            },300)
        };

        $scope.showPopup = function () {
            $("#users-list-popup").css("visibility","visible");
            $("#users-list-popup .modal-body").animate({top:"50%"},300);
        };

        $rootScope.$on('show-user-list', function(event, args) {
            $scope.showPopup();
            $scope.users = [];
            $scope.endpoint = args.endpoint;
            $scope.fullcount = args.fullcount;
            $scope.type = args.type;
            $scope.icon = args.icon;
            $scope.offset = 0;
            $scope.getLikes();
        });

        $scope.getLikes = function () {
            $http({
                method: 'POST',
                url:  $scope.endpoint,
                data: {
                    offset: $scope.offset
                }
            }).then(function successCallback(response) {
                    var newMessages = [];
                    newMessages = newMessages.concat($scope.users);
                    newMessages = newMessages.concat(response.data.users);
                    $scope.users = newMessages;
                    $scope.offset = $scope.users.length;
                    if ($scope.fullcount == $scope.offset){
                        $("#load-more-users").css("visibility","hidden");
                    } else {
                        $("#load-more-users").css("visibility","visible");
                    }

                },
                function errorCallback(response) {
                    showErrorAlert("Something went wrong. Please try again later!");
                });
        }
    }]);

})(window,window.angular);
var yotta = angular.module('appYotta', ['ngRoute', 'ngSanitize', '$chatFunctions','$pubnubModul'] );
yotta.controller('headerController', ['$scope', '$location', '$http','$fixedChat','$pubnub', '$timeout', function ($scope, $location, $http, $fixedChat,$pubnub,$timeout) {
    $scope.x = true;
    $scope.emotes = $fixedChat.emotes();
    $scope.showFixChat = false;

    // /!*******      MINI CHAT   **********!/
    $scope.myFunc = function (data) {
        $fixedChat.getChatUsers(data,$scope,$http,$timeout);
    };

    // /!********  END OF MINI CHAT *********!/

    angular.element(document).ready(function () {
        $scope.myFunc(true);
        $pubnub.configurePubnub(function ($data) {
            var currentFriendId = currentChatFriendId();
            if($data["last_mess"][0]["friendId"] == currentFriendId || $data["last_mess"][0]["userId"] == currentFriendId) {
                $timeout(function () {
                    var newArray = [];
                    newArray = newArray.concat($scope.conversation);
                    newArray.push($data["last_mess"][0]);
                    $fixedChat.changeChatMessage($scope, newArray,true);
                    $scope.$apply();
                }, 0);
            } else {
                $('.chat').find("#" + $data["last_mess"][0]["userId"]).addClass("mess-unread");
                $("#fixChatUser-" +  $data["last_mess"][0]["userId"]).find(".fixChatUserNewMess").remove();
                $("#fixChatUser-" +  $data["last_mess"][0]["userId"]).append("<div class='fixChatUserNewMess'></div>");
            }
            $scope.changeMessageCount($data["unread_messages_count"]);
        });
    });

    /*******      FIXED CHAT   **********/

    $scope.fixedChat = function (userId, friendId, mess_id, userName, flag) {
        $scope.x = true;
        $fixedChat.changeChatUser(userId,friendId,mess_id,userName,flag,$scope,$http,function (response) {
        });
    };

    $scope.changeMessageCount = function($newValue) {
        // $fixedChat.changeMessageCount($newValue,$scope,$timeout);
    };

    $scope.sendMessage = function (type) {
        $fixedChat.sendMessage(type,'',$scope, $http);
        $(".emoticons").slideUp(100);
    };

    $scope.sendImage = function ($this,e) {
        $fixedChat.uploadImageSubmit($this,e,$scope,$http);
    };

    $scope.getChatMessages = function () {
        // $scope,$http,flag,returnResponse,eraze
        $fixedChat.getChatMessages($scope,$http,true,function () {
        },false);
    };

    $scope.chatPopup = function (data) {
        window.close();
        $(".fixedChat", opener.document).css({"display": "block"})
    };

    $scope.openImage = function ($image) {
        $fixedChat.openImage($image);
    };
}]);

$(document).ready(function () {
    /**** Chat Area Js ****/
    $(".message").click(function () {
        $(".chat").slideToggle();
        $('.newMessCount').css({"display": 'none'}).text(0);
        // $('.newMessCount').text(0);
        $('.notification-bar .message #lastSenderId').val('');

    });


    $(".chat .chat-items-wrap").click(function () {
        $('.chat').slideUp();
        var haight = $('.messages-wrap').height() + 350;
        $('.fixedChat').scrollTop(haight);
    });

    //
    // /!*******************emoticons ****************************!/
    $(".smiles").click(function () {
        $(".emoticons").slideToggle("slow");
    });

    $(".emoticons .emote").click(function () {
        var emote = $(this).attr('data-emote');
        var value =  $('#mini-chat-message');
        value.val($(value).val() + emote);
    });

});


(function(p,c,main,a){
    "use strict";
    a = c.module('appYotta');
    a.controller('FriendsController', ['$scope','$http','$sce','$filter', function ($scope,$http,$sce,$filter) {
        /**********Get people may know**********/
        $scope.showFriendsList = false;
        $scope.showPeopleMayNow = true;
        $scope.showSearchFriend = true;
        $scope.getPeoplemayKnow = function () {
            $scope.showFriendsList = true;
            $scope.showSearchFriend = true;
            $scope.showPeopleMayNow = false;
            $http({
                method: 'POST',
                url: 'friends/peopleMayKnow',
                data: {'online_friends':1}
            }).then(function successCallback(response) {
                $scope.peoplesMayKnow = response.data;
            }, function errorCallback(response) {
                // console.log('Can not get people may now');
            });
        };
        $scope.getAllFriends = function () {
            $scope.showFriendsList = false;
            $scope.showPeopleMayNow = true;
            $scope.showSearchFriend = true;
        };
        $scope.findMoreFriend = function () {
            $scope.showFriendsList = true;
            $scope.showPeopleMayNow = true;
            $scope.showSearchFriend = false;
            $scope.searchUserAjax('', 'randomUsers');
        };

        $scope.addToFriendlist = function ($event,id) {
            $.ajax({
                method: "POST",
                url: "/add/" + id,
                data: {
                    template: "addFriend"
                }, success: function (res) {
                    var result = JSON.parse(res);
                    var html = "<a id='addFriend' style='cursor: pointer'>"+ result.message +"</a>"
                    var param = $('.friends-menu>li:nth-of-type(5)').empty();
                    $('.friends-menu>li:nth-of-type(5)').append(html);
                    if(result.coin){
                        showCoins(result.coin, "You Unfriend Friend");
                    }
                }
            });
        };

        /********Search user*********/
        $scope.searchUserText = '';
        $scope.searchUser = function () {
            if ($scope.searchUserText.length >= 3 && $scope.searchUserText.length < 50) {
                $scope.searchUserAjax($scope.searchUserText, 'search');
            } else if ($scope.searchUserText.length < 1) {
                $scope.searchUserAjax('', 'randomUsers');
            }
        };
        $scope.searchUserAjax = function (searchUserText, flag) {
            $http({
                method: 'POST',
                url: '/searchFriend',
                data: {
                    searchText: searchUserText,
                    flag: flag
                }
            }).then(function successCallback(response) {
                if (response.data == '') {
                    $scope.searchUserResults = '';
                } else {
                    $scope.searchUserResults = response.data;
                    // console.log($scope.searchUserResults);
                }
            }, function errorCallback(response) {
                // console.log('Can not search User');
            });
        };

        /**********Load More friends**********/
        $scope.offset = 10;
        $scope.loadMoreFriends = function () {
            $scope.allowLoadMore = false;
            $http({
                method: 'POST',
                url: 'loadMoreFriends',
                data: {
                    "offset":$scope.offset
                }
            }).then(function successCallback(response) {
                var newMessages = [];
                newMessages = newMessages.concat($scope.friends);
                newMessages = newMessages.concat(response.data);
                $scope.friends = newMessages;
                $scope.allowLoadMore = true;
                $scope.offset = $scope.friends.length;
            }, function errorCallback(response) {
                $scope.allowLoadMore = true;
            });
        };

        //Load more by scroll
        $scope.allowLoadMore = true;
        angular.element(main).bind("scroll", function() {
            if ($scope.allowLoadMore == true){
                var height = main.scrollHeight - $(window).height();
                if($(".mainContainer").scrollTop() >= height){
                    $scope.loadMoreFriends();
                }
            }
        });
    }]);

})(window,window.angular,document.getElementById("mainContainer"));
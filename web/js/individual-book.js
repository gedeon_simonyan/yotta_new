(function(p,c,main,a){
    "use strict";
    a = c.module('appYotta');
    a.controller('SocialController', ['$scope','$rootScope','$http','$sce','$filter', function ($scope,$rootScope,$http,$sce,$filter) {
        $scope.likeHover = function () {
            var countOfLikes = $(".likess").html();
            if (countOfLikes > 0){
                $(".likes .tooltiptext").css("visibility","visible");
            }
        };

        $scope.likeLeave = function () {
            $(".likes .tooltiptext").css("visibility","hidden");
        };

        $scope.clickedLikes = function () {
            $(".likes .tooltiptext").css("visibility","hidden");
            var countOfLikes = $(".likess").html();
            if (countOfLikes != 0){
                $rootScope.$emit('show-user-list', {"icon":"/img/icons/heart.png","type":"likes","endpoint":"/getLikes/"+$scope.bookId,"fullcount":countOfLikes});
            }
        };


    //    Lent

        $scope.lentHover = function () {
            $(".lents .tooltiptext").css("visibility","visible");
        };

        $scope.lentLeave = function () {
            $(".lents .tooltiptext").css("visibility","hidden");
        };

        $scope.clickedLent = function (fullCount) {
            $(".lents .tooltiptext").css("visibility","hidden");
            $rootScope.$emit('show-user-list', {"icon":"/img/icons/lent.png","type":"lents","endpoint":"/getLent/"+$scope.bookId,"fullcount":fullCount});
        };
    }]);

})(window,window.angular,document.getElementById("mainContainer"));

$(".result-item-image-tag").click(function () {
    showModalImage($(this).attr('src'))
});
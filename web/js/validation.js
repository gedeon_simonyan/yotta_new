function validateEmail(email) {
    var re = /^[a-z]+[a-z0-9._-]+@[a-z0-9]+\.[a-z.]{1,5}$/;
    return re.test(email.toLowerCase());
}

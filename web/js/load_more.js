$(document).ready(function(){
    startDrawData();
});

function startDrawData() {
    if (!(typeof data === 'undefined')) {
        console.log(data);
        drawTimeline(data);
        var allowScroll = true;
        var offset = data.length;

        $(".mainContainer").scrollTop(0);
        $(".mainContainer").scroll(function(){
            var height = document.getElementById("mainContainer").scrollHeight - $(window).height();
            if($(".mainContainer").scrollTop() >= height){
                if (allowScroll == false) {
                    return
                }
                allowScroll = false;

                $.ajax({
                    url: '/load-more',
                    type: 'GET',
                    data:{
                        'offset':offset,
                        'fromIndividualPage': $(".timelineContent").attr("from_individual"),
                        'individual_id': $(".timelineContent").attr("individual_id")
                    },
                    success:function(data){
                        drawTimeline($.parseJSON(data));
                        offset += 5;
                        allowScroll = true;
                    }
                });
            }
        });
    }
}

function getHour(h,m) {
    result = "";
    if (h < 10){
        result += "0"+h;
    } else {
        result += h;
    }
    if (m < 10){
        result += ":0"+m;
    } else {
        result += ":"+m;
    }
    return result;
}

function drawTimeline(data,beggining) {
    console.log("======");
    console.log(data);
    var now = new Date();
    var today = now.toISOString().slice(0,10);
    var yesterday = new Date(now.getTime() - 24 * 60 * 60 * 1000).toISOString().slice(0,10);
    var recievedData = data;

    for (i = 0; i < recievedData.length; ++i) {
        labelDate = '';
        dataItem = recievedData[i];
        itemDate = new Date(dataItem.post_date.replace(" ","T"));
        if(dataItem.post_date == today){
            labelDate = 'today';
        }else if(dataItem.post_date == yesterday){
            labelDate = 'yesterday';
        }else{
            labelDate = dataItem.post_date;
            itemYear = $.datepicker.formatDate("yy", itemDate);
            currentYear = $.datepicker.formatDate("yy", new Date());
            if(itemYear != currentYear){
                labelDate = $.datepicker.formatDate("M d, yy",itemDate);
            }else{
                labelDate = $.datepicker.formatDate("M d",itemDate);
            }
        }

        if(beggining == true){
            lastArticle = $( ".timelineContent article:first-child" );
        } else {
            lastArticle = $( ".timelineContent article:last-child" );
        }

        container = "";
        if(lastArticle.attr("labelDate") != labelDate){
            console.log(lastArticle.attr("labelDate"));
            console.log(labelDate);
            container += '<article labelDate="'+labelDate+'">' +
                '<div class="art-date">'+
                '<hr/>' +
                '<span class="post-date">'+labelDate+'</span>' +
                '</div> <div></div>';

            container += "</article>";
            if(beggining == true) {
                $('.timelineContent').prepend(container);
            } else {
                $('.timelineContent').append(container);
            }
        }

        lastArticle = $(".timelineContent article:last-child" );

        insideElement = "";
        console.log(dataItem.post_type );
        if (dataItem.post_type == "friend"){
            insideElement += createElementForFriend(dataItem);
        } else if (dataItem.post_type == ""){
            insideElement += createElementForBook(dataItem,itemDate);
        } else if (dataItem.post_type == "markup"){
            if (dataItem.markup_type == "music"){
                insideElement += createElementForMusicMarkup(dataItem);
            } else if (dataItem.markup_type == "comment" || dataItem.markup_type == "wiki"){
                insideElement += createElementForWikiMarkup(dataItem);
            } else if (dataItem.markup_type == "video"){
                insideElement += createElementForVideoMarkup(dataItem);
            } else if (dataItem.markup_type == "picture"){
                insideElement += createElementForImageMarkup(dataItem);
            }
        }
        if(beggining == true) {
            lastArticle.prepend(insideElement)
        } else {
            lastArticle.append(insideElement)
        }
    }
    postItemEach();
    imageModal();
    setupClicks();
    setupTooltip();
}

function receivedNewPostFromPubnub($post) {
    drawTimeline($post,true);
}

function createElementForFriend(dataItem, itemDate) {
    return " <div class=\"friendRequestRow\">" +
    "                     <a href='/user/"+dataItem.friend_user_id+"'>" +
    "                       <div class=\"us-avatar\">"+dataItem.friend_picture+"</div></a>" +
        "                    <div class=\"friendInfo\">" +
    "                            <div>"+dataItem.friend_firstname+"</div>" +
    "                            <div>"+dataItem.friend_lastname+"</div>" +
    "                        </div>" +
    "                        <span>"+dataItem.post_note+"</span>" +
    "                      <a href='/user/"+dataItem.friend_data[0].friend_user_id+"'>" +
    "                         <div class=\"us-avatar right\">"+ dataItem.friend_data[0].friend_picture+"</div> </a>" +
    "                           <div class=\"friendFriendInfo\">" +
        "                        <div>"+dataItem.friend_data[0].friend_firstname+"</div>" +
    "                            <div>"+dataItem.friend_data[0].friend_lastname+"</div>" +
    "                        </div>" +
    "                </div>"+
        "    <div class=\"post-item-footer underline\" style=\"position:relative;\"/>";
}

function createElementForBook(dataItem) {
    insideElement = "<div class=\"post-user\">" +
        "                     <a href='/user/"+dataItem.friend_user_id+"'>" +
        "                    <div class=\"us-avatar\">"+dataItem.friend_picture+"</div></a>" +
        "                    <div class=\"display-author\">" +
        "                        <div class=\"us-display-name\">"+dataItem.friend_firstname+" "+ dataItem.friend_lastname + "</div>" +
        "                        <div class=\"post-time\">" +
        "                            <span>"+dataItem.post_note+"</span>" +
        "                            <span class=\"right\"> / "+getHour(itemDate.getHours(),itemDate.getMinutes())+"</span>" +
        "                        </div>" +
        "                    </div>" +
        "                </div>";
    insideElement += "<div class=\"post-item\" data-post-item-id=\""+dataItem.post_id+"\"" +
        "                     data-post-book-id=\""+dataItem.post_book_id+"\">" +
        "                    <div class=\"post-item-image\">";
    if (dataItem.book_image) {
        insideElement += "<img class=\"bookImage\" src=\""+dataItem.book_image+"\"" +
            "                                 alt=\"book\" title=\"book\"/>";
    } else {
        insideElement += "               <div class=\"post-item-image-block\">\n" +
            "                                <div class=\"post-item-image-text\">IMAGE NOT AVIABLE</div>" +
            "                                <div class=\"post-item-image-wrap\"><img class='' =\"bookImage\"" +
            "                                                                       src=\"/img/logo.png\"" +
            "                                                                       alt=\"book image\"/>" +
            "                                </div>" +
            "                            </div>";
    }
    insideElement += "      </div>" +
        "                    <div class=\"post-item-info\">" +
        "                        <h2 class=\"post-item-title\"><a" +
        "                                    href=\"/book/"+dataItem.post_book_id+"\">"+dataItem.book_title+"</a>" +
        "                        </h2>" +
        "                        <p class=\"book-item-auther\">"+dataItem.book_author+"</p>" +
        "                        <p class=\"book-item-location\">Bloomsbury (UK)</p>";

    if (dataItem.short_date == "0000-00-00") {
        insideElement += "<p class=\"book-item-publish\"></p>";
    } else {
        insideElement += "<p class=\"book-item-publish\"> "+$.datepicker.formatDate("d/m/yy",new Date(dataItem.short_date))+"</p>";
    }
    insideElement += "<div class=\"book-item-rate\">" +
        "                            <span class=\"bookRate\">Book rate: "+dataItem.rate+"&nbsp;</span><span class=\"bookRate\">Your rate: </span>" +
        "                            <div class=\"bookStarsBackground\">" +
        "                                <div class=\"booksRateCount ratecount_"+dataItem.post_book_id+"\" style=\"width:"+(dataItem.my_rate * 20)+"%\">" +
        "                                    <div class=\"bookRate\" id=\"rating-"+dataItem.post_book_id+"_"+dataItem.id+"\"" +
        "                                         data-id=\""+dataItem.post_book_id+"\">" +
        "                                        <input type=\"radio\" id=\"star5-"+dataItem.post_book_id+"_"+dataItem.id+"\"" +
        "                                               name=\"rating-"+dataItem.post_book_id+"_"+dataItem.id+"\" value=\"5\"/><label" +
        "                                                for=\"star5-"+dataItem.post_book_id+"_"+dataItem.id+"\"" +
        "                                                title=\"Rocks!\"></label>" +
        "                                        <input type=\"radio\" id=\"star4-"+dataItem.post_book_id+"_"+dataItem.id+"\"" +
        "                                               name=\"rating-"+dataItem.post_book_id+"_"+dataItem.id+"\" value=\"4\"/><label" +
        "                                                for=\"star4-"+dataItem.post_book_id+"_"+dataItem.id+"\"" +
        "                                                title=\"Rocks!\"></label>" +
        "                                        <input type=\"radio\" id=\"star3-"+dataItem.post_book_id+"_"+dataItem.id+"\"" +
        "                                               name=\"rating-"+dataItem.post_book_id+"_"+dataItem.id+"\" value=\"3\"/><label" +
        "                                                for=\"star3-"+dataItem.post_book_id+"_"+dataItem.id+"\" title=\"Meh\"></label>" +
        "                                        <input type=\"radio\" id=\"star2-"+dataItem.post_book_id+"_"+dataItem.id+"\"" +
        "                                               name=\"rating-"+dataItem.post_book_id+"_"+dataItem.id+"\" value=\"2\"/><label" +
        "                                                for=\"star2-"+dataItem.post_book_id+"_"+dataItem.id+"\"" +
        "                                                title=\"Kinda bad\"></label>" +
        "                                        <input type=\"radio\" id=\"star1-"+dataItem.post_book_id+"_"+dataItem.id+"\"" +
        "                                               name=\"rating-"+dataItem.post_book_id+"_"+dataItem.id+"\" value=\"1\"/><label" +
        "                                                for=\"star1-"+dataItem.post_book_id+"_"+dataItem.id+"\"" +
        "                                                title=\"Sucks big time\"></label>" +
        "                                    </div>" +
        "                                </div>" +
        "                            </div>" +
        "                        </div>" +
        "                    </div>" +
        "                </div>";
    insideElement += "<div class=\"clear\"></div>"+
        "                <div class=\"post-item-footer underline\" style=\"position:relative;\">" +
        "                    <div class=\"icon footer-icon post-vote\">" +
        "                       <a href='#'><img attr_book_id='"+dataItem.post_book_id+"' is_liked='"+dataItem.i_liked_book+"' src=\""+(dataItem.i_liked_book == null ? "/img/icons/dislike.png":"/img/icons/heart.png")+"\" class=\"left post-vote-icon post-vote-icon"+dataItem.post_book_id+"\" /></a>" +
        "                       <a href='#' attr_book_id='"+dataItem.post_book_id+"'  class=\"text  tooltip count-post-book-like_"+dataItem.post_book_id+"\" attr_full_count='"+dataItem.likes_count+"'>" +
        "                       <span class=\"tooltiptext\"></span>";

    insideElement += parseLike(dataItem.likes_count);

    insideElement += "            </a>" +
        "                    </div>" +
        "                    <div class=\"icon footer-icon post-markups\">&#xf0f6;" +
        "                        <span class=\"text markups_click count_markups_result_"+dataItem.post_book_id+"\">";
    if (dataItem.markups_count){
        insideElement += dataItem.markups_count;
    }
    if (dataItem.markups_count){
        if (dataItem.markups_count > 1){
            insideElement += " markups";
        } else {
            insideElement += " markup";
        }
    } else {
        insideElement += "markups";
    }

    insideElement +="      </span>" +
        "                    </div>" +
        "                    <div class=\"icon footer-icon post-share\">\n" +
        "                        <input type=\"checkbox\" hidden class=\"click_checkbox_"+dataItem.post_id+"\"/>" +
        "                        <div class=\"social-links pop_up_show click_popup_show_"+dataItem.post_id+"\">" +
        "                            <a class=\"social-icon icon-gp google\"" +
        "                               href=\"https://plus.google.com/share?hl=en-us&url=http://dev4.wedoapps.eu/book/"+dataItem.post_book_id+"&text="+dataItem.book_title+"\"" +
        "                               onclick=\"javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;\"></a>" +
        "                            <a class=\"social-icon icon-tw twitter twitter-share-button\" target=\"_blank\"" +
        "                               href=\"http://twitter.com/share?&text="+dataItem.book_title+"&hashtags="+dataItem.book_title+"&url=http://web.yotta/timeline\"></a>" +
        "                            <a class=\"social-icon icon-fb facebook share_facebook_button\" bookId='"+dataItem.post_id+"'></a>" +
        "                            <div class=\"clear\"></div>\n" +
        "                        </div>" +
        "                        &#xe804;" +
        "                        <a href='#' class='text'><span class=\"text post_share_click_button_"+dataItem.post_id+"\">share in social</span></a>" +
        "                    </div>" +
        "                    <div class=\"clear\"></div>" +
        "                </div>";
    return insideElement;
}


function createElementForMusicMarkup(dataItem) {
    insideElement =
        "    <div class=\"post-user\">" +
        "        <div class=\"us-avatar\">" +
                        dataItem.friend_picture +
        "        </div>" +
        "        <div class=\"display-author\">" +
        "            <div class=\"us-display-name\">"+dataItem.friend_firstname+" "+ dataItem.friend_lastname +"</div>" +
        "            <div class=\"post-time\">" +
        "                <span>"+dataItem.post_note+" <a href=\"/book/"+dataItem.post_book_id+"\" class='yotta-text-color'>"+dataItem.book_title+"</a></span>" +
        "                <span class=\"right\"> / "+getHour(itemDate.getHours(),itemDate.getMinutes())+"</span>" +
        "            </div>" +
        "        </div>" +
        "    </div>" +
        "    <div class=\"audio-play\">" +
        "        <div class=\"audio-img\">" +
        "            <img src=\""+dataItem.markup_data.music_author_img+"\">" +
        "        </div>" +
        "        <div class=\"audio-desc pl-160 flex-box\">" +
        "            <div class=\"witi-desc\">" +
        "                <h4>"+dataItem.markup_data.title+"</h4>" +
        "                <p class=\"left book-item-location left\">"+dataItem.markup_data.music_author+"</p>" +
        "            </div>" +
        "            <a target='_blank' href=\""+dataItem.markup_data.music_track_url+"\" class=\"social-icon-timeline\">" +
        "                <img src=\"/img/icons/icons-itunes.png\" alt=\"\">" +
        "                <span>see in iTunes</span>" +
        "            </a>" +
        "        </div>" +
        "    </div>"+
        "    <div class=\"post-item-footer underline\" style=\"position:relative;\"/>";
    return insideElement;
}

function createElementForWikiMarkup(dataItem) {
    insideElement =
        "<div class=\"post-user\">" +
        "        <div class=\"us-avatar\">" +
        dataItem.friend_picture +
        "        </div>" +
        "        <div class=\"display-author\">\n" +
        "            <div class=\"us-display-name\">"+dataItem.friend_firstname+" "+ dataItem.friend_lastname +"</div>" +
        "            <div class=\"post-time\">" +
        "                <span>"+dataItem.post_note+" <a href=\"/book/"+dataItem.post_book_id+"\" class='yotta-text-color'>"+dataItem.book_title+"</a></span>" +
        "                <span class=\"right\"> / "+getHour(itemDate.getHours(),itemDate.getMinutes())+"</span>" +
        "            </div>" +
        "        </div>" +
        "    </div>" +
        "    <div class=\"audio-play\">" +
        "        <div class=\"audio-desc flex-box\">" +
        "            <div class=\"witi-desc\">" +
        "                <h4>"+dataItem.markup_data.title+"</h4>" +
        "                <p class=\"left book-item-location left\"> <p class='desc'>"+dataItem.markup_data.markup_desc.substring(0,230)+"</p>";

    if (dataItem.markup_data.markup_desc.length > 230){
        insideElement+="                         <a full_text='"+dataItem.markup_data.markup_desc+"'  class='more' href=\"\">more...</a>";
    }
    insideElement+="                </p>" +
        "            </div>";
    if (dataItem.markup_type != "comment"){
        insideElement += "<a  target='_blank' href=\""+dataItem.markup_data.preview_url+"\" class=\"social-icon-timeline\">" +
            "                <img src=\"/img/icons/icons-wiki.png\" alt=\"\">" +
            "                <span>see in wiki</span>\n" +
            "            </a>";
    }
    insideElement += "        </div>" +
        "    </div>" +
        "    <div class=\"post-item-footer underline\" style=\"position:relative;\"/>";
    return insideElement;
}

function createElementForVideoMarkup(dataItem) {
    insideElement =
        "<div class=\"post-user\">" +
        "    <div class=\"us-avatar\">" +
                dataItem.friend_picture +
        "    </div>" +
        "    <div class=\"display-author\">" +
        "        <div class=\"us-display-name\">"+dataItem.friend_firstname+" "+ dataItem.friend_lastname +"</div>" +
        "        <div class=\"post-time\">" +
        "                <span>"+dataItem.post_note+" <a href=\"/book/"+dataItem.post_book_id+"\" class='yotta-text-color'>"+dataItem.book_title+"</a></span>" +
        "                <span class=\"right\"> / "+getHour(itemDate.getHours(),itemDate.getMinutes())+"</span>" +
        "        </div>" +
        "    </div>" +
        "</div>" +
        "<div class=\"audio-play\">" +
        "    <div class=\"audio-img\">" +
        "        <iframe width=\"250\" src=\"https://www.youtube.com/embed/"+dataItem.markup_data.content+"\" frameborder=\"0\"" +
        "                allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>" +
        "    </div>" +
        "    <div class=\"audio-desc pl-280 flex-box\">" +
        "        <div class=\"witi-desc\">" +
        "            <h4>"+dataItem.markup_data.title+"</h4>" +
        "            <p class=\"left book-item-location left\">"+dataItem.markup_data.markup_desc+"</p>" +
        "        </div>" +
        "        <a target='_blank' href=\"https://www.youtube.com/watch?v="+dataItem.markup_data.content+"\" class=\"social-icon-timeline\">" +
        "            <img src=\"/img/icons/icons-youtube.png\" alt=\"\">" +
        "            <span>see in YouTube</span>" +
        "        </a>" +
        "    </div>" +
        "</div>" +
        "    <div class=\"post-item-footer underline\" style=\"position:relative;\"/>";
    return insideElement;
}


function createElementForImageMarkup(dataItem) {
    insideElement =
        "<div class=\"post-user\">" +
        "        <div class=\"us-avatar\">" +
                    dataItem.friend_picture +
        "        </div>" +
        "        <div class=\"display-author\">" +
        "            <div class=\"us-display-name\">"+dataItem.friend_firstname+" "+ dataItem.friend_lastname +"</div>" +
        "            <div class=\"post-time\">" +
        "                <span>"+dataItem.post_note+" <a href=\"/book/"+dataItem.post_book_id+"\" class='yotta-text-color'>"+dataItem.book_title+"</a></span>" +
        "                <span class=\"right\"> / "+getHour(itemDate.getHours(),itemDate.getMinutes())+"</span>" +
        "            </div>" +
        "        </div>" +
        "    </div>" +

        "    <div class=\"audio-play\">" +
        "        <div class=\"book-img\">" +
        "            <img src=\""+dataItem.markup_data.content+"\" alt=\"\" width=\"250\" height=\"150\">" +
        "        </div>" +
        "        <div class=\"audio-desc pl-280 flex-box\">" +
        "            <div class=\"witi-desc\">" +
        "                <h4>"+dataItem.markup_data.title+"</h4>" +
        "            </div>" +
        "            <a target='_blank' href=\""+dataItem.markup_data.content+"\" class=\"social-icon-timeline\">" +
        "                <img src=\"/img/icons/icons-link.png\" alt=\"\">" +
        "                <span>see in link</span>" +
        "            </a>" +
        "        </div>" +
        "    </div>" +
        "    <div class=\"post-item-footer underline\" style=\"position:relative;\"/>";
    return insideElement;
}


function setupTooltip() {

    $(".post-vote-icon").each(function (key, value) {
        $(this).unbind('click');
        $(this).click(function () {
            likeButtonClicked($(this).attr("attr_book_id"),$(this));
        });
    });

    $(".more").each(function (key, value) {
        $(this).unbind('click');
        $(this).click(function () {
            if ($(this).html() == "less"){
                $(this).html("more...");
                text = $(this).attr("full_text").slice(0,230);
                $(this).parent().find(".desc").html(text);
            } else {
                $(this).html("less")
                fullText = $(this).attr("full_text");
                $(this).parent().find(".desc").html(fullText);
            }
        });
    });

    $('.tooltip').each(function (key, value) {
        $(this).unbind('mouseover');
        $(this).mouseover(function () {
            angular.element(document.getElementsByClassName('timelineContent')[0]).scope().likeHover($( this ).attr("attr_book_id"),$( this ).attr("attr_full_count"),$( this ).find( '.tooltiptext' ));
        });
        $(this).unbind('mouseout');
        $(this).mouseout(function () {
            angular.element(document.getElementsByClassName('timelineContent')[0]).scope().likeLeave($( this ).find( '.tooltiptext' ));
        });
        $(this).unbind('click');
        $(this).click(function () {
            bookId = $( this ).attr("attr_book_id");
            fullCount = $(".count-post-book-like_"+bookId).attr("attr_full_count");
            if (fullCount > 0){
                angular.element(document.getElementsByClassName('timelineContent')[0]).scope().clickedLikes(bookId,$( this ).attr("attr_full_count"),$( this ).find( '.tooltiptext' ));
            }
        });
    });
}

function likeButtonClicked(bookID,_this) {
    url = '/book/' + bookID;
    key = _this.attr("is_liked") == 'null';
    allData = {bookId: bookID, key: key, name: ""};

    fullCount = $(".count-post-book-like_"+bookID).attr("attr_full_count");
    if (fullCount == "undefined"){
        fullCount = 0;
    }

    if (key == true) {
        $(".post-vote-icon"+bookID).attr("src","/img/icons/heart.png");
        $(".post-vote-icon"+bookID).attr("is_liked","em");
        ++fullCount;
    } else {
        $(".post-vote-icon"+bookID).attr("src","/img/icons/dislike.png");
        $(".post-vote-icon"+bookID).attr("is_liked","null");
        --fullCount;
    }
    $(".count-post-book-like_"+bookID).attr("attr_full_count",fullCount);
    $(".count-post-book-like_"+bookID).text(parseLike(fullCount));

    $.ajax({
        url: url,
        type: 'POST',
        data: allData,
        dataType: "json",
        success: function (data) {
            if (data.coin) {
                if(data.coin < 0){
                    showCoins(data.coin,'You dislike the book.')
                } else {
                    showCoins(data.coin,'You like the book')
                }
            }
        }
    });
}

function parseLike(likes_count) {
    insideElement = "";
    if (likes_count){
        insideElement += likes_count;
        if (likes_count > 1){
            insideElement += " likes";
        } else {
            insideElement += " like";
        }
    } else {
        insideElement += "likes";
    }
    return insideElement;
}

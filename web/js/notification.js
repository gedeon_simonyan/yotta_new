$(document).ready(function () {
    var notificationBell = $('.notification').children('.notificationCount').text();

    friendRequestAccept = function (data) {
        var friend_id = data;
        $.ajax({
            type: 'POST',
            url: "/friendRequestAccept",
            data: {
                friend_id: friend_id
            },
            success: function () {
                $(".user" + friend_id).hide();
                var notificationBell = $('.notification-bar').children().children('.notification').children('.notificationCount');
                var count = parseInt(notificationBell.text()) - 1;
                if (count <= 0) {
                    $(notificationBell).css('display', 'none');
                } else {
                    $(notificationBell).text(count);
                }
            },
            error: function () {
                console.log("error");
            }
        });
    };

    friendRequestDecline = function (data) {
        var friend_id = data;
        $.ajax({
            type: 'POST',
            url: "/friendRequestDecline",
            data: {
                friend_id: friend_id
            },
            success: function () {
                $(".user" + friend_id).hide();
                var notificationBell = $('.notification-bar').children().children('.notification').children('.notificationCount');
                var count = parseInt(notificationBell.text()) - 1;
                if (count <= 0) {
                    $(notificationBell).css('display', 'none');
                } else {
                    $(notificationBell).text(count);
                }
            },
            error: function () {
                console.log("error");
            }
        });
    };
    changStatus = function (note_status, id) {
        var x = $(".notification_" + id + " .unnoticed");
        var parentDiv = x.parent('.notificItems').attr('status');
        if (note_status == 0 && parentDiv != 'read') {
            $.ajax({
                type: 'POST',
                url: "/changeNotificationStatus",
                data: {
                    notification_id: id
                },
                success: function (data) {
                    var div = $(".notification_" + id + " .unnoticed");
                    div.css("background", "#ffffff");
                    div.parentsUntil('.notificItems notification' + id).css('background', '#ffffff');
                    var notificationBell = $('.notification-bar').children().children('.notification').children('.notificationCount');
                    var count = parseInt(notificationBell.text());
                    $(notificationBell).text(count - 1);
                    if (notificationBell.text() <= 0) {
                        notificationBell.css('display', 'none');
                    }
                    $(div).parent('.notificItems').attr('status', 'read');

                },
                error: function () {
                    console.log("error");
                }
            });
        }
    };
});

function receivedNewNotificationFromPubnub(data) {
    drowSendNotification(data);
    // var notificationBell = $('.notification-bar').children().children('.notification').children('.notificationCount');
    // var count = parseInt(notificationBell.text());
    // getNotificationCount();

    var getNotification = true;
    $.ajax({
        type: 'post',
        url: '/getNotificationCount',
        data: {
            getNotification: getNotification
        }, success: function (data) {
            var notificationCount = JSON.parse(data);
            if (notificationCount > 0) {
                var selector = $('.notification-bar').children().children('.notification').children('.notificationCount');
                selector.css('display', 'block');
                // var html = "<div class='notificationCount'>"+ notificationCount +"</div>"
                console.log(parseInt(notificationCount));
                selector.text(parseInt(notificationCount))
                // .prepend(html)
            }
        }
    });
}

function drowSendNotification(data) {
    if (data.markup_type === "friend") {
        var friendNotification = "<div class=\"diffNotificItems user"+data.friendId+"\">" +
            "                    <div class=\"friendImage\"><img src=\""+ data.picture +"\" alt=\"image\"></div>" +
            "                    <div class=\"notific\"><span>" + data.firstname + "</span>" +
            "                        <span>" +data.lastname +"</span>"+" "+data.notification+"</div>" +
            "                    <div class=\"diffDate\">" + data.date + "</div>" +
            "                    <div class=\"friendRequest\">" +
            "                        <div class=\"accept\" onclick=\"friendRequestAccept("+data.friendId+"\">Accept</div>" +
            "                        <div class=\"decline\" onclick=\"friendRequestDecline("+data.friendId+")\">Decline</div>" +
            "                    </div>\n" +
            "                </div>";
        // var img = html(user_image);
        $('section .notification').prepend(friendNotification);
    } else {
        var otherNotification = "<div class=\"notificItems notification_"+data.id+"\"  onclick=\"changStatus("+data.note_status+","+data.id+")\"" +
            "                        style=\"background: #EDF2FA\"" +
            "                    <div class=\"friendImage\"><img src=\""+ data.picture +"\" alt=\"image\"></div>" +
            "                    <div class=\"notific\"><span>"+ data.firstname +"</span>" +
            "                        <span>"+data.lastname +"</span>"+data.notification+"</div>" +
            "                    <div class=\"bookTitle\"><a href=\""+ data.bookInfo[0].book_url +"\" target=\"_blank\">"+data.bookInfo[0].title +"</a></div>" +
            "                    <div class=\"date\">"+ data.date +"</div>" +
            "                    <div class=\"unnoticed\"></div>" +
            "                </div>";
        $('section .notification').prepend(otherNotification);
    }
}


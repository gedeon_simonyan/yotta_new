yotta.controller('statisticsController', ['$scope', '$location', '$http', '$timeout', '$interval', function ($scope, $location, $http, $timeout, $interval) {
    $scope.getUserStatistics = function (filter) {
        $scope.menuItems = ['All', 'Today', 'Last Week', 'Last Month'];
        $scope.activeMenu = $scope.menuItems[0];
        $http({
            method: 'POST',
            url: '/statistics',
            headers: {
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            },
            data:{filterBy:filter}
        }).then(function successCallback(response) {
            var resp = angular.fromJson(response);
            var userStatistics = resp.data.userStatisitics.data;
            $scope.putDataInTwig(userStatistics,resp.data.userStatisitics.allCount);
            $scope.progressBar(resp);
            var data = $scope.prepareDataForChart(resp);
            $scope.fushionChart(data);
        }, function errorCallback(response) {
            $scope.error = response.errorMessage;
        });
    };

    $scope.getUserStatistics();
    $scope.putDataInTwig = function (userStatistics, countSumMarkup) {
            $scope.countSumMarkup = null;
            $scope.countMusicMarkup = null;
            $scope.countPictureMarkup = null;
            $scope.countVideoMarkup = null;
            $scope.countReviewMarkup = null;
            if(countSumMarkup){
                $scope.countSumMarkup  = countSumMarkup;
            }

            $scope.type = [];
            angular.forEach(userStatistics, function(value){
                var item = value.type;
                if(item == "music"){
                    $scope.countMusicMarkup = value.counts;
                }
                if(item == "picture"){
                    $scope.countPictureMarkup = value.counts;
                }
                if(item == "video"){
                    $scope.countVideoMarkup = value.counts;
                }
                if(item == "review") {
                    $scope.countReviewMarkup = value.counts;
                }
            });
    };
    $scope.prepareDataForChart = function (response) {
        var item;
        var data = [];
        var count = response.data.allStatistics.data;
        var i = 0;
        var allCount = 0;
        var lastCount = 0;
        var allStatistics = response.data.allStatistics.data;
        angular.forEach(allStatistics, function (value,key) {
            i++;
            if (i != count.length) {
                item = {"value": value.compare.userStatisticsPercent};
                item.displayValue = value.compare.userStatisticsPercent+"%";
                allCount = allCount + parseFloat(value.compare.userStatisticsPercent);
            } else {
                lastCount = 100 - allCount;
                item = {"value": parseFloat(lastCount).toFixed(1)};
                item.displayValue = value.compare.userStatisticsPercent+"%";
            }
            if (value.creator_id == response.data.user_id) {
                item.color = "#19BD9B";
            }
            item.tooltext = value.firstname + " " +
                value.lastname;
            data.push(item);
        });
        return data;
    };

    $scope.progressBar = function (statistics) {
        var userStatisticsPercent = parseFloat(statistics.data.userStatisticsPercent).toFixed(1);
        var friendsStatisticsPercent = parseFloat(statistics.data.friendsStatisticsPercent).toFixed(1);
        if(isNaN(userStatisticsPercent)){
            userStatisticsPercent=0;
        }
        if(isNaN(friendsStatisticsPercent)){
            friendsStatisticsPercent=0;
        }
        $("#compare #friendsDiv>span").text(friendsStatisticsPercent+"%");
        $("#compare #userDiv").text(userStatisticsPercent+"%");
        $("#compare #userDiv").css({"width":userStatisticsPercent+"%"})
        // if(!(userStatisticsPercent > 0) && !(friendsStatisticsPercent > 0)){
        //     $("#compare #friendsDiv>span").text("0%");
        //     $("#compare #userDiv").text("0%");
        //     $("#compare #userDiv").css({"width":"0%"})
        // }
        // else if(userStatisticsPercent > 0 && !(friendsStatisticsPercent > 0)) {
        //     $("#compare #friendsDiv>span").text("0");
        //     $("#compare #userDiv").css({"width":userStatisticsPercent+"%"});
        //     $("#compare #userDiv").text(userStatisticsPercent +" %");
        // }
        // else if(!(userStatisticsPercent > 0) && (friendsStatisticsPercent > 0)){
        //     $("#compare #friendsDiv>span").text(friendsStatisticsPercent);
        //     $("#compare #userDiv").text("");
        //     $("#compare #userDiv").css({"display": "none", "width":"0%"});
        // }
        // else {
        //     $("#compare #friendsDiv>span").text(friendsStatisticsPercent + "%");
        //     $("#compare #userDiv").css({"display": "block","width": userStatisticsPercent+"%"});
        //     $("#compare #userDiv").text(userStatisticsPercent +" %");
        //
        //     $('#chart-container').css("display","block");
        // }

    };
    $scope.setActive = function(menuItem) {
        $scope.activeMenu = menuItem
    };
    $scope.fushionChart = function (data) {
        FusionCharts.ready(function() {
            var medalsChart = new FusionCharts({
                type: 'scrollColumn2d',
                renderAt: 'chart-container',
                width: '100%',
                height: '250',
                dataFormat: 'json',
                dataSource: {
                    "chart": {
                        "animation" : true,
                        "animationDuration" : 1,
                        "showLimits" : true,
                        "showDivLineValues" : true,
                        "showShadow" : "0",
                        "transposeAxis" : "1",
                        "showXAxisLine" : "1",
                        "xAxisLineColor" : "#000000",
                        "xAxisLineThickness" : "1",
                        "showYAxisLine" : "1",
                        "yAxisLineColor" : "#000000",
                        "yAxisLineThickness" : "1",
                        "showYAxisValues" : "1",
                        "yAxisValuesStep" : "2",
                        "rotateYAxisName" : "1",
                        "yAxisNameWidth" : "1",
                        "yAxisMinValue" : "0",
                        "yAxisMaxValue" : "100",
                        "forceYAxisValueDecimals" : "0",
                        "showvalues": "1",
                        "placeValuesInside": "0",
                        "rotateValues": "0",
                        "valueFontColor": "#000",
                        "baseFontColor": "#333333",
                        "paletteColors": "#c7c7c7",
                        "bgcolor": "#FFFFFF",
                        "showalternatehgridcolor": "0",
                        "divlinecolor": "#fff",
                        "showcanvasborder": "0",
                        "linethickness": "0",
                        "plotfillalpha": "100",
                        "plotgradientcolor": "",
                        "divlineThickness": "0",
                        "divLineDashed": "0",
                        "divLineDashLen": "0",
                        "scrollheight": "10",
                        "flatScrollBars": "1",
                        "scrollShowButtons": "0",
                        "scrollColor": "#cccccc",
                        "showHoverEffect": "0",
                        "useRoundEdges" : "0",
                        "showToolTip" : "1"

                    },
                    "categories": [{
                        "showLabel" : "1",
                        "category": data
                    }],
                    "dataset": [{
                        "data": data
                    }]
                }
            });
            medalsChart.render();
        });
    };
}]);





(function(p,c,f){
    "use strict";
    p=c.module("$pubnubModul",[]);
    f = p.factory("$pubnub",function () {

        var pubnub = null;
        var deferred;
        var callback;

        function resendGetPubnubKeyRequest() {
            //if error send getPubnubKeys request again
            setTimeout(function () {
                getPubnubKeys();
            },4000);
        }

        function connectToPubnub(subscribeKey, publishKey,currentUserHash, currentUserid) {
            pubnub = new PubNub({
                publishKey: publishKey,
                subscribeKey: subscribeKey
            });
            pubnub.subscribe({
                channels: ["oo-chat-" + currentUserHash,"notification-" + currentUserHash,"post-"]
            });
            pubnub.addListener({
                message: function (m) {
                    console.log(m.message);
                    if (m.channel === "oo-chat-" + currentUserHash) {
                        drowSendMessage(m.message, currentUserid);
                    } else if (m.channel === "notification-" + currentUserHash) {
                        receivedNewNotificationFromPubnub(m.message);
                    } else if (m.channel === "post-") {
                       receivedNewPostFromPubnub(m.message);
                    }
                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                }
            });
        }

        function configurePubnub($callback) {
            callback = $callback;
            getPubnubKeys();
        }

        function getPubnubKeys() {
            $.ajax({
                url: "/pubnupKeys",
                type:"POST",
                success: function (data) {
                    if (data && data.error) {
                        resendGetPubnubKeyRequest();
                    } else {
                        connectToPubnub(data.sub_key, data.publish_key, data.current_user_hash, data.current_user_id);
                    }
                },
                error:function(err){
                    resendGetPubnubKeyRequest();
                },
                cache:false,
                processData:false,
                contentType:  'application/json; charset=utf-8',
                dataType:'json'
            });
        }

        function drowSendMessage(data, current_user_id) {
            callback(data);
            // var chat_message = data.last_mess[0].message;
            // var last_message = chat_message;
            // var now_date = new Date();
            // now_date = now_date.toISOString().substring(0, 10);
            // var count = $('.notification-bar .message div').text();
            // var hidden_id = $('.notification-bar .message #lastSenderId').val();
            // var type = data.last_mess[0].type;
            // if (type == 'text') {
            //     var message = "<p>" + last_message + "</p>";
            // } else {
            //     var message = "<div class='fixedChatMessageImage'><img src='/" + last_message + "' alt='image' title='image'></div>";
            // }
            // if (data.last_mess[0].userId == current_user_id) {
            //     $('.fixedChatConversation ul').append("<li class='clearfix active ng-scope'><div class='your-message '> " +
            //         "<span  class='mess-date'>" + now_date + "</span><div class='mess-items-" + type + "'>" + message + "</div></div></li>");
            //     $('.chat div').find('#' + data.last_mess[0].friendId + " p").text(last_message);
            //     var haight = $('.messages-wrap').height() + 370;
            //     $('.fixedChat').scrollTop(haight);
            //     var html = $('.chat div').find('#' + data.last_mess[0].friendId + " p").text();
            //     // $('.chat div').find('#' + data.last_mess[0].friendId + " p").text();
            //     //
            //     html = decodeEmoticons(html);
            //     $('.chat div').find('#' + data.last_mess[0].friendId + " p").html(html);
            // } else {
            //     if (data.sender != current_user_id) {
            //         if (hidden_id.indexOf("_" + data.sender) < 0) {
            //             $('.notification-bar .message div').attr("style", "display: block !important;  visibility: visible");
            //             $('.notification-bar .message #lastSenderId').val(hidden_id + '_' + data.sender);
            //         }
            //     }
            //     var active = $('.fixedChatusersList ul').find('.activeConversation');
            //     var active_conv = active.attr('id');
            //     var id = data.last_mess[0].userId;
            //     if (active_conv == 'fixChatUser-' + id) {
            //         $('.fixedChatConversation ul').append("<li class='clearfix active ng-scope'><div class='no-your-message ng-scope '> <span  class='mess-date'>"
            //             + now_date + "</span><div class='image-block'><img src='" + data.last_mess.picture + "'></div><div class='mess-items-" + type + "'>" + message + "</div></div></li>");
            //         $('.chat>div').find("#" + data.last_mess[0].userId + " p").html(last_message);
            //         $('.chat').find("#" + data.last_mess[0].userId).addClass("mess-unread");
            //         var haight = $('.messages-wrap').height() + 360;
            //         $('.fixedChat').scrollTop(haight);
            //     } else {
            //         $("#fixChatUser-" + id).find(".fixChatUserNewMess").remove();
            //         $("#fixChatUser-" + id).append("<div class='fixChatUserNewMess'></div>");
            //         // console.log($("#fixChatUser-" + id));
            //     }
            // }
        }
        return {
            configurePubnub: configurePubnub
        }
    });
})(window,window.angular);



// $.getScript("js/displayResultFromPubnub.js", function() {
//
//
//
//     var pubnub = null;
//     $(document).ready(function () {
//         getPubnubKeys();
//     });
//
//     function getPubnubKeys() {

//     }
//
//     function resendGetPubnubKeyRequest() {
//         //if error send getPubnubKeys request again
//         setTimeout(function () {
//             getPubnubKeys();
//         },4000);
//     }
//
//     function connectToPubnub(subscribeKey, publishKey,currentUserHash, currentUserid) {
//         pubnub = new PubNub({
//             publishKey: publishKey,
//             subscribeKey: subscribeKey
//         });
//         pubnub.subscribe({
//             channels: ["oo-chat-" + currentUserHash,"notification-" + currentUserHash]
//         });
//         pubnub.addListener({
//             message: function (m) {
//                 if (m.channel === "oo-chat-" + currentUserHash) {
//                     drowSendMessage(m.message, currentUserid);
//                 } else if (m.channel === "notification-" + currentUserHash) {
//                     receivedNewNotificationFromPubnub(data);
//                 }
//             },
//             error: function (error) {
//                 console.log(JSON.stringify(error));
//             }
//         });
//     }
// });
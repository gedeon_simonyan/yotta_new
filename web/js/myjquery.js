$(document).ready(function () {
    //handle mouseup event when user clicks anyware on window
    //On this function we hide menu if the user clicks outside of it
    $(document).mouseup(function (e) {
        //getting the menu class from html
        var container = $(".navManu");
        //check if the mouse is not inside the menu
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            //if the above statement is true hide the menu {because user clicked outside of the chat}
            if ($('.nav-bar').hasClass('opened')) {
                $('.nav-bar').addClass('notOpen');
                // e.stopPropagation('.nav-bar');
                hideMenu();
                $('.nav-bar').removeClass('opened')
            } else {
                $('.nav-bar').removeClass('notOpen')
            }
        }

        //get search area object from html
        container = $(".search-area-block");
        //check if the mouse is not inside the search area
        if (!container.is(e.target) && container.css('display') != "none" && container.has(e.target).length === 0) {
            //if the above statement is true hide the search area {because user clicked outside of the chat}
            $('.search-area-block').slideToggle('slow');
        }
    });

    /***Disable document horizontal scroll ***/
    $(window).bind('mousewheel', function (e) {
        if (e.originalEvent.wheelDeltaX != 0) {
            e.preventDefault();
        }
    });
    $(document).keydown(function (e) {
        if (e.keyCode == 37) {
            e.preventDefault();
        }
        if (e.keyCode == 39) {
            e.preventDefault();
        }
    });

    $('.nav-bar').click(function () {
        if (!$('.nav-bar').hasClass('opened') && !$('.nav-bar').hasClass('notOpen')) {
            $(".mainContainer").animate({
                    marginLeft: "+=281"
                }, function () {
                    $(".navManu").css({"display": 'block', 'width': '280px'});
                }
            );
            $('.nav-bar').addClass('opened');
        } else {
            $('.nav-bar').removeClass('notOpen');
        }
    });

    function hideMenu() {
        $(".mainContainer").animate({
            marginLeft: "-=281"
        }, function () {
            // $(".notificationBarMain").css('width', "360px");
            // $(".header-profile ul > li:last-of-type").css("display", 'inline');
        });
        $(".navManu").animate({
            width: "0"
        });
    }

    /*********EDIT PROFILE JS********/
    var x;
    $('.edit-icon').click(function () {
        if ($(this).parent().hasClass('open')) {
            $(this).html('&#xe808;');
            $(this).parent().children('.showed-data').slideUp(400);
            $(this).parent().parent().find('.open').find('.showed-data-bottom-block ').slideUp(400);
            $(this).parent().removeClass('open');
            x = 0;
        } else {

            if ($(this).parent().parent().find('.open')) {
                $(this).parent().parent().find('.open').find('.showed-data').slideUp(400);
                $(this).parent().parent().find('.open').find('.edit-icon').html('&#xe808;');
                $(this).parent().parent().find('.open').find('.showed-data-bottom-block ').slideUp(400);
                $('.open').removeClass('open');
                x = 0;
            }
            $(this).parent().children('.showed-data').slideDown(400);
            $(this).parent().addClass('open');
            $(this).html('&#xe80b;');
        }
    });


    $('#change-password').click(function () {
        if (x == 0) {
            $(this).parent().children('.showed-data-bottom-block').slideDown(400);
            x = 1;
        } else {
            $(this).parent().children('.showed-data-bottom-block').slideUp(400);
            x = 0;
        }
    });

    /*********User friends JS********/

    $("body").delegate(".add-to-friend", "click", function () {
        $(this).text('Request sended');
        $(this).removeClass('add-to-friend');
        $(this).addClass('add-to-friend-request');
    });

    $('.all-friends, .people-may-know, .find-friends').click(function () {
        $(this).parent().find('.active').removeClass('active');
        $(this).addClass('active');
    });

    $('#find-friends').click(function () {
        $('.find-friends-block').css({display: 'block'}).animate({width: '70%'}, 300);
    });


    $('.all-friends').click(function () {
        $('.find-friends-block').animate({width: '0%'}, 300, function () {
            $('.find-friends-block').css({display: 'none'});
        });
    });

    $(".result-item-image-tag").click(function () {
        showModalImage($(this).attr('src'))
    });

    $('.tooltip-items').click(function () {
        var type = $(this).text();
        var book_id = $(this).parent().attr('id');
        var book_description = $(this).parents('.result-item').find('#book_info').attr('data_description');
        var book_img = $(this).parents('.result-item').find('#book_info').attr('data_img');
        var book_publisher = $(this).parents('.result-item').find('#book_info').attr('data_publisher');
        var book_publish_date = $(this).parents('.result-item').find('#book_info').attr('data_publish_date');
        var book_authors = $(this).parents('.result-item').find('#book_info').attr('data_authors');
        var book_price = $(this).parents('.result-item').find('#book_info').attr('data_price');
        var book_book_url = $(this).parents('.result-item').find('#book_info').attr('data_book_url');
        var language = $(this).parents('.result-item').find('.book-item-lenguage').text();
        var book_title = $(this).parents('.result-item').find('.book-item-title').text();

        var book_info = {
            bookId: book_id,
            title: book_title,
            lang: language,
            description: book_description,
            img: book_img,
            publisher: book_publisher,
            publish_date: book_publish_date,
            authors: book_authors,
            price: book_price,
            book_url: book_book_url
        };

        $.post("/addBook", {
            data: {
                type: type,
                book_id: book_id,
                book_info: JSON.stringify(book_info)
            }
        }, function (response) {
            if (response == 'false') {
                console.log('Book status updated');
            } else {
                if (response == "0") {
                    showSuccessAlert("Your book successfully moved to " + type + " list");
                } else {
                    showCoins(response, "YOU ADD BOOK");
                }
            }
        }).fail(function () {
            $('.add-writing').show();
            showErrorAlert('Error while adding the book. Please try again later.');
        });
    });
});




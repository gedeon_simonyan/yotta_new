//Creating copper
var cropper = createCropper();
//Animation time to hide or show the modal
var animationTime = 500;
//Animation time to hide or show the upload button
var animationTimeUploadButton = 200;

$(document).ready(function () {
    //Hide footer from the beginning
    $(".footer_content").css("visibility","hidden");

    $(".modal-close").click(function () {
        hideModal(true)
    });

    $("#edit_prof_picture_button").click(function () {
        $("#edit_prof_picture_file_input").click();
    });

    /**
     * upload profile picture part
     */

    //if the animation is still in progress don't allow another animation come
    var allowAnimatingButton = true;
    /**
     * Hover function for profile picture
     * we should open hide the upload photo button so user can upload a picture
     */
    $("#profile_picture_left").hover(
        function () {
            //hover in function
            if (allowAnimatingButton){
                allowAnimatingButton = false;
                $("#edit_prof_picture_button").animate({bottom:"0"},animationTimeUploadButton,function () {
                    allowAnimatingButton = true;
                });
            }
        },
        function () {
            //hover out function
            if (allowAnimatingButton) {
                allowAnimatingButton = false;
                $("#edit_prof_picture_button").animate({bottom:"-30%"},animationTimeUploadButton,function () {
                    allowAnimatingButton = true;
                });
            }
        }
    )

    /**
     * Detect when the picture has been selected and autosubmit the form
     */
    $(document).delegate('#edit_prof_picture_file_input','change',function () {
        $("#edit_prof_picture_form").submit();
        showModal();
        $("#edit_prof_picture_file_input").val("");
    });

    /**
     * Submit the form with jquery using ajax
     */
    $(document).delegate('#edit_prof_picture_form','submit', function (event) {
        event.preventDefault();
        var formData = new FormData(this);
        console.log(formData);
        $.ajax({
            url: $(this).attr("form_action"),
            type:"POST",
            data: formData,
            success: function (data) {
                if (data.error != null){
                    alert(data.error);
                    hideModal(true);
                } else {
                    //open crop picture
                   openCropPicturePart(data.path,cropper);
                }
            },
            error:function(err){
                if(data.status == 400){
                    alert(data.responseJSON.error);
                } else {
                    alert("Error connecting server.");
                }
                hideModal(true);
            },
            cache:false,
            processData:false,
            contentType: false
        });
    });
});

/**
 * Show modal body
 * @param withAnimation hide body with animation or not
 */
function showModal() {
    $("#edit_prof_picture").css("visibility","visible");
    $(".modal-body").animate({top:"50%"},animationTime);
}

/**
 * Hide modal body
 * @param withAnimation hide body with animation or not
 */
function hideModal(withAnimaton) {
    if (withAnimaton) {
        $(".modal-body").animate({top:"-50%"},animationTime);
        setTimeout(function () {
            $("#edit_prof_picture").css("visibility","hidden");
        },animationTime)
    } else {
        $("#edit_prof_picture").css("visibility","hidden");
    }
}

//CROPPER FUNCTION

var doChangeSliderValue = true;

/**
 *
 */
function createCropper() {
    var image = document.querySelector('#cropper_image');
    var newCropper = new Cropper(image, {
        dragMode: 'move',
        aspectRatio: 1,
        autoCropArea: 1,
        restore: false,
        guides: false,
        center: true,
        highlight: false,
        cropBoxMovable: false,
        cropBoxResizable: false,
        toggleDragModeOnDblclick: false,
        zoomOnWheel: true,
        zoomable: true,
        viewMode:1,
        ready: function (e) {
            setMinValueToSlider(getRatioSize());
        },
        zoom: function (e) {
            if (e.detail.ratio > 1) {
                e.preventDefault();
            }
            if (doChangeSliderValue){
                changeValueOfSlider(e.detail.ratio);
            }
            doChangeSliderValue = true
        }
    });
    return newCropper;
}

function getRatioSize() {
    var data = cropper.getImageData();
    return data.width / data.naturalWidth;
}

/**
 * Open crop picture part
 * @param path the path of image url
 * @param cropper the cropper object
 */
function openCropPicturePart(path, cropper) {
    var getUrl = window.location;
    var url = getUrl .protocol + "//" + getUrl.host + "/" + path;
    cropper.replace(url);
    cropper.reset();

    $(".modal_loading").remove();
    $(".cropper_area").css("display","block");
    $(".footer_content").css("visibility","visible");
}

function sliderValueChanged(value) {
    doChangeSliderValue = false;
    cropper.zoomTo(value);
}

function setMinValueToSlider(value) {
    $(".slider").attr("min",value);
    changeValueOfSlider(value);
}

function changeValueOfSlider(value) {
    $(".slider").val(value);
}

function saveButtonClicked() {
    if (typeof cropper.getCroppedCanvas().toBlob !== "undefined") {
        cropper.getCroppedCanvas().toBlob(function(blob) {
            upoadBlob(blob)
        }, "image/jpeg", 0.75);
    }
    else if (typeof cropper.getCroppedCanvas().msToBlob !== "undefined") {
        var blob = cropper.getCroppedCanvas().msToBlob()
        upoadBlob(blob)
    } else {
    }
}

function upoadBlob(blob) {
    var formData = new FormData();
    formData.append('croppedImage', blob);
    $.ajax({
        url: '/profile/editpicture',
        method: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            if (data.error != null){
                alert(data.error);
            } else if(data.path != null){
                var path = data.path;
                var getUrl = window.location;
                var url = getUrl .protocol + "//" + getUrl.host + "/" + path;
                $(".author-image-block img").attr("src",url);
                $(".avatar img").attr("src",url);
                $(".us-avatar img").attr("src",url);
            } else {
                alert("Something went wrong please try again later.");
            }
            hideModal(true);
            console.log(data);
        },
        error: function (data) {
            console.log(data);
            if(data.status == 400){
                alert(data.responseJSON.error);
            } else {
                alert("Error connecting server.");
            }
        }
    });
}
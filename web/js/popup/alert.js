var timer = null;

function showSuccessAlert(message) {
    $(".YottaAlert").css("border-color", "#19BD9B");
    showAlert(message,true)
}

function showErrorAlert(message) {
    $(".YottaAlert").css("border-color", "#b5342d");
    showAlert(message,true)
}

function showBlockAlert(message) {
    $(".YottaAlert").css("border-color", "#19BD9B");
    showAlert(message,false)
}

function hideAlert() {
    $(".YottaAlert").animate({top:"-100%"},400);
}

function showAlert(message,hideAfter) {
    if (timer != null){
        clearTimeout(timer);
    }
    $( ".YottaAlert" ).stop();
    $(".YottaAlert").text(message);
    $(".YottaAlert").css("visibility","visible");
    $(".YottaAlert").css("top","-100%");
    $(".YottaAlert").animate({top:"0"},400);
    if(hideAfter){
        timer = setTimeout(function () {
            $(".YottaAlert").animate({top:"-100%"},400);
        },3000);
    }
}

var showCoinsTimer = null;

function showCoins(coinsCount,title) {
    if (coinsCount == 0){
        return;
    }
    if (showCoinsTimer != null){
        clearTimeout(showCoinsTimer);
    }
    if (coinsCount < 0){
        coinsCount = -1 * coinsCount;
        $('#add-book-coins-image').attr("src", "/img/sadCoins.png");
        $('.you-earned').text("You loose");
        $('.great').css("visibility","hidden");
    } else {
        $('.great').css("visibility","visible");
        $('.you-earned').text("You earned");
        $('#add-book-coins-image').attr("src" , "/img/coins.png");
    }

    $('.add-book-coins-block h3').text(title);
    $('.add-book-coins-block .coins-count>span').text(coinsCount);
    $('.add-book-coins-block').fadeIn(1000);
    showCoinsTimer = setTimeout(function () {
        $('.add-book-coins-block').fadeOut(1000);
    }, 4000);
}
/********************** IMAGE SHOW HIDE METHODS **********************/

function showModalImage(img) {
    url = "";
    if (img.indexOf("http") >= 0){
        url = img;
    } else {
         url ="/"+img;
    }
    $("#popup_show_img").attr("src",url);
    $(".popup_container").css("visibility","visible");
    $(".image_container").animate({top:"0"},300);

    $(".modal-close").unbind("click");
    $("#hide_popup_div").unbind("click");
    $(".modal-close").click(function () {
        hideModalImage(true)
    });
    setTimeout(function () {
        $("#hide_popup_div").click(function () {
            hideModalImage(true)
        });
    },300);
}

function hideModalImage(withAnimaton) {
    if (withAnimaton) {
        $(".image_container").animate({top:"-100%"},300);
        setTimeout(function () {
            $(".popup_container").css("visibility","hidden");
        },300)
    } else {
        $(".popup_container").css("visibility","hidden");
    }
}
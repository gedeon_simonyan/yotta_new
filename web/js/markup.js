(function(p,c,main,a){
    "use strict";
    a = c.module('appYotta');
    a.controller('AddMusicController', ['$scope','$http','$sce','$filter','$rootScope','$q', function ($scope,$http,$sce,$filter,$rootScope,$q) {
        $rootScope.$on('openMusic', function(event, args) {
            $scope.showMusicMarkup();
        });

        $scope.showMusicMarkup = function () {
            $scope.typeMarkupToSearch = "song";
            $("#add-music").css("visibility","visible");
            $("#add-music .modal-body").animate({top:"50%"},400);
        };

        $scope.hideMusicMarkup = function () {
            $("#search_music_text").val("");
            $scope.musicMarkupSearchResuls = [];
            $("#add-music .modal-body").animate({top:"-100%"},400);
            setTimeout(function () {
                $("#add-music").css("visibility","hidden");
            },400);
        };

        $scope.searchMusicMarkup = function () {
            var text = $("#search_music_text").val();
            if (text == ""){
                return ;
            }
            if ($scope.canceler){
                $scope.canceler.resolve();
            }
            showBlockAlert("Searching Music");
            $scope.canceler = $q.defer();
            $http({
                method: 'GET',
                url:"https://itunes.apple.com/search",
                params: {
                    'entity': $scope.typeMarkupToSearch,
                    'limit':20,
                    'term':text
                }
            }).then(function successCallback(response) {
                    hideAlert();
                    if (response.data.resultCount && response.data.results){
                        $scope.musicMarkupSearchResuls = response.data.results;
                        $("#modal-result-music").scrollTop(0);
                    }
                },
                function errorCallback(response) {
                    hideAlert();
                    if(showAlert){
                        showErrorAlert("Something went wrong. Please try again later!");
                    }
                });
        };

        $scope.searchTextChanged = function (event) {
            if (event.charCode == 13){
                $scope.searchMusicMarkup();
            }
        };

        $scope.addMarkup = function (markup) {
            var sendData = {
                type: "music",
                bookId: $scope.bookId,
                music_genre : markup.primaryGenreName,
                content: markup.previewUrl,
                content_type: "audio",
                title: markup.trackName,
                music_author: markup.artistName,
                music_price: markup.trackPrice,
                music_track_url: markup.trackViewUrl,
                music_author_image: markup.artworkUrl100,
                preview_url: markup.trackViewUrl,

            };
            $rootScope.$emit('send-markup', sendData);
        };
    }]);

    a.controller('AddVideoController', ['$scope','$http','$sce','$filter','$rootScope','$q', function ($scope,$http,$sce,$filter,$rootScope,$q) {
        $rootScope.$on('openVideo', function(event, args) {
            $scope.showVideoMarkup();
        });

        $scope.showVideoMarkup = function () {
            $scope.typeMarkupToSearch = "song";
            $("#add-video").css("visibility","visible");
            $("#add-video .modal-body").animate({top:"50%"},400);
        };

        $scope.hideVideoMarkup = function () {
            $("#search_music_text").val("");
            $scope.musicMarkupSearchResuls = [];
            $("#add-video .modal-body").animate({top:"-100%"},400);
            setTimeout(function () {
                $("#add-video").css("visibility","hidden");
            },400);
        };

        $scope.searchVideoMarkup = function () {
            var text = $("#search_video_text").val();
            if (text == ""){
                return ;
            }
            if ($scope.canceler){
                $scope.canceler.resolve();
            }
            showBlockAlert("Searching Videos");
            $scope.canceler = $q.defer();
            $http({
                method: 'POST',
                url:"/searchVideo",
                data: {
                    q: $("#search_video_text").val()
                }
            }).then(function successCallback(response) {
                console.log(response);
                    hideAlert();
                    if (response.data){
                        if (!response.data.error){
                            $scope.videoMarkupSearchResuls = response.data;
                            $("#modal-result-video").scrollTop(0);
                        }
                    }
                },
                function errorCallback(response) {
                    hideAlert();
                    if(showAlert){
                        showErrorAlert("Something went wrong. Please try again later!");
                    }
                });
        };

        $scope.searchTextChanged = function (event) {
            if (event.charCode == 13){
                $scope.searchVideoMarkup();
            }
        };

        $scope.addMarkup = function (markup) {
            var sendData = {
                type: "video",
                bookId: $scope.bookId,
                music_genre : "",
                content: markup.id.videoId,
                content_type: "video",
                title: markup.snippet.title,
                music_author: markup.snippet.channelTitle,
                music_price: 0,
                music_track_url: "",
                music_author_image: "",
                preview_url: ""
            };
            $rootScope.$emit('send-markup', sendData);
        };

        $scope.generateYoutubeIframe = function (id) {
            return  $sce.trustAsHtml('<iframe width="100%" height="230px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');
        }
    }]);

    a.controller('AddImageController', ['$scope','$http','$sce','$filter','$rootScope','$q', function ($scope,$http,$sce,$filter,$rootScope,$q) {
        $rootScope.$on('openImage', function(event, args) {
            $scope.showImageMarkup();
        });

        $scope.showImageMarkup = function () {
            $scope.typeMarkupToSearch = "song";
            $("#add-image").css("visibility","visible");
            $("#add-image .modal-body").animate({top:"50%"},400);
        };

        $scope.hideImageMarkup = function () {
            $("#search_music_text").val("");
            $scope.musicMarkupSearchResuls = [];
            $("#add-image .modal-body").animate({top:"-100%"},400);
            setTimeout(function () {
                $("#add-image").css("visibility","hidden");
            },400);
        };

        $scope.searchImageMarkup = function () {
            var text = $("#search_image_text").val();
            if (text == ""){
                return ;
            }
            if ($scope.canceler){
                $scope.canceler.resolve();
            }
            showBlockAlert("Searching Images");
            $scope.canceler = $q.defer();
            $http({
                method: 'POST',
                url:"/searchImage",
                data: {
                    q: $("#search_image_text").val()
                }
            }).then(function successCallback(response) {
                    hideAlert();
                    if (response.data){
                        if (!response.data.error){
                            $scope.imageMarkupSearchResuls = response.data;
                            $("#modal-result-image").scrollTop(0);
                        }
                    }
                },
                function errorCallback(response) {
                    hideAlert();
                    if(showAlert){
                        showErrorAlert("Something went wrong. Please try again later!");
                    }
                });
        };

        $scope.searchTextChanged = function (event) {
            if (event.charCode == 13){
                $scope.searchImageMarkup();
            }
        };

        $scope.addMarkup = function (markup) {
            var sendData = {
                type: "picture",
                bookId: $scope.bookId,
                music_genre : "",
                content: markup.link,
                content_type: "picture",
                title: markup.title,
                music_author: "",
                music_price: 0,
                music_track_url: "",
                music_author_image:"",
                preview_url: markup.image.contextLink
            };
            $rootScope.$emit('send-markup', sendData);
        };
    }]);

    a.controller('AddWikiController', ['$scope','$http','$sce','$filter','$rootScope','$q', function ($scope,$http,$sce,$filter,$rootScope,$q) {
        $rootScope.$on('openWiki', function(event, args) {
            $scope.showWikiMarkup();
        });

        $scope.showWikiMarkup = function () {
            $scope.typeMarkupToSearch = "song";
            $("#add-wiki").css("visibility","visible");
            $("#add-wiki .modal-body").animate({top:"50%"},400);
        };

        $scope.hideWikiMarkup = function () {
            $("#search_music_text").val("");
            $scope.musicMarkupSearchResuls = [];
            $("#add-wiki .modal-body").animate({top:"-100%"},400);
            setTimeout(function () {
                $("#add-wiki").css("visibility","hidden");
            },400);
        };

        $scope.searchWikiMarkup = function () {
            var text = $("#search_wiki_text").val();
            if (text == ""){
                return ;
            }
            if ($scope.canceler){
                $scope.canceler.resolve();
            }
            showBlockAlert("Searching Wikis");
            $scope.canceler = $q.defer();
            $http({
                method: 'POST',
                url:"/searchWiki",
                data: {
                    q: $("#search_wiki_text").val()
                }
            }).then(function successCallback(response) {

                    hideAlert();
                    if (response.data){
                        if (!response.data.error){
                            $scope.wikiMarkupSearchResuls = [];
                            for (var j = 0; j < response.data[1].length; ++j){
                                $scope.wikiMarkupSearchResuls[j] = [];
                                var markup = response.data[1][j];
                                $scope.wikiMarkupSearchResuls[j].title = markup;
                            }

                            for (var j = 0; j < response.data[2].length; ++j){
                                var markup = response.data[2][j];
                                $scope.wikiMarkupSearchResuls[j].desc = markup;
                            }

                            for (var j = 0; j < response.data[3].length; ++j){
                                var markup = response.data[3][j];
                                $scope.wikiMarkupSearchResuls[j].url = markup;
                            }
                            $("#modal-result-wiki").scrollTop(0);
                        }
                    }
                },
                function errorCallback(response) {
                    hideAlert();
                    if(showAlert){
                        showErrorAlert("Something went wrong. Please try again later!");
                    }
                });
        };

        $scope.searchTextChanged = function (event) {
            if (event.charCode == 13){
                $scope.searchWikiMarkup();
            }
        };

        $scope.openWikiPart = function () {
            $("#wiki").addClass("active");
            $("#comment").removeClass("active");
            $("#wiki_content").css("display","block");
            $("#comment_content").css("display","none");
        };

        $scope.openCommentPart = function () {
            $("#wiki").removeClass("active");
            $("#comment").addClass("active");
            $("#wiki_content").css("display","none");
            $("#comment_content").css("display","block");
            $scope.wikiMarkupSearchResuls = [];
        };

        $scope.addCommentMarkup = function () {
            var text = $("#add_comment_text").val();
            if (text == ""){
                return;
            }
            var sendData = {
                type: "comment",
                bookId: $scope.bookId,
                music_genre : "",
                content: "",
                markup_desc: text,
                content_type: "comment",
                title: "",
                music_author: "",
                music_price: 0,
                music_track_url: "",
                music_author_image:"",
                preview_url:""
            };
            $rootScope.$emit('send-markup', sendData);
        };

        $scope.addMarkup = function (markup) {
            var sendData = {
                type: "wiki",
                bookId: $scope.bookId,
                music_genre : "",
                content: "",
                markup_desc: markup.desc,
                content_type: "wiki",
                title: markup.title,
                music_author: "",
                music_price: 0,
                music_track_url: "",
                music_author_image:"",
                preview_url: markup.url
            };
            $rootScope.$emit('send-markup', sendData);
        };
    }]);

    a.controller('markupController', ['$scope','$http','$sce','$filter','$rootScope', function ($scope,$http,$sce,$filter,$rootScope) {
        angular.element(document).ready(function () {
            $scope.musicCount = 0;
            $scope.videoCount = 0;
            $scope.photoCount = 0;
            $scope.reviewsCount = 0;
            $scope.lastModifiedDate = new Date();
            $scope.markupType = 'all';
            $scope.getMarkups(false,$scope.bookId,false);
        });

        $scope.getMarkups = function (eraseData,bookId,showAlert) {
            $scope.allowLoadMore = false;
            $http({
                method: 'POST',
                url: "/getMarkup/"+bookId,
                data: {
                    'type': $scope.markupType,
                    'lastDate':($filter('date')($scope.lastModifiedDate, "yyyy-MM-dd HH:mm:ss"))
                }
            }).then(function successCallback(response) {
                    $scope.allowLoadMore = true;
                    var newMessages = [];
                    if (eraseData == false && $scope.markups){
                        newMessages = newMessages.concat($scope.markups);
                    }
                    newMessages = newMessages.concat(response.data.markups);

                    angular.forEach(newMessages, function (value, key) {
                        if ("string" == typeof value.modifyDate.date ){
                            value.modifyDate = new Date(value.modifyDate.date.replace(/-/g, "/").replace(".000000",""));
                        }
                    });

                    $scope.markups = newMessages;

                    if ($scope.markups.length > 0 ){
                        $scope.lastModifiedDate = $scope.markups[$scope.markups.length-1].modifyDate;
                    }

                    var reviewsNewCount = 0;
                    $scope.allMarkupsCount = 0;
                    for (var i = 0; i < response.data.markup_count.length; ++i){
                        var count = response.data.markup_count[i].markupCount;
                        if (response.data.markup_count[i].type == "picture"){
                            $scope.photoCount = count;
                        } else if (response.data.markup_count[i].type == "video"){
                            $scope.videoCount = count;
                        } else if (response.data.markup_count[i].type == "music"){
                            $scope.musicCount = count;
                        } else if (response.data.markup_count[i].type == "comment"){
                            reviewsNewCount += parseInt(count);
                        } else if (response.data.markup_count[i].type == "wiki"){
                            reviewsNewCount += parseInt(count);
                        }
                        $scope.allMarkupsCount += parseInt(count);
                    }

                    var markupString = "markup";
                    if ($scope.allMarkupsCount > 1){
                        markupString = "markups";
                    }
                    $(".markups").html($scope.allMarkupsCount+" "+markupString);
                    $scope.reviewsCount = reviewsNewCount;
                },
                function errorCallback(response) {
                    if(showAlert){
                        showErrorAlert("Something went wrong. Please try again later!");
                    }
                    $scope.allowLoadMore = true;
                });
        };

        $scope.changeMarkupType = function($event,type) {
            $('.markup-buttons .active').removeClass("active");
            if ($event != null){
                angular.element($event.target).parent().addClass("active");
            }
            $scope.lastModifiedDate = new Date();
            $scope.markupType = type;
            $scope.getMarkups(true,$scope.bookId,true);
        };

        //Load more by scroll
        $scope.allowLoadMore = true;
        angular.element(main).bind("scroll", function() {
            if ($scope.allowLoadMore == true){
                var height = main.scrollHeight - $(window).height();
                if($(".mainContainer").scrollTop() >= height){
                    $scope.getMarkups(false,$scope.bookId,true);
                }
            }
        });


    //    OPEN SEARCH MARKUPS
        $scope.openMusic = function () {
            $rootScope.$emit('openMusic', {});
        };

        $scope.openVideo = function () {
            $rootScope.$emit('openVideo', {});
        };

        $scope.openImage = function () {
            $rootScope.$emit('openImage', {});
        };

        $scope.openWiki = function () {
            $rootScope.$emit('openWiki', {});
        };

        $rootScope.$on('send-markup', function(event, args) {
            showBlockAlert("Adding makup to book.")
            $http({
                method: 'POST',
                url:"addMarkup",
                data: args
            }).then(function successCallback(response) {
                    $("#add_comment_text").val("");
                    hideAlert();
                    if(response.data.message == "fail"){
                        showErrorAlert("Something went wrong. Please try again later!");
                    } else {
                        showSuccessAlert("Markup added successfully.")
                    }
                    setTimeout(function () {
                        $scope.changeMarkupType(null,$scope.markupType);
                    },1000);
                },
                function errorCallback(response) {
                    hideAlert();
                    showErrorAlert("Something went wrong. Please try again later!");
                });
        });

        $scope.generateYoutubeIframe = function (id) {
            return  $sce.trustAsHtml('<iframe width="100%" height="250" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');
        };

        $scope.showDesc = function (fullText,id) {
            if ( $("#more_"+id).html() == "less"){
                $("#desc_"+id).html(fullText.slice(0,230));
                $("#more_"+id).html("more...");
            } else {
                $("#desc_"+id).html(fullText);
                $("#more_"+id).html("less");
            }
        };

        $scope.deleteMarkup = function (markup) {
            showBlockAlert("Deleting markup")
            $http({
                method: 'POST',
                url:"/deleteMarkup",
                data: {
                    markup_id: markup.id
                }
            }).then(function successCallback(response) {
                    hideAlert();
                    if(response.data.message == "fail"){
                        showErrorAlert("Something went wrong. Please try again later!");
                    } else {
                        showSuccessAlert("Markup successfully deleted");
                        $scope.removeMarkup(markup)
                    }
                },
                function errorCallback(response) {
                    hideAlert();
                    showErrorAlert("Something went wrong. Please try again later!");
                });
        };

        $scope.removeMarkup = function (makup) {
          for (var i = 0; i < $scope.markups.length; ++i){
              if ($scope.markups[i].id == makup.id){
                  $scope.markups.splice(i,1);
                  break;
              }
          }
        };

        $scope.getRightProfilePicture = function (pic) {
            if (pic){
                if (pic.indexOf("http") != -1){
                    return pic;
                }
                return "/"+pic;
            }
            return '/img/defaultUserImg.png';
        };
    }]);

})(window,window.angular,document.getElementById("mainContainer"));
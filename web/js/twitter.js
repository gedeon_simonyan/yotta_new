'use strict';
/**
 * @file twitter.js
 * Performs twitter OAuth login using OAuth.js
 * @author Ashot Harutyunyan
 * @date 06.08.2016
 */

/**
 * @ngdoc service
 * @name App.service:Twitter
 * @description Performs twitter OAuth login using OAuth.js
 * @requires $q
 * */
angular.module('appYotta').factory('Twitter', ['$q', function ($q) {
  return {
    /**
     * @ngdoc method
     * @name App.service:Twitter#initialize
     * @param {string} key oauth app public key
     * @description initialize oauth app
     * */
    initialize: function (key) {
      OAuth.initialize(key, {cache: true});
    },
    /**
     * @ngdoc method
     * @name App.service:Twitter#connectTwitter
     * @returns {object} a promise
     * @description performs login into twitter app
     * */
    connectTwitter: function () {
      var deferred = $q.defer();
      OAuth.popup('twitter', {cache: true}, function (error, result) {
        if (!error) {
          deferred.resolve(result);
        } else {
          deferred.reject(error);
        }
      });
      return deferred.promise;
    }
  };

}]);

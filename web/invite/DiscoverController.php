<?php
/**
 * Created by PhpStorm.
 * User: Hovo
 * Date: 5/29/2017
 * Time: 11:04 AM
 */

namespace YottaBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use YottaBundle\YottaBundle;

class DiscoverController extends Controller
{
    /**
     * @Route("/discover")
     */
    public function timelineAction()
    {
        $topRatedBooks = $this->topRatedBooks();
        $lastRatedBooks = $this->lastRatedBooks();
        $yotters = $this->getYottes();

        foreach ($yotters as $key => $value) {
            $user_img = $yotters[$key]['picture'];
            $image = $this->get('yotta.repository.user')->generateCorrectProfilePicture($user_img, false);
            $yottersImg[$key]['picture'] = $image;
            $yottersImg[$key]['id'] = $yotters[$key]['user_id'];
        }

        $topBooksResult = array();
        foreach ($topRatedBooks as $key => $value) {
            $topBooksResult[$key]['data'] = $this->getBook($value['book_id']);
            $topBooksResult[$key]['rate'] = $value['rate'];
            $topBooksResult[$key]['b_id'] = $value['book_id'];
        }

        $lastBooksResult = array();
        foreach ($lastRatedBooks as $key => $value) {
            $lastBooksResult[$key]['data'] = $this->getBook($value['book_id']);
            if(!$this->getBook($value['book_id'])){

            }
            $lastBooksResult[$key]['rate'] = $value['rate'];
            $lastBooksResult[$key]['b_id'] = $value['book_id'];

        }


        return $this->render('@Yotta/templates/timeline.html.twig',
            array("topratedBook" => $topBooksResult, "lastRatedBooks" => $lastBooksResult, "yotters" => $yottersImg));
    }

    private function topRatedBooks()
    {
        $em = YottaBundle::getContainer()->get('doctrine')->getManager();
        $con = $em->getConnection()
            ->prepare(
                'SELECT  `book_id` ,AVG(`stars`) AS rate FROM  `rating` GROUP BY `book_id` ORDER BY rate DESC LIMIT 20'
            );
        $con->execute();
        $result = $con->fetchAll();

        return $result;
    }

    private function getBook($book_id)
    {
        $em = $this->getDoctrine()->getManager()
            ->createQueryBuilder()
            ->select('b.title', 'b.authors', 'b.publisher', 'b.image', 'b.likes', 'b.modified', 'b.id')
            ->from('YottaBundle:Books', 'b')
            ->where('b.id = :param')
            ->setParameter('param', $book_id)
            ->getQuery();
        $query = $em->getArrayResult();
        foreach ($query as $key => $value){
            $query[$key]['image'] = $this->get('yotta.repository.user')->generateCorrectBookPicture($query[$key]['image'], false);
        }
        return $query[0];
    }

    private function lastRatedBooks()
    {
        $em = YottaBundle::getContainer()->get('doctrine')->getManager();
        $con = $em->getConnection()
            ->prepare(
                'SELECT  `book_id` ,AVG(`stars`) AS rate FROM  `rating` GROUP BY `book_id` ORDER BY `id` DESC LIMIT 20'
            );
        $con->execute();
        $result = $con->fetchAll();
        return $result;
    }

    public function getYottes()
    {
        $em = YottaBundle::getContainer()->get('doctrine')->getManager();
        $con = $em->getConnection()
            ->prepare(
                'SELECT coins.`user_id`, SUM(coins.`coin`) AS rate, user_info.`picture`  FROM coins 
INNER JOIN `user_info` WHERE user_info.`user_id` = coins.`user_id`
GROUP BY coins.`user_id` ORDER BY rate DESC LIMIT 20');
        $con->execute();
        $result = $con->fetchAll();
        return $result;
    }

    /**
     * @Route("/getYottersInfo")
     */
    public function getYottersInfo(Request $request){

        $user_id = isset($_POST['id']) ? $_POST['id'] : '';
        $em = YottaBundle::getContainer()->get('doctrine')->getManager();
        $con = $em->getConnection()
            ->prepare(
                "SELECT user_info.`picture`,user_info.`firstname`, user_info.`lastname`, users.`username`, SUM(coins.`coin`) AS rate FROM user_info
INNER JOIN users 
INNER JOIN coins
WHERE (user_info.`user_id`=". $user_id. ") AND (users.`id` = user_info.`user_id`) AND (coins.`user_id` = user_info.`user_id`) "
            );
        $con->execute();
        $result = $con->fetchAll();
        return new Response(json_encode($result));
    }
}
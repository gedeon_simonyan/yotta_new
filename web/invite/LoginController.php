<?php

namespace YottaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use TwitterOAuth;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use YottaBundle\YottaBundle;

class LoginController extends Controller
{
    private $CollbackUrl = 'http://dev4.wedoapps.eu';
    private $twitter_consumer_key = 'rgvCfl3g6vULJL5RFHfRMYSwZ';
    private $twitter_conumer_secret = 'zGYx15qZOCvNlmh5YF9xEOvjtYLnYQF8wTYXVkui5uEhm8Gtqi';
    private static $requestToken = '';
    private static $requestTokenSecret = '';

    /**
     * @Route("/OAuthTw")
     */

    public function OAuthTwAction(Request $request)
    {
        $session = $request->getSession();
        if ($session->isStarted()) {
            require_once(__DIR__ . '/../../../web/lib/twitteroauth/twitteroauth.php');
            $twitteroauth = new TwitterOAuth($this->twitter_consumer_key, $this->twitter_conumer_secret,
                $session->get('request_token'), $session->get('request_token_secret'));

            $access_token = $twitteroauth->getAccessToken($_GET['oauth_verifier']);
            $session->set('access_token', $access_token);

            $session->set('tw_access_key', $twitteroauth->token->key);
            $session->set('tw_access_secret', $twitteroauth->token->secret);

            $user_info = $twitteroauth->get('account/verify_credentials');
            $u_name = explode(' ', $user_info->name);
            $user_data = array(
                'access_token' => $access_token['oauth_token'],
                'tw_id' => $user_info->id,
                'fb_id' => '',
                'gp_id' => '',
                'email' => '',
                'f_name' => $u_name[0],
                'l_name' => $u_name[1],
                'username' => $user_info->screen_name,
                'avatar' => $user_info->profile_image_url
            );

            $u_id = $this->chackUserTwitterId($user_info->id);
            if ($u_id) {
                $session->set('user_id', $u_id);
            } else {
                $u_id = $this->addUser($user_data);
                $session->set('user_id', $u_id);
                setcookie('__yotta_tw_share', '1', time() + 3600, '/');
            }
            return new RedirectResponse('/profile');
//            return $this->render('YottaBundle:Default:layout.html.twig', array('tw_login_error' => 'you alredy registred with twitter'));
        }

//                $publishKey = $config['publish_key'];
//                $subscribeKey = $config['subscribe_key'];
//
//                $pubnub = new Pubnub(array(
//                    'publish_key' => $publishKey,
//                    'subscribe_key' => $subscribeKey,
//                ));
//
//                $pubnub->publish(array(
//                    'channel' 	=> 'statusChannel',
//                    'message' 	=> array("user_id" => $result['id'],"user"=>$result, "status" => 'online' )
//                ));
    }

    /**
     * @Route("/twitter")
     */
    public function twitter(Request $request)
    {
        require_once(__DIR__ . '/../../../web/lib/twitteroauth/twitteroauth.php');
        $connection = new TwitterOAuth($this->twitter_consumer_key, $this->twitter_conumer_secret);
        $request_token = $connection->getRequestToken($this->CollbackUrl . "/OAuthTw"); //get Request Token
        if ($request_token) {
            $session = $request->getSession();
            if (!$session->isStarted()) {
                $session = new Session();
                $session->start();
            }
            $session->set("request_token", $request_token['oauth_token']);
            $token = $request_token['oauth_token'];
            $session->set('request_token', $token);
            $session->set('request_token_secret', $request_token['oauth_token_secret']);

            switch ($connection->http_code) {
                case 200:
                    $url = $connection->getAuthorizeURL($token);
                    return new RedirectResponse($url);
                    break;
                default:
                    var_dump("Error Receiving Request Token");
                    break;
            }
        } else {
            var_dump("Error Receiving Request Token");
        }
    }

    /**
     * @Route("/share_in_tw")
     */
    public function share_in_tw(Request $request)
    {
        require_once(__DIR__ . '/../../../web/lib/twitteroauth/twitteroauth.php');

        $data = $request->getContent("data");

        $session = $request->getSession();
        $tw_key = $session->get('tw_access_key');
        $tw_secret = $session->get('tw_access_secret');

        $twitteroauth = new TwitterOAuth($this->twitter_consumer_key, $this->twitter_conumer_secret, $tw_key, $tw_secret);
        $tweet = "I am alredy in yottaBook!!   http://dev4.wedoapps.eu";

        if ($data == 'tweet') {
            $post = $twitteroauth->post('statuses/update', array('status' => $tweet));
        }
        $return = $twitteroauth->get('followers/list', array('screen_name' => 'YevgineEv'));

        return new Response(json_encode($return));
    }


    /**
     * @Route("/login_social")
     */
    public function login_socialAction(Request $request)
    {
        $data = json_decode(file_get_contents("php://input"));
        $uid = $data->uid;
        $social = $data->social;
        $first_name = isset($data->first_name) ? addslashes($data->first_name) : '';
        $last_name = isset($data->last_name) ? addslashes($data->last_name) : '';
        $avatar = isset($data->avatar) ? addslashes($data->avatar) : '';
        $email = isset($data->email) ? addslashes($data->email) : '';
        $accessToken = addslashes($data->accessToken);
        $error['error'] = ' ';
        $user_data = array(
            'access_token' => $accessToken,
            'tw_id' => '',
            'fb_id' => '',
            'gp_id' => '',
            'email' => $email,
            'f_name' => $first_name,
            'l_name' => $last_name,
            'username' => '',
            'avatar' => $avatar
        );
        if (!empty($data)) {
            switch ($social) {
                case "fb":
                    $user_id = $this->chackUserFacebookId($uid);
                    if (!$user_id) {
                        $user_data['fb_id'] = $uid;
                        $user_id = $this->addUser($user_data);
                        $sharing['share'] = 'fb';
                        setcookie('__yotta_fb_share', '1', time() + 3600, '/');
                    }
                    break;
                case "gp":
                    $user_id = $this->chackUserGooglePluskId($uid);
                    if (!$user_id) {
                        $user_data['gp_id'] = $uid;
                        $user_id = $this->addUser($user_data);
                        $sharing['share'] = 'gp';
                        setcookie('__yotta_gp_share', '1');
                    }
                    break;
            }
            if ($user_id) {
                if (!$request->getSession()->isStarted()) {
                    $session = new Session();
                    $session->start();
                    $session->set('user_id', $user_id);
                } else {
                    $session = $request->getSession();
                    $session->set('user_id', $user_id);
                }
                if ($sharing) {
                    return new Response(json_encode($sharing));
                } else {
                    $this->profileAction($request);
                    return new Response(json_encode('ok'));
                }


            } else {
                $error['error'] = 'error login';
                return new Response(json_encode($error));
            }
//        else{
//            $error['error'] = 'error login';
////            return new Response(json_encode($error));
//        }


//                if ($request->getSession()->get('user_id')) {
//
////                return $this->render('YottaBundle:Default:profile.html.twig');
//                } else {
//
//                }

        }
//        return $this->render('YottaBundle:Default:layout.html.twig');
    }

    /**
     * @Route("/social_friends")
     */
    public function socialFriendsAction(Request $request)
    {
        $data = $request->getContent("data");
        $data = json_decode($data, true);

        $social_friends = $data['friends'];
        $social = $data['social'];

        $session = $request->getSession();
        if ($session->isStarted() && $session->get('user_id')) {
            $u_id = $session->get('user_id');
            $fr_array = array();

            switch ($social) {
                case 'fb':
                    foreach ($social_friends as $k => $friend) {
                        $f_id = $this->chackUserFacebookId($friend["id"]);
                        if ($f_id) {
                            $frendship = $this->chackUserFriendByID($u_id, $f_id);
                            if (empty($frendship)) {
                                $fr_array[$k]['id'] = $f_id;
                                $fr_array[$k]['f_name'] = $friend["first_name"];
                                $fr_array[$k]['l_name'] = $friend ["last_name"];
                                $fr_array[$k]['picture'] = $friend["picture"]["data"] ["url"];
                            }
                        }
                    }
                    break;
                case 'tw':
                    $social_friends = json_decode($social_friends, true);

                    foreach ($social_friends["users"] as $k => $friend) {
                        $f_id = $this->chackUserTwitterId($friend["id"]);
                        if ($f_id) {
                            $frendship = $this->chackUserFriendByID($u_id, $f_id);
                            if (empty($frendship)) {
                                $name = explode(' ', $friend["name"]);
                                $fr_array[$k]['id'] = $f_id;
                                $fr_array[$k]['f_name'] = $name[0] ? $name[0] : $friend['screen_name'];
                                $fr_array[$k]['l_name'] = $name[1] ? $name[1] : '';
                                $fr_array[$k]['picture'] = $friend["profile_image_url_https"];
                            }
                        }
                    }
                    break;
            }

            return new Response(json_encode($fr_array));
        }
        return $this->render('YottaBundle:Default:layout.html.twig');
    }


    /**
     * @Route("/login",name="login")
     */
    public function loginAction(Request $request)
    {
        $data = json_decode(file_get_contents("php://input"));
        if (!empty($data)) {
            $name = isset($data->name) ? addslashes($data->name) : "";
            $password = isset($data->password) ? $this->validatePassword($data->password) : "";
            if ($password) {
                $password = sha1($data->password);
            }
            if (strlen($name) >= 6 && strlen($password) >= 6) {
                $em = $this->getDoctrine()->getManager()
                    ->getConnection()
                    ->prepare(
                        'SELECT `id` FROM `users` WHERE  `username`=:name AND `password`=:password'
                    );
                $em->bindValue('name', $name);
                $em->bindValue('password', $password);
                $em->execute();
                $user['id'] = $em->fetchAll();
//
                if (isset($user["id"][0]['id'])) {
                    if (!$request->getSession()->isStarted()) {
                        $session = new Session();
                        $session->start();
                        $id = $user['id'][0]['id'];
                        $session->set('user_id', $id);
//                        $this->profileAction($request);
//                        return new RedirectResponse("/profile");
                    }
                } else {
                    $user['error'] = 'Incorrect Login or Password';
                    setcookie('PHPSESSID', 'value', time() - 1);
                    if ($request->getSession()->isStarted()) {
                        $request->getSession()->remove('user_id');
                    }
//                        return $this->render('YottaBundle:Default:layout.html.twig', array("isconn" => $request->getSession()->getId()));
//                    return new Response(json_encode($user));
                }
                return new Response(json_encode($user));
            }
        }

        if ($request->getSession()->get('user_id')) {
//            $this->profileAction($request);
            return new RedirectResponse("/profile");
        } else {
            return new RedirectResponse("/");
//            return $this->render('YottaBundle:Default:layout.html.twig', array("isconn" => $request->getSession()->getId()));
        }
    }


    /**
     * @Route("/logout")
     */

    public function logoutAction(Request $request)
    {
        setcookie('PHPSESSID', 'value', time() - 1);

        if ($request->getSession()->isStarted()) {
//            return new RedirectResponse("/");
            $this->deleteCookie();
            $request->getSession()->remove('user_id');
            return $this->render('YottaBundle:Default:layout.html.twig', array("isconn" => $request->getSession()->getId()));
        } else
//            return $this->render('YottaBundle:templates:profile.html.twig', array('ID' => $request->getSession()->getId()));
            return $this->render('YottaBundle:Default:layout.html.twig', array("isconn" => $request->getSession()->getId()));
//            return new RedirectResponse("/");
    }

    /**
     * @Route("/registration")
     */
    public function registrationAction(Request $request)
    {
        $errors = array();
        $data = json_decode(file_get_contents("php://input"));
        if (!empty($data)) {
            $name = addslashes($data->name);
            $email = $data->email;
            $password = $this->validatePassword($data->password);
            if ($password) {
                $password = sha1($data->password);
            } else {
                $errors['password'] = 'password can be more 6 symboles';
            }
            $repository = $this->getDoctrine()->getManager();
            $query1 = $repository->createQueryBuilder()
                ->select('u.id')
                ->from('YottaBundle:Users', 'u')
                ->where('u.email = :email')
                ->setParameter(':email', $email)
                ->getQuery();
            $res = $query1->getResult();
            $user['email'] = isset($res[0]['id']) ? $res[0]['id'] : '';

            $query2 = $repository->createQueryBuilder()
                ->select('u.id')
                ->from('YottaBundle:Users', 'u')
                ->where('u.username = :u_name')
                ->setParameter(':u_name', $name)
                ->getQuery();
            $res = $query2->getResult();
            $user['username'] = isset($res[0]['id']) ? $res[0]['id'] : '';

            $hash = sha1('user' . $name . 'yotta');
            if (empty($user['email']) && empty($user['username'])) {
                if (strlen($name) >= 6 && strlen($email) > 0 && strlen($password) >= 6) {
                    $repository = $this->getDoctrine()->getManager()->getConnection();
                    $query = $repository->prepare('INSERT INTO `users` (`username`,`password`,`email`,`created`,`status`,`modified`,`is_active`,`hash`,`profile`,`browser`)
                                                  values(:u_name, :pass, :email, :c_date, :status, :m_date,:is_active,:hash, :profile, :borwser)');
                    $query->bindValue('u_name', $name);
                    $query->bindValue('pass', $password);
                    $query->bindValue('email', $email);
                    $query->bindValue('c_date', date("Y-m-d H:i:s"));
                    $query->bindValue('status', "offline");
                    $query->bindValue('m_date', date("Y-m-d H:i:s"));
                    $query->bindValue('is_active', 'no');
                    $query->bindValue('hash', $hash);
                    $query->bindValue('profile', "user");
                    $query->bindValue('borwser', 'web');
                    $query->execute();
                    $user_id = $repository->lastInsertId();

                    $repository = $this->getDoctrine()->getManager()->getConnection();
                    $query2 = $repository->prepare('INSERT INTO `user_info` (`forum_visible`,`user_id`) values(:form_vis, :u_id)');
                    $query2->bindValue('form_vis', "all");
                    $query2->bindValue('u_id', $user_id);
                    $query2->execute();

                    $subject = "Confirmation Email for YottaBook";
                    $http = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://';
                    $url = $http . $_SERVER['SERVER_NAME'] . '/activate_account?u=' . $user_id . '&h=' . $hash;
                    $confirmation_link = '<a href="' . $url . '">Activate Account</a>';
                    $maildata = array(
                        "subject" => $subject,
                        "from" => "ev.shahinyan@gmail.com",
                        "to" => $email,
                        "message" => $confirmation_link,
                        "name" => $name
                    );
                    $this->sendMail($maildata);
//                    die();
                    return new Response(json_encode('ok'));
                }

            } else {
                if (!empty($user['username'])) {
                    $errors['err'] = 'This username alredy exist';
                }
                if (!empty($user['email'])) {
                    $errors['err'] = 'This email alredy exist';
                }
                return new Response(json_encode($errors));
            }

            if ($request->getSession()->get('user_id')) {
                $this->profileAction($request);
                return new RedirectResponse("/profile");
            }

        } else {
            return $this->render('YottaBundle:templates:registration.html.twig');
        }
    }


    /**
     * @Route("/profile",name="profile")
     */
    public function profileAction(Request $request)
    {
        $session = $request->getSession();

        if ($session->isStarted() && $session->get('user_id')) {
            $user_id = $session->get('user_id');
            $user_data = $this->getUserData($user_id);
            $repository = $this->getDoctrine()->getManager();
            $query = $repository->createQueryBuilder()
                ->select('p.id,p.note, p.date, p.markupType, 
                            b.title AS book_title, b.image, b.publisher, b.publishDate, b.authors, b.bookUrl, b.price, b.tags, b.likes, b.markups,b.id AS book_id,
                            uf.picture, uf.firstname, uf.lastname,
                            m.content, m.title, m.musicAuthor, m.musicAuthorImg')
                ->from('YottaBundle:Posts', 'p')
                ->innerJoin('YottaBundle:Books', 'b', 'WITH', 'p.bookId = b.id')
                ->innerJoin('YottaBundle:UserInfo', 'uf', 'WITH', 'uf.id = p.friend')
                ->innerJoin('YottaBundle:Markups', 'm', 'WITH', 'm.id = p.markupId')
                ->where('p.bookId != 0 Order by p.date DESC')
                ->setMaxResults(20)
                ->getQuery();
            $posts = $query->getResult();

            $em = YottaBundle::getContainer()->get('doctrine')->getManager();
            $con = $em->getConnection()
                ->prepare('
                SELECT GROUP_CONCAT(t.id) FROM (SELECT p.`id`
                FROM posts AS p
                INNER JOIN books AS b
                INNER JOIN user_info AS uf
                INNER JOIN markups AS m
                WHERE (p.`book_id` != 0) AND (p.`book_id` = b.`id`) AND (uf.`id` = p.`friend_id`) AND (m.`id` = p.`markup_id`)
                ORDER BY p.`date` DESC LIMIT 20 ) AS t
                ');
                $con->execute();
            $result = $con->fetchAll();
            $result = $result[0]["GROUP_CONCAT(t.id)"];

            $em = YottaBundle::getContainer()->get('doctrine')->getManager();
            $con = $em->getConnection()
                ->prepare(
                    'SELECT LEFT(p.date , 10) as date FROM `posts` AS p 
                     INNER JOIN books 
                     INNER JOIN user_info
                     INNER JOIN markups
                     WHERE (books.`id` = p.`book_id`) AND (user_info.`id` = p.`friend_id`) 
                     AND (markups.`id` = p.`markup_id`) AND (p.`book_id` != 0)
                     AND p.`id`IN ('. $result .')
                     GROUP BY YEAR(p.date),MONTH(p.date), DAY(p.date) ORDER BY p.date DESC'
                );
            $con->execute();
            $dates = $con->fetchAll();

            foreach ($posts as $key => $post) {
                $posts[$key]['date'] = $posts[$key]['date']->format('Y-m-d');
                $date = $post['publishDate'];
                if ($date) {
                    $posts[$key]['publishDate'] = $date->format('Y-m-d');
                    preg_match("#^-.+#i", $posts[$key]['publishDate'], $matches);
                    if ($matches[0]) {
                        $posts[$key]['publishDate'] = '0000-00-00';
                    } else {
                        $posts[$key]['publishDate'] = $date->format('Y-m-d');
                    }
                } else {
                    $posts[$key]['publishDate'] = '0000-00-00';
                }
                $user_img = $posts[$key]['picture'];
                $image = $this->generateCorrectProfilePicture($user_img, false);
                $posts[$key]['picture'] = $image;
            }
            
            $rate = new LibraryController();
            foreach ($posts as $key => $value){
                $books= $rate->getBookRate( $value['book_id']);
                if($books and isset($books[0]['rate'])){
                    $posts[$key]['rate']=(integer)$books[0]['rate']*20;
                }else{
                    $posts[$key]['rate']=0;
                }
            }

            return $this->render('YottaBundle:templates:profile.html.twig', array('user_data' =>
                $user_data, 'posts' => $posts, 'dates' => $dates ));
        } else {
//            return $this->render('YottaBundle:Default:layout.html.twig');
            return new RedirectResponse("/");
        }
    }

    private function sendMail($mailData)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject($mailData['subject'])
            ->setFrom($mailData['from'])
            ->setTo($mailData['to'])
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'Emails/registration.html.twig',
                    array('name' => $mailData['name'], 'message' => $mailData['message'])
                ),
                'text/html'
            );

        $type = $message->getHeaders()->get('Content-Type');
        $type->setValue('text/html');
        $type->setParameter('charset', 'utf-8');
        $this->get('mailer')->send($message);
    }

    /**
     * @Route("/activate_account", name="activate_acount")
     */
    public function activate_account(Request $request)
    {
        $user_id = isset($_GET['u']) ? $_GET['u'] : '';
        $user_hash = isset($_GET['h']) ? $_GET['h'] : '';
        $access_token = sha1('yotta' . $user_id . 'book');
        if ($user_id && $user_hash) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->update('YottaBundle:Users', 'u')
                ->set('u.accessToken', ':a_token')
                ->set('u.isActive', ':is_active')
                ->where('u.id = :u_id AND u.hash = :u_hash')
                ->setParameter("a_token", $access_token)
                ->setParameter('is_active', 'yes')
                ->setParameter('u_id', $user_id)
                ->setParameter('u_hash', $user_hash)
                ->getQuery();
            $query->execute();

            return new RedirectResponse("/");

        } else {
            return "Error! Something is wrong.";
        }
    }


    private function deleteCookie()
    {
        setcookie('u_id', null, -1, '/');
        setcookie('f_id', null, -1, '/');
        setcookie('u_name', null, -1, '/');
    }


    private function chackUserTwitterId($uid)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getConnection()
            ->prepare(
                'SELECT `id` FROM `users` WHERE  `twitter_user_id`=:tw_id'
            );
        $query->bindValue('tw_id', $uid);
        $query->execute();
        $user_id = $query->fetchAll();
        $user_id = $user_id[0]['id'];
        return $user_id;
    }

    private function chackUserFacebookId($uid)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getConnection()
            ->prepare(
                'SELECT `id` FROM `users` WHERE  `facebook_user_id`=:fb_id'
            );
        $query->bindValue('fb_id', $uid);
        $query->execute();
        $user_id = $query->fetchAll();
        $user_id = $user_id[0]['id'];
        return $user_id;
    }

    private function chackUserGooglePluskId($uid)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getConnection()
            ->prepare(
                'SELECT `id` FROM `users` WHERE  `google_id`=:gp_id'
            );
        $query->bindValue('gp_id', $uid);
        $query->execute();
        $user_id = $query->fetchAll();
        $user_id = $user_id[0]['id'];
        return $user_id;
    }


    private function chackUserFriendByID($u_id, $f_id)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getConnection()
            ->prepare(
                'SELECT * FROM `friends` WHERE (`friend_id`= :f_id AND `user_id` = :u_id)  OR  (`friend_id`= :u_id AND `user_id` = :f_id)'
            );
        $query->bindValue('u_id', $u_id);
        $query->bindValue('f_id', $f_id);
        $query->execute();
        $result = $query->fetchAll();
        return $result;
    }

    private function addUser($user_data)
    {
        $repository = $this->getDoctrine()->getManager()->getConnection();
        $query = $repository->prepare('INSERT INTO `users` (`email`,`twitter_user_id`,`facebook_user_id`,`google_id`,`access_token`,`created`, `is_active`) values(:email, :tw_id, :fb_id, :gp_id, :acces_token, :c_date, :is_active)');
        $query->bindValue('email', $user_data['email']);
        $query->bindValue('tw_id', $user_data['tw_id']);
        $query->bindValue('fb_id', $user_data['fb_id']);
        $query->bindValue('gp_id', $user_data['gp_id']);
        $query->bindValue('acces_token', $user_data['access_token']);
        $query->bindValue('c_date', date("Y-m-d H:i:s"));
        $query->bindValue('is_active', "yes");
        $query->execute();
        $user_id = $repository->lastInsertId();

        $query = $repository->prepare('INSERT INTO `user_info` (`picture`,`firstname`,`lastname`,`user_id`) values(:pic, :f_name, :l_name, :u_id)');
        $query->bindValue('pic', $user_data['avatar']);
        $query->bindValue('f_name', $user_data['f_name']);
        $query->bindValue('l_name', $user_data['l_name']);
        $query->bindValue('u_id', $user_id);
        $query->execute();

        return $user_id;
    }

    private function objectToArrayGenerator($obj, $out = array())
    {
        foreach ((array)$obj as $index => $node) {
            $out[$index] = (is_object($node)) ? $this->objectToArrayGenerator($node) : $node;
        }
        return $out;
    }


    private function getUserData($user_id)
    {
        $repository = $this->getDoctrine()->getManager();
        $query = $repository->createQueryBuilder()
            ->select('u.id, u.username, u.password, u.profile, u.email, ui.firstname, ui.lastname, ui.picture')
            ->from('YottaBundle:Users', 'u')
            ->innerJoin('YottaBundle:UserInfo', 'ui', 'WITH', 'ui.user = u.id')
            ->where('u.id = :id')
            ->setParameter('id', $user_id)
            ->getQuery();

        $user_data = $query->getResult();
        $user_img = $user_data[0]['picture'];
        $image = $this->generateCorrectProfilePicture($user_img, false);
        $user_data[0]['picture'] = $image;
        return $user_data[0];
    }

    private function generateCorrectProfilePicture($user_img, $flag)
    {
        preg_match("#http.*#i", $user_img, $matches);
        if ($matches) {
            $src = $user_img;
        } else if ($user_img == null) {
            $src = "/img/defaultUserImg.png";
        } else {
            $src = "/img/$user_img";
        }
        if ($flag) {
            return $src;
        } else {
            $image = "<img src='$src' alt='image' title='image' />";
            return $image;
        }
    }


    private function validatePassword($password)
    {
        if (strlen($password) >= 6) {
            $password = trim($password);
            $password = stripslashes($password);
            $password = htmlspecialchars($password);
            return $password;
        } else {
            return false;
        }
    }

}
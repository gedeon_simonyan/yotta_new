<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Registration extends Controller {

	public function action_index()
	{
        if (HTTP_Request::POST == $this->request->method()){
            $this->validate();
        } else {
            $this->redirect(URL::base(true));
        }

    }

    public function action_facebook(){
        $user = new Model_user();
        $error = $user->addFacebookUser($_POST);
        if(false == $error){
            echo "success";
        } else {
            print_r($error);
        }

    }

//    google

    public function action_oauth2callback(){
        require_once 'Google/google.php';
        $openid = new LightOpenID("http://localhost");

        if ($openid->mode) {
            if ($openid->mode == 'cancel') {
                echo "User has canceled authentication!";
            } elseif($openid->validate()) {
                $data = $openid->getAttributes();
                $user = new Model_user();
                $user->addGoogleUser($data,$openid->identity);
                $this->redirect("logged/profile/index");
            } else {
                $this->receivedError(array("error"=> "The user has not logged in."));
            }
        } else {
            $this->receivedError(array("error"=> "Something went wrong. Please try again later."));
        }
    }

    public function action_google(){
        require_once 'Google/google.php';
        $openid = new LightOpenID("http://localhost");

        $openid->identity = 'https://www.google.com/accounts/o8/id';
        $openid->required = array(
          'namePerson/first',
          'namePerson/last',
            "picture",
          'contact/email',
        );
        $openid->returnUrl = URL::base(true)."registration/oauth2callback";

        $this->redirect($openid->authUrl());
    }
//    twitter

    public function action_OAuth()
    {
        try{
            require_once('twitteroauth/twitteroauth.php');
            $twitteroauth = new TwitterOAuth('egjLSoP71dlkKJRCKMb7coUI2', 'XO114UPh9ke9MEEqRTAdRJRKpQHawzi6RK8vI9FXsFnSBbrWwP',
            Session::instance()->get('request_token'), Session::instance()->get('request_token_secret'));
            $access_token = $twitteroauth->getAccessToken($_GET['oauth_verifier']);
            Session::instance()->set('access_token',$access_token);
            $user_info = $twitteroauth->get('account/verify_credentials');
            $user = new Model_user();

        } catch (Exception $e){
            $this->receivedError(array("error"=> $e->getMessage()));
            return;
        }

        if( false == $user->addTwitterUser($user_info,$_GET['oauth_verifier'],$_GET['oauth_token'])){
            $this->redirect("logged/profile/index");
            return;
        } else {
            $this->receivedError( array("error"=> "Unknown error occurred. Please try again later."));
        }
    }

    private function receivedError($error){
        $view =  View::factory("site/sign_in_up")->set("loginError",$error)->set("loginData",$_POST);
        $this->response->body($view);
    }
    public function action_twitter()
    {
            require_once('twitteroauth/twitteroauth.php');
            $connection = new TwitterOAuth("egjLSoP71dlkKJRCKMb7coUI2", "XO114UPh9ke9MEEqRTAdRJRKpQHawzi6RK8vI9FXsFnSBbrWwP");
            $request_token = $connection->getRequestToken(URL::base()."registration/OAuth"); //get Request Token

        	if(	$request_token) {
                $session = Session::instance();
                $session->set("request_token", $request_token['oauth_token']);
        		$token = $request_token['oauth_token'];
                $session->set('request_token',$token) ;
                $session->set('request_token_secret',$request_token['oauth_token_secret']);

        		switch ($connection->http_code) {
        			case 200:
        				$url = $connection->getAuthorizeURL($token);
                        $this->redirect($url);
        			    break;
        			default:
                        $this->receivedError(array("error"=> "Error Receiving Request Token"));
        		    	break;
        		}

        	} else {
                $this->receivedError(array("error"=> "Error Receiving Request Token"));
        	}
    }

    private function validate(){
        $user = new Model_user();

       $post = new Validation($_POST);
       $post->rule('username','trim')
            ->rule('username', 'not_empty')
            ->rule('username', 'min_length', array(':value','4'))
            ->rule('username', 'regex', array(':value','/^[a-zA-Z0-9]+$/'))
            ->rule('username', array($user, 'unique_username'))

            ->rule('email','trim')
            ->rule('email', 'not_empty')
            ->rule('email', "email")

            ->rule('password','trim')
            ->rule('password', 'not_empty')
            ->rule('password', 'min_length', array(':value','6'));

        if ($post->check()) {
            $error = $user->register($post->data());
            if($error == false){
                if(count($error) > 0){
                    $this->redirect("logged/profile/index");
                    return;
                } else {
                    $error = array("error"=> "Unknown error occurred. Please try again later.");
                }
            }
        } else {
            $error = $post->errors('validate');
        }
        $view =  View::factory("site/sign_in_up")->set("registerError",$error)->set("data",$_POST);
        $this->response->body($view);
    }

} // End Welcome
